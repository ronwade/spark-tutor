
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
    
    <?php

        $asset_folder = (!empty($asset_folder))? $asset_folder : 'https://spark-education.com/wp-content/uploads/assets';
        $website_url = (!empty($website))? $website : "https://spark-education.com";

    ?>

    <head>
        <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <meta
        content="width=device-width" name="viewport"/>
        <!--[if !mso]><!-->
        <meta
        content="IE=edge" http-equiv="X-UA-Compatible"/>
        <!--<![endif]-->
        <title></title>
        <!--[if !mso]><!-->
        <link
        href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet" type="text/css"/>
        <!--<![endif]-->
        <style>
            body {
                background-color: #B8CCE2;
                font-family: Cabin, Arial, Helvetica Neue, Helvetica, sans-serif;
                line-height:1.2;
            }
        </style>
    </head>
    <body>
        <div style="max-width: 600px; margin: 0 auto; background-color: white;">
        
            <?php include_once "parts/email-header.php" ?>

            <div class="content-body" style="padding: 10px 40px 40px 40px;  min-height: 500px;">
                <h1>Tutor Registration Status</h1>
                <hr>
                <h3>Re: <?php echo "$first_name $last_name"; ?></h3>
                <div>Hi there,
                    <p>
                        <?php echo "$first_name"; ?>'s registration status has been marked as <strong><?php echo $registration_status; ?></strong>
                        on Spark Education platform.
                        Please review <?php echo "$first_name"; ?>'s account and make the necessary changes if applicable.
                    </p>
                    <div>
                        <a href="<?php echo $tutor_myprofile_url;  ?>" style="display: inline-block; padding: 10px 20px; background-color: #FFD500; border-radius: 25px; color: black; text-decoration: none; text-transform: uppercase; font-weight: 600;">
                            View <?php echo "$first_name"; ?>'s Profile  &gt;&gt;
                        </a>
                        <a href="<?php echo $tutor_dashboard_url;  ?>" style="float: right; display: inline-block; padding: 10px 20px; background-color: #FFD500; border-radius: 25px; color: black; text-decoration: none; text-transform: uppercase; font-weight: 600;">
                            View Dashboard  &gt;&gt;
                        </a>
                    </div>
                    <br>
                    <br>
                    <div style="clear: both">
                        <p>To get familar with our platform, we recommend that you take our Tutor Onboarding Course by click on the link below:</p>
                        <a href="<?php echo $tutor_onboarding_course_url;  ?>" style="display: inline-block; padding: 10px 20px; background-color: #FFD500; border-radius: 25px; color: black; text-decoration: none; text-transform: uppercase; font-weight: 600;">
                            Tutor Onboarding Course  &gt;&gt;
                        </a>
                    </div>
                </div>
            </div>
            
            <?php include_once "parts/email-footer.php" ?>
        </div>
    </body>
</html>