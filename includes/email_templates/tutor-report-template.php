<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable"
       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;">
    <tbody>
    <tr>
        <td align="center" valign="top" id="bodyCell"
            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;">
            <!-- BEGIN TEMPLATE // -->
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                <tr>
                    <td align="center" valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer"
                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
                <tbody>
                <tr>
                    <td valign="top" id="templatePreheader"
                        style="background:#fafafa none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
                               style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnTextBlockOuter">
                            <tr>
                                <td valign="top" class="mcnTextBlockInner"
                                    style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <!--[if mso]>
                                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
                                           style="width:100%;">
                                        <tr>
                                    <![endif]-->

                                    <!--[if mso]>
                                    <td valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <!--[if mso]>
                                    </td>
                                    <![endif]-->

                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" id="templateHeader"
                        style="background:#FFFFFF none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 0;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
                               style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnTextBlockOuter">
                            <tr>
                                <td valign="top" class="mcnTextBlockInner"
                                    style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <!--[if mso]>
                                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
                                           style="width:100%;">
                                        <tr>
                                    <![endif]-->

                                    <!--[if mso]>
                                    <td valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                           style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                           width="100%" class="mcnTextContentContainer">
                                        <tbody>
                                        <tr>

                                            <td valign="top" class="mcnTextContent"
                                                style="padding: 0px 18px 9px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-size: 16px;line-height: 150%;text-align: left;">

                                                <h1 style="text-align: center;display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;">
                                                    <br>
                                                    Review Lesson</h1>
                                                &nbsp;

                                                <hr>
                                                <p style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-size: 16px;line-height: 150%;text-align: left;">
                                                <?php echo esc_html__('Hi', 'spark-tutor') ?> <strong><?php echo esc_html($tutor_name); ?></strong>,<br>
                                                    <br>
                                                    <?php echo esc_html__('Your', 'spark-tutor') ?> <strong>"<?php echo esc_html($course_name) ?>"&nbsp;</strong><?php echo esc_html__('course booking with', 'spark-tutor') ?>
                                                    <strong><?php echo esc_html($student_name) ?></strong> <?php echo esc_html__('is finished at', 'spark-tutor') ?> <strong><?php echo esc_html($time) ?></strong>
                                                    <?php echo esc_html__('on', 'spark-tutor') ?> <strong><?php echo esc_html($date) ?></strong> <?php echo esc_html__('for', 'spark-tutor') ?> <strong><?php echo esc_html($duration) . " mins" ?></strong>.&nbsp;<br>
                                                    <?php echo esc_html__('Please click on the following link to give report to your student', 'spark-tutor') ?>: <strong><a href="<?php echo esc_url($link) ?>"><?php echo esc_url($link) ?></a></strong>
                                                    <br>
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!--[if mso]>
                                    </td>
                                    <![endif]-->

                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
            <!-- // END TEMPLATE -->
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
