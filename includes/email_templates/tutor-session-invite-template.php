
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">

    <?php

        $asset_folder = (!empty($asset_folder))? $asset_folder : 'https://spark-education.com/wp-content/uploads/assets';
        $website_url = (!empty($website))? $website : "https://spark-education.com";


        if (!empty($spark_asset_folder)){
            $image_folder = "$spark_asset_folder/img";
        } else {
            $image_folder = "$asset_folder";
            $spark_asset_folder = $asset_folder;
        }

    ?>

    <head>
        <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <meta
        content="width=device-width" name="viewport"/>
        <!--[if !mso]><!-->
        <meta
        content="IE=edge" http-equiv="X-UA-Compatible"/>
        <!--<![endif]-->
        <title></title>
        <!--[if !mso]><!-->
        <link
        href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet" type="text/css"/>
        <!--<![endif]-->
        <style>
            body {
                background-color: #B8CCE2;
                font-family: Cabin, Arial, Helvetica Neue, Helvetica, sans-serif;
                line-height: 1.2;
            }
        </style>
    </head>
    <body>
        <div style="max-width: 600px; margin: 0 auto; background-color: white;">

            <?php include_once "parts/email-header.php" ?>

            <div class="content-body" style="padding: 10px 40px 40px 40px;  min-height: 500px;">
                <h3>Great News <?php echo "$first_name $last_name"; ?>!</h3>
                <div>
                    <p style="font-size: 12pt;">
                        Your registration status has been <strong>reviewed</strong>.
                        It's now time for STEP 2:
                    </p>

                    <div align="center" class="img-container center fixedwidth" style="padding-right: 0px;padding-left: 0px;">
                        <img align="center" alt="Image" border="0" class="center fixedwidth" src="<?php echo $image_folder.'/'.$image_status ?>"
                            style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 530px; display: block;" title="Image" width="530"/>
                    </div>
                    <p style="font-size: 14pt; text-align: center">
                        <span style="font-weight: 600;" >Participate in the LIVE tutorial on</span>
                        <br>
                        December 12, 2020 at 12:00 pm - 1:30 pm.
                    </p>
                    <div>
                        <a href="https://zoom.us/meeting/register/tJMkc-qoqjMqE9Rlc3b1OqckgsGXUN4lb33o" style="display: inline-block; padding: 10px 20px; background-color: #FFD500; border-radius: 25px; color: black; text-decoration: none; text-transform: uppercase; font-weight: 600;">
                            ACCESS THE TUTORIAL
                        </a>
                        <br>
                        <br>
                        <p  style="font-size: 14pt;">In the meantime, get familiar with our platform and record any questions you may have.</p>
                        <a href="<?php echo $tutor_dashboard_url;  ?>" style="display: inline-block; padding: 10px 20px; background-color: #FFD500; border-radius: 25px; color: black; text-decoration: none; text-transform: uppercase; font-weight: 600;">
                            View Dashboard  &gt;&gt;
                        </a>
                    </div>
	                 <p style="font-size: 12pt; font-weight: bold;">
		                  <br>
		                  Brittany Singh Williams, M.Ed.<br>
		                  Founder, Spark Education Limited<br>
		                  <br>
		                  "Igniting Future Leaders"<br>
		                  <a href="https://www.spark-education.com">www.spark-education.com</a><br>
		                  <a href="tel:18765767756">1.876.576.7756</a>
	                 </p>
                </div>
            </div>

            <?php include_once "parts/email-footer.php" ?>
        </div>
    </body>
</html>
