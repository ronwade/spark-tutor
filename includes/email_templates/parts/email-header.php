
<div style="background-color: #132f40; min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top; width: 600px;"> <div style="width:100% !important;">
    <!--[if (!mso)&(!IE)]><!-->
    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 20px;">
    <!--<![endif]-->
        <div align="left" class="img-container left fixedwidth" style="padding-right: 25px;padding-left: 25px;">
        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 25px;padding-left: 25px;" align="left"><![endif]-->
            <div style="font-size:1px;line-height:25px"></div>
            <a href="<?php echo $website_url; ?>">
                <img alt="Image" border="0" class="left fixedwidth" src="<?php echo $asset_folder ?>/Spark_Logo_light.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 210px; display: block;" title="Image" width="210">
            </a>
            <div style="font-size:1px;line-height:25px"></div>
            <!--[if mso]></td></tr></table><![endif]-->
            </div>
        <!--[if (!mso)&(!IE)]><!-->
        </div>
    <!--<![endif]-->
    </div>
</div>