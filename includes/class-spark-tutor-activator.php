<?php

/**
 * Fired during plugin activation
 *
 * @link       http://osoobe.com
 * @since      1.0.0
 *
 * @package    Spark_Tutor
 * @subpackage Spark_Tutor/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Spark_Tutor
 * @subpackage Spark_Tutor/includes
 * @author     Oshane Bailey <b4.oshany@gmail.com>
 */
class Spark_Tutor_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		set_transient( 'sparktutor_flush', 1, 60 );
		register_taxonomy('weekday', 'gu_tut_available_day', array(
			'label' => 'Weekday',
			'labels' => array(
				'name' => esc_html__('Weekday', 'spark-tutor'),
				'singular_name' => esc_html__('Weekday', 'spark-tutor'),
			),
			'public' => true,
			'hierarchical' => true,

		));
		if (term_exists('monday', 'weekday', null) == null) {
			wp_insert_term('monday', 'weekday');
		}
		if (term_exists('tuesday', 'weekday', null) == null) {
			wp_insert_term('tuesday', 'weekday');
		}
		if (term_exists('wednesday', 'weekday', null) == null) {
			wp_insert_term('wednesday', 'weekday');
		}
		if (term_exists('thursday', 'weekday', null) == null) {
			wp_insert_term('thursday', 'weekday');
		}
		if (term_exists('friday', 'weekday', null) == null) {
			wp_insert_term('friday', 'weekday');
		}
		if (term_exists('saturday', 'weekday', null) == null) {
			wp_insert_term('saturday', 'weekday');
		}
		if (term_exists('sunday', 'weekday', null) == null) {
			wp_insert_term('sunday', 'weekday');
		}

	}

}
