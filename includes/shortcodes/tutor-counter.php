<?php
    $tutor_count = CPTutor::getActiveTutorCount();
?>
<div class="premium-counter spark-tutor-counter premium-counter-area left" data-duration="3000" data-from-value="0" 
    data-to-value="<?php echo $tutor_count; ?>" data-delimiter="," data-rounding="0">

    <div class="premium-counter-icon">

        <span class="icon animated" data-animation="">

            <i aria-hidden="true" class="icon icon-team1 animated"></i>
        </span>
    </div>


    <div class="premium-init-wrapper">

        <div class="premium-counter-value-wrap">

            <span class="premium-counter-init" id="counter-98c6008">
            <?php echo $tutor_count; ?>
            </span>

        </div>

        <h4 class="premium-counter-title">
            <div>
                Tutors 
            </div>
        </h4>
    </div>

</div>