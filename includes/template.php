<?php
    class Template
    {
        protected $_file;
        protected $_data = array();

        public function __construct($file = null)
        {
            $this->_file = $file;
        }

        public function set($key, $value)
        {
            $this->_data[$key] = $value;
            return $this;
        }

        public function changeTemplate($template_file)
        {
            $this->_file = $template_file;
        }

        public function render()
        {
            extract($this->_data);
            ob_start();
            include($this->_file);
            return ob_get_clean();
        }
    }


?>