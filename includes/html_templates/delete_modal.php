<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="<?php echo admin_url('admin-post.php'); ?>">
                <input type="hidden" value="<?php echo esc_attr($id) ?>" name="view_post_id"/>
                <input type="hidden" value="<?php echo esc_attr($id) ?>" name="id" id="delete-object-id" />
                <input type="hidden" name="action" value="delete_object">
                <input type="hidden" name="page" value="<?php echo spark_get_current_page() ?>">
                <input type="hidden" name="view" value="<?php echo spark_get_current_view() ?>">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo esc_html__('Are you sure', 'spark-tutor') ?>?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div>
                            <?php echo esc_html__('Do you really want to delete the record', 'spark-tutor') ?>? <?php echo esc_html__('This process cannot be undone', 'spark-tutor') ?>.
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Cancel', 'spark-tutor') ?></button>
                    <button type="button" class="btn btn-danger btn-confirm-delete"><?php echo esc_html__('Delete', 'spark-tutor') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
