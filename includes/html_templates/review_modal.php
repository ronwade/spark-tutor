<?php
$id = isset($_GET['id']) ? sanitize_text_field($_GET['id']) : '';
?>
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="reviewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" action="<?php echo admin_url('admin-post.php'); ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="reviewModalLabel"><?php echo esc_html__('Review', 'spark-tutor') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <input type="hidden" value="<?php echo esc_attr($id) ?>" name="view_post_id"/>
                        <input type="hidden" value="-1" name="id" class="delete-object-id" />
                        <input type="hidden" name="action" value="delete_object">
                        <input type="hidden" name="page" value="<?php echo spark_get_current_page() ?>">
                        <input type="hidden" name="view" value="<?php echo spark_get_current_view() ?>">
                        <div class="row">
                            <div class="col-sm-4">
                                <?php echo esc_html__('Tutor', 'spark-tutor') ?>: <a href="#" class="tutor-name"> <?php echo esc_html__('Tutor Name', 'spark-tutor') ?></a>
                            </div>
                            <div class="col-sm-4">
                                <?php echo esc_html__('Student', 'spark-tutor') ?>: <a href="#" class="student-name"> <?php echo esc_html__('Student Name', 'spark-tutor') ?></a>
                            </div>
                            <div class="col-sm-4">
                                <?php echo esc_html__('Date Written', 'spark-tutor') ?>: <span class="created-date"></span>
                            </div>
                        </div>
                        <div class="row mt-4 review-rating">
                            <div class="col-sm-12 ">
                                <h5><?php echo esc_html__('Review Rating', 'spark-tutor') ?></h5>
                            </div>
                            <div class="col-sm-12 star-rating-modal">
                                <span class="star-color star-wrapper"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <h5><?php echo esc_html__('Review Details', 'spark-tutor') ?></h5>
                            <div class="comment">
                                Comment
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-confirm-delete"><?php echo esc_html__('Delete', 'spark-tutor') ?></button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Close', 'spark-tutor') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
