<?php
class CPSubject extends CPEntity {
	function __construct() {
		$this->cpPostType = SPARK_TUTOR_SUBJECT_POST_TYPE;
		$this->fillable = ['name', 'picture'];
		$this->cpPostName = SPARK_TUTOR_SUBJECT_POST_NAME;
	}
	public function save_post_data($post_data) {
		$file_name = isset($_FILES['picture']) && sanitize_file_name($_FILES['picture']['name']) ? sanitize_file_name($_FILES['picture']['name']) : '';

		if ($file_name != '') {
			error_log('message');
			$attachment_id = 0;

			if ($_FILES['picture']['error'] == 0) {
				$upload_overrides = array('test_form' => false);

				$file_return = wp_handle_upload($_FILES['picture'], $upload_overrides);

				$filename = $file_return['file'];

				$attachment = array(
					'post_mime_type' => $file_return['url'],
					'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
					'post_content' => '',
					'post_status' => 'inherit',
					'guid' => $file_return['url'],
				);

				$attachment_id = wp_insert_attachment($attachment, $file_return['url']);
				require_once ABSPATH . 'wp-admin/includes/image.php';
				$attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);

				$final = wp_update_attachment_metadata($attachment_id, $attachment_data);
			}
			$post_data['picture'] = wp_get_attachment_url($attachment_id);
		} else {
			$post_data['picture'] = SPARK_TUTOR_PLUGIN_PUBLIC_IMG_URL."placeholder.jpg";
		}
		return parent::save_post_data($post_data);
	}
}
