<?php
class CPEntity {
	protected $cpPostName;
	protected $cpPostType;
	protected $fillable = [];
	protected $uniqueMeta = [];
	protected $post;
	protected $postMeta;
	protected $postErrors = [];

	function __construct() {

	}

	public function getUniqueFields() {
		return $this->uniqueMeta;
	}

	protected function getFilters($postData, $fields, $operator) {
		return array_map(
			function ($meta) use ($postData, $operator) {
				return  array(
					"key" => $meta,
					"value"=> $postData[$meta],
					"compare" => $operator
				);
			},
			$fields
		);
	}


	/**
	 * Check if post field data is unique.
	 * @param array $postData - HTTP POST data
	 * @param array $fields - check if the given postData fields are unique.
	 *
	 * @return bool - True if the field data is unique, else false.
	 */
	public function uniqueFieldValidator($postData, $fields) {

		// Fields are unique if no fields provide;
		if ( empty($fields) ) {
			return true;
		}

		// Remove current post data that haven't changed for existing content.
		if ( !empty($postData['id']) ) {
			$metaData = get_post_meta( $postData['id'] );
			$fields = array_filter(
				$fields,
				function ($field) use ($postData, $metaData) {
					return $postData[$field] != $metaData[$field][0];
			});
		}
			
		// Build query filter based on fields.
		$filters = $this->getFilters($postData, $fields, '=');
		if(!empty($filters)) {
			$args = array(
				'post_type' => $this->cpPostType,
				'relation' => 'OR',
				'meta_query' => $filters
			);
			$posts = new WP_Query($args);
			if($posts->have_posts()) {
				return false;
			}
			return true;
		}
		return true;
	}

	public static function getCount($queryArgs=array()) {
		$obj = new static();
		$queryArgs['post_type'] = $obj->getCpPostType();
		$q = new WP_Query($queryArgs);
		return $q->post_count;
	}

	public function uniqueValidator($postData) {
		return $this->uniqueFieldValidator($postData, $this->uniqueMeta);
	}

	public function validate($postData) {
		return $this->uniqueValidator($postData);
	}

	

	public function save_post_data($postData) {
		return $this->_save_post_data($postData);
	}

	public function _save_post_data($postData) {

		if (!$this->validate($postData)) {
			return false;
		}

		if (empty($postData['id'])) {
			$date = date('m-d-Y H:i');
			$postarr = [
				'post_title' => $this->cpPostName . ' - ' . $date,
				'post_content' => '',
				'post_type' => $this->cpPostType,
				'post_status' => 'publish',
			];
			$postId = wp_insert_post($postarr, false);
			update_post_meta($postId, 'status', SPARK_TUTOR_STATUS_ACTIVE);
		} else {
			$postId = $postData['id'];
		}
		foreach ($this->fillable as $meta_name) {
			if (isset($postData[$meta_name])) {

				if ($meta_name == 'note' || $meta_name == 'description' || $meta_name == 'detail') {
					$meta_data = sanitize_textarea_field($postData[$meta_name]);
				} else {
					$meta_data = sanitize_text_field($postData[$meta_name]);
				}
				update_post_meta($postId, $meta_name, $meta_data);
			}
		}
		return $postId;
	}

	public function setField($field_name) {

	}


	public function getId() {
		return $this->post->ID;
	}

	public function getField($field_name) {
		if (isset($this->post)) {
			switch ($field_name) {
			case 'ID':
				return $this->post->ID;
				break;
			case 'created_date':
				return $this->post->post_date;
				break;
			default:
				return get_post_meta($this->post->ID, $field_name, true);
				break;
			}

		}
		return '';
	}

	public function getPic() {
		if ( in_array('picture', $this->fillable) ) {
			return $this->getField('picture');
		} else if ( in_array('thumbnail', $this->fillable) ) {
			return $this->getField('thumbnail');
		}
		return null;
	}

	/**
	 * @return mixed
	 */
	public function getCpPostType() {
		return $this->cpPostType;
	}

	public function isValidEntity() {
		return !empty($this->cpPostType);
	}

	/**
	 * @param mixed $cpPostType
	 *
	 * @return self
	 */
	public function setCpPostType($cpPostType) {
		$this->cpPostType = $cpPostType;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPost() {
		return $this->post;
	}

	/**
	 * @param mixed $post
	 *
	 * @return self
	 */
	public function setPost($post) {
		$this->post = $post;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPostMeta() {
		return $this->postMeta;
	}

	/**
	 * @param mixed $postMeta
	 *
	 * @return self
	 */
	public function setPostMeta($postMeta) {
		$this->postMeta = $postMeta;

		return $this;
	}


	/**
	 * Get entity
	 * @param int $id  Post Id.
	 *
	 * @return self
	 */
	public static function getEntity($id) {
		return spark_get_entity_by_id($id);
	}
}
