<?php
class CPBooking extends CPEntity {
	function __construct() {
		$this->cpPostType = SPARK_TUTOR_BOOKING_POST_TYPE;
		$this->fillable = ['student_name', 'student_phone', 'student_email', 'student_grade', 'subject', 'course', 'status', 'note', 'payment_method', 'price', 'duration', 'tax', 'total', 'start_time', 'end_time', 'booking_date', 'tutor', 'unique_id'];
		$this->cpPostName = SPARK_TUTOR_BOOKING_POST_NAME;
	}
	public function save_post_data($post_data) {
		$bookingId = parent::save_post_data($post_data);
		$date = date('Y-m-d H:i:s');
		update_post_meta($bookingId, 'created_date', $date);
		update_post_meta($bookingId, 'unique_id', spark_generateUniqueId($bookingId));
		if (empty($post_data['id'])) {
			update_post_meta($bookingId, 'status', SPARK_TUTOR_STATUS_PENDING);}
		if ($post_data['is_edit'] == false) {
			update_post_meta($bookingId, 'status', sanitize_text_field($post_data['status']));

		}
		return $bookingId;

	}
}
