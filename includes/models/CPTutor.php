<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
}


class CPTutor extends CPEntity {

	protected $uniqueMeta = ['email', 'trn_ssn'];
	protected $requiredMeta = [];
	protected $metaCheck = ['email', 'trn_ssn', 'resume', 'picture', 'name', 'intro'];
	protected $_user = null;

	function __construct() {
		$this->cpPostType = SPARK_TUTOR_TUTOR_POST_TYPE;
		$this->fillable = ['name', 'picture', 'resume', 'email',
			'phone', 'earining', 'status', 'avg_rating', 'intro', 'subject', 'trn_ssn',
			'intro_video_url', 'intro_video_source', 'grades', 'subjects'];
		$this->cpPostName = SPARK_TUTOR_TUTOR_POST_NAME;
	}

	/**
	 * @param string $url_param
	 * @param bool $private Whether the file should be private (only viewable by site admins) or not
	 *
	 * @return false|string
	 */
	private function save_file($url_param, $private = false) {
		$file_name = isset($_FILES[$url_param]) && sanitize_file_name($_FILES[$url_param]['name']) ? sanitize_file_name($_FILES[$url_param]['name']) : '';

		if ($file_name != '') {
			error_log('message');
			$attachment_id = 0;

			if ($_FILES[$url_param]['error'] == 0) {
				$upload_overrides = array('test_form' => false);

				if ($private) {
					$this->pre_private_upload();
				}

				$file_return = wp_handle_upload($_FILES[$url_param], $upload_overrides);

				$filename = $file_return['file'];

				$attachment = array(
					'post_mime_type' => $file_return['url'],
					'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
					'post_content' => '',
					'post_status' => 'inherit',
					'guid' => $file_return['url'],
				);

				$attachment_id = wp_insert_attachment($attachment, $file_return['url']);
				require_once ABSPATH . 'wp-admin/includes/image.php';
				$attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);

				$final = wp_update_attachment_metadata($attachment_id, $attachment_data);

				if ($private) {
					$this->post_private_upload();
				}
			}
			return wp_get_attachment_url($attachment_id);
		}
		return false;

	}

	public function save_post_data($post_data) {
		$password = null;
		$has_email_changed = false;

		$photo_attachment_url = $this->save_file('picture');
		if ( $photo_attachment_url ) {
			$post_data['picture'] = $photo_attachment_url;
		}

		$resume_attachment_url = $this->save_file('resume', true);
		if ( $resume_attachment_url ) {
			$post_data['resume'] = $resume_attachment_url;
		}

		if ( !empty($post_data['password'])) {
			$password = sanitize_text_field($post_data['password']);
		}

		$email = get_param_from_array($post_data, 'email', '');
		if($email != $this->getField('email')) {
			$has_email_changed = true;
		}

		if ( isset($post_data['subjects']) ) {
			$post_data['subjects'] = implode(',', $post_data['subjects']);
		}
		if ( isset($post_data['grades']) ) {
			$post_data['grades'] = implode(',', $post_data['grades']);
		}
		$tutorid = $this->_save_post_data($post_data);

		if ( !empty($post_data['cusr']) &&  $post_data['cusr'] == 't') {
			$this->register_instructor(
				$tutorid,
				sanitize_text_field($post_data['name']),
				$post_data['email'],
				$password
			);
		}
		$subjects = array();

		if (isset($post_data['course_name'])) {
			$course_ids = array();
			for ($i = 0; $i < count($post_data['course_name']); $i++) {
				$course_data = array(
					'name' => $post_data['course_name'][$i],
					'subject' => $post_data['course_subject'][$i],
					'duration' => $post_data['course_duration'][$i],
					'price' => $post_data['course_price'][$i],
					'tutor' => $tutorid,
				);
				if (isset($post_data['course_id'])) {
					$course_data['id'] = $post_data['course_id'][$i];
					array_push($course_ids, $course_data['id']);
				}
				array_push($subjects, sanitize_text_field($post_data['course_subject'][$i]));
				$course = new CPCourse();
				$courseId = $course->save_post_data($course_data);
				array_push($course_ids, $courseId);
			}
			$course_ids = array_unique($course_ids);
			$current_courses = CourseServices::get_courses_by_tutor_id($tutorid);
			foreach ($current_courses as $course) {
				if (!in_array($course->getField('ID'), $course_ids)) {
					wp_delete_post($course->getField('ID'));
				}
			}
		}
		$subjects = array_unique($subjects);
		update_post_meta($tutorid, 'subject', $subjects);

		if ( ! $has_email_changed ) {
			$this->auto_set_user_by_email();
		}
		return $tutorid;
	}

	public static function update_spark_tutor_user_id($user_id, $spark_tutor_id) {
		update_user_meta($user_id, SPARK_USER_META_TUTOR, $spark_tutor_id);
	}

	/**
	 * Register new user and mark him as instructor
	 *
	 * @since v.1.0.0
	 */
	public function register_instructor($spark_tutor_id, $name, $email, $password){
		$password = sanitize_text_field($password);
		$name_parts = explode(" ", sanitize_text_field($name));
		$last_name = array_pop($name_parts);
		$first_name = implode(" ", $name_parts);

		$num_users = CPTutor::getActiveTutorCount();
		$username = "$first_name".$num_users.''.rand(1, 100);


		$userdata = array(
			'user_login'    =>  $username,
			'user_email'    =>  $email,
			'first_name'    =>  $first_name,
			'last_name'     =>  $last_name,
			'role'          =>  SPARK_TUTOR_ROLE,
			'user_pass'     =>  $password,
		);

		$user_id = wp_insert_user( $userdata ) ;
		if ( ! is_wp_error($user_id)){
			update_user_meta($user_id, '_is_tutor_instructor', tutor_time());
			update_user_meta(
				$user_id, '_tutor_instructor_status',
				apply_filters('tutor_initial_instructor_status', 'pending'));

			CPTutor::update_spark_tutor_user_id($user_id, $spark_tutor_id);

			do_action('tutor_new_instructor_after', $user_id);
			do_action('send_tutor_registration_email', $user_id);
		}
		return $user_id;
	}

	public function user() {
		if ( empty($this->_user) ) {
			$tutor_id = $this->getId();
			if (! $tutor_id ) {
				return null;
			}
			$this->_user = static::get_user_by_tutor_id($tutor_id);
		}
		return $this->_user;
	}

	public function has_account() {
		return ! empty ($this->user());
	}

	public static function getActiveTutorCount(){
		return static::getCount(array(
			'meta_query' => array(
				array(
					"key" => "status",
					"value"=> SPARK_TUTOR_STATUS_ACTIVE,
					"compare" => "="
				)
			)
		));
	}

	public static function gen_profile_hash($email, $username, $tutor_id) {
		return md5("$email-$username-$tutor_id");
	}

	public static function get_profile_code_by_user($user) {
		return static::gen_profile_hash(
			$user->get("user_email"),
			$user->get('user_login'),
			$user->get(SPARK_USER_META_TUTOR)
		);
	}

	public static function get_profile_code_by_id($user_id) {
		$user = new \WP_User($user_id);
		return static::get_profile_code_by_user($user);
	}

	public static function get_user_tutor_profile($user) {

		if ( is_int($user) ) {
			$user = new \WP_User($user);
		}

		$tutor_id = $user->get(SPARK_USER_META_TUTOR);
		return static::getEntity( ( !empty($tutor_id) )? $tutor_id : 0 );
	}

	public static function get_user_by_tutor_id($tutor_id) {
		$result = get_users(
			array('meta_key' => SPARK_USER_META_TUTOR, 'meta_value' => $tutor_id)
		);
		if (count($result) > 0) {
			$user = $result[0];
			return ($user->ID)? $user : null;
		}
		return null;
	}

	public static function get_user_by_email($email) {
		return get_user_by('email', $email );
	}

	public function profile_code() {
		return static::get_profile_code_by_user($this->user());
	}

	public function check_profile_code($profile_code) {
		return $profile_code == $this->profile_code();
	}

	public function getMissingFields() {

		return array_filter($this->metaCheck, function($field) {
			return empty($this->getField($field));
		});
	}

	public function getName() {
		return $this->getField('name');
	}

	public function getEmail() {
		return $this->getField('email');
	}

	public function get_user_with_email() {
		return static::get_user_by_email($this->getEmail());
	}

	public function auto_set_user_by_email() {
		try {
			$user = $this->get_user_with_email();
			if ( $user->ID ) {
				static::update_spark_tutor_user_id($user->ID, $this->getId());
			}

		} catch (Exception $th) {
			//throw $th;
		}

	}

	public function has_video() {
		return ! empty($this->getField('intro_video_url')) && ! empty($this->getField('intro_video_source'));
	}

	public function render_video () {
		render_tutor_intro_video_html(
			esc_attr($this->getField('intro_video_source')),
			esc_attr($this->getField('intro_video_url'))
		);
	}


	public function pre_private_upload() {
		add_filter('upload_dir', array($this, 'private_upload_dir'));
	}

	public function post_private_upload() {
		remove_filter('upload_dir', array($this, 'private_upload_dir'));
	}

	public function private_upload_dir($uploads) {
		$uploads['path'] = SPARK_TUTOR_PLUGIN_UPLOAD_PATH;
		$uploads['url'] = SPARK_TUTOR_PLUGIN_UPLOAD_DIR_NAME;
		$uploads['subdir'] = SPARK_TUTOR_PLUGIN_UPLOAD_DIR_NAME;
		return $uploads;
	}

	public static function get_tutor_by_email($email) {
		$tutors = TutorServices::get_tutor_by_email($email);
		if ( count($tutors) > 0 ) {
			return $tutors[0];
		}
		return CPTutor::get_user_tutor_profile(0);
	}

	public static function get_current_tutor() {
		$s_user = wp_get_current_user();
		$tutors = TutorServices::get_tutor_by_email($s_user->get('user_email'));
		if ( count($tutors) > 0 ) {
			return $tutors[0];
		}
		return CPTutor::get_user_tutor_profile($s_user->ID);
	}

}
