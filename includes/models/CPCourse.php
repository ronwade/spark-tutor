<?php
class CPCourse extends CPEntity {
	function __construct() {
		$this->cpPostType = SPARK_TUTOR_COURSE_POST_TYPE;
		$this->fillable = ['name', 'subject', 'duration', 'price', 'tutor', 'thumbnail', 'description', 'detail'];
		$this->cpPostName = SPARK_TUTOR_COURSE_POST_NAME;
	}

	public function save_post_data($post_data) {
		$file_name = isset($_FILES['picture']) && sanitize_file_name($_FILES['picture']['name']) ? sanitize_file_name($_FILES['picture']['name']) : '';
		if (empty($post_data['price'])) {
			$post_data['price'] = spark_get_default_price();
		}

		if ($file_name != '') {
			error_log('message');
			$attachment_id = 0;

			if ($_FILES['picture']['error'] == 0) {
				$upload_overrides = array('test_form' => false);

				$file_return = wp_handle_upload($_FILES['picture'], $upload_overrides);

				$filename = $file_return['file'];

				$attachment = array(
					'post_mime_type' => $file_return['url'],
					'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
					'post_content' => '',
					'post_status' => 'inherit',
					'guid' => $file_return['url'],
				);

				$attachment_id = wp_insert_attachment($attachment, $file_return['url']);
				require_once ABSPATH . 'wp-admin/includes/image.php';
				$attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);

				$final = wp_update_attachment_metadata($attachment_id, $attachment_data);
			}
			$post_data['thumbnail'] = wp_get_attachment_url($attachment_id);
		}
		return parent::save_post_data($post_data);
	}

}
