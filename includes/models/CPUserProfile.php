<?php
/**
 * Main Plugin Class
 *
 * @package User Profile
 */

if ( ! defined( 'ABSPATH' ) )
exit;

if ( ! function_exists( 'wp_handle_upload' ) ) {
require_once( ABSPATH . 'wp-admin/includes/file.php' );
}

/**
 * Disable User Login plugin main class
 */
class CPUserProfile extends CPEntity {

	/**
	 * The user meta key to use for storing whether the user is disabled.
	 *
	 * @var string
	 */

	protected $uniqueMeta = [];
	protected $requiredMeta = [];
	protected $metaCheck = [];

	/**
	 * Hide constructor for this singleton
	 *
	 * @since 1.0.0
	 */
	private function __construct() {
		
		$this->cpPostType = SPARK_TUTOR_USER_PROFILE_POST_TYPE;
		$this->fillable = [
			'firstname', 'lastname', 'picture', 'email',  'intro',
			'grade', 'school', 'relation',
			'managed_by', 'created_by',
			'account_id'
		];
		$this->cpPostName = SPARK_TUTOR_USER_PROFILE_POST_NAME;

	}

	public static function get_profile($user) {
		$user = user_or_id($user);
		$id = $user->get(SPARK_USER_META_ID);

		if ( is_int($id) ) {
			$user_profile = CPUserProfile::getEntity($id);
			if ( $user_profile ) {
				return $user_profile;
			}
		}

		try {
			$roles = (array) $user->roles;
			$main_role = $roles[0];
		} catch (Exception $e) {
			$roles = [];
			$main_role = "";
		}

		$user_profile = new CPUserProfile();
		$user_data = [
			'first_name' => $user->get('firstname'),
			'last_name' => $user->get('firstname'),
			'picture' => '',
			'email' => $user->get('email'),
			'intro' => $user->get('description'),
			'grade' => '',
			'school' => '',
			'relation' => 'self',
			'managed_by' => $user->ID,
			'created_by' => $user->ID,
			'account_id' => $user->ID,
			'spark_role' => $main_role,
			'roles' => $roles
		];
		$user_pid = $user_profile->save_post_data($user_data);
		update_user_meta($user->ID, SPARK_USER_META_TUTOR, $user_pid);
		return $user_profile;
	}

	public function setSession(){
		if( $this->user ) {
			wp_set_current_user( $user_id, $user->user_login );
			wp_set_auth_cookie( $user_id );
		}
	}

	public function user(){
		return $this->user;
	}

	// public function is_parent() {
	// 	return (
	// 		in_array( SPARK_TUTOR_ROLE, (array) $this->user->roles ) ||
	// 		in_array( SPARK_PARENT_ROLE, (array) $this->user->roles )
	// 	);
	// }

	public function is_customer() {
		return (
			! $this->is_tutor()
		);
	}

	public function is_tutor() {
		return in_array( SPARK_TUTOR_ROLE, (array) $this->user->roles );
	}

	/**
	 * Checks if a user is disabled
	 *
	 * @since  1.2.0
	 *
	 * @param int $user_id The user ID to check
	 * @return boolean true if disabled, false if enabled
	 */
	public function tutor() {

		// Get user meta
		$tutor_id = get_user_meta( $this->user_id, 'tutor_id', false );
		if ( $tutor_id ) {
			return true;
		}

		return false;

	}

} //end class User