<?php
class CPReview extends CPEntity {
	function __construct() {
		$this->cpPostType = SPARK_TUTOR_REVIEW_POST_TYPE;
		$this->fillable = ['rating', 'comment', 'type', 'tutor', 'booking', 'tutor_name', 'student_name', 'course_name', 'booking_date', 'booking_date', 'booking_unique_id'];
		$this->cpPostName = SPARK_TUTOR_REVIEW_POST_NAME;
	}
	public function save_post_data($post_data) {
		return parent::save_post_data($post_data);
	}

}
