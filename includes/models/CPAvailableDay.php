<?php
class CPAvailableDay extends CPEntity {
	function __construct() {
		$this->cpPostType = SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE;
		$this->fillable = ['start_time', 'end_time', 'tutor'];
		$this->cpPostName = SPARK_TUTOR_AVAILABLE_DAY_POST_NAME;
	}
	public function save_post_data($post_data) {
		$weekday = $post_data['weekday'];
		switch ($weekday) {
		case '0':
			$day = 'sunday';
			break;
		case '1':
			$day = 'monday';
			break;
		case '2':
			$day = 'tuesday';
			break;
		case '3':
			$day = 'wednesday';
			break;
		case '4':
			$day = 'thursday';
			break;
		case '5':
			$day = 'friday';
			break;
		case '6':
			$day = 'saturday';
			break;
		default:
			break;
		};
		$available_day_id = parent::save_post_data($post_data);
		wp_set_object_terms($available_day_id, $day, 'weekday', true);
		return $available_day_id;
	}
}