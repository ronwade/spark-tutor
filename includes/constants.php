<?php
define('SPARK_TUTOR_BOOKING_POST_TYPE', 'gu_tut_booking');
define('SPARK_TUTOR_TUTOR_POST_TYPE', 'gu_tut_tutor');
define('SPARK_TUTOR_SUBJECT_POST_TYPE', 'gu_tut_subject');
define('SPARK_TUTOR_COURSE_POST_TYPE', 'gu_tut_course');
define('SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE', 'gu_tut_available_day');
define('SPARK_TUTOR_REVIEW_POST_TYPE', 'gu_tut_review');
define('SPARK_TUTOR_USER_PROFILE_POST_TYPE', 'spark_tutor_user_profile');
define('SPARK_TUTOR_CUSTOM_POST_TYPES', [
    SPARK_TUTOR_BOOKING_POST_TYPE,
    SPARK_TUTOR_TUTOR_POST_TYPE,
    SPARK_TUTOR_SUBJECT_POST_TYPE,
    SPARK_TUTOR_COURSE_POST_TYPE,
    SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE,
    SPARK_TUTOR_REVIEW_POST_TYPE,
    SPARK_TUTOR_USER_PROFILE_POST_TYPE,
]);

define('SPARK_TUTOR_BOOKING_POST_NAME', 'Booking');
define('SPARK_TUTOR_TUTOR_POST_NAME', 'Tutor');
define('SPARK_TUTOR_USER_PROFILE_POST_NAME', 'User Profile');
define('SPARK_TUTOR_SUBJECT_POST_NAME', 'Subject');
define('SPARK_TUTOR_COURSE_POST_NAME', 'Course');
define('SPARK_TUTOR_AVAILABLE_DAY_POST_NAME', 'Available_Day');
define('SPARK_TUTOR_REVIEW_POST_NAME', 'Review');

// Statuses
define('SPARK_TUTOR_STATUS_ACTIVE', 'Active');
define('SPARK_TUTOR_STATUS_REGISTERED', 'Registered');
define('SPARK_TUTOR_STATUS_APPROVED', 'Approved');
define('SPARK_TUTOR_STATUS_REVIEWED', 'Reviewed');
define('SPARK_TUTOR_STATUS_REVIEWING', 'Reviewing');
define('SPARK_TUTOR_STATUS_INACTIVE', 'Inactive');
define('SPARK_TUTOR_STATUS_CONFIRMED', 'Confirmed');
define('SPARK_TUTOR_STATUS_INPROGRESS', 'InProgress');
define('SPARK_TUTOR_STATUS_FINISHED', 'Finished');
define('SPARK_TUTOR_STATUS_CANCELLED', 'Cancelled');
define('SPARK_TUTOR_STATUS_REJECTED', 'Rejected');
define('SPARK_TUTOR_STATUS_PENDING', 'Pending');

define('SPARK_TUTOR_STATUS_REVIEW_LIST', [
    SPARK_TUTOR_STATUS_REVIEWING,
    SPARK_TUTOR_STATUS_INPROGRESS,
    SPARK_TUTOR_STATUS_PENDING
]);

define('SPARK_TUTOR_STATUS_REJECT_LIST', [
    SPARK_TUTOR_STATUS_INACTIVE,
    SPARK_TUTOR_STATUS_CANCELLED,
    SPARK_TUTOR_STATUS_REJECTED
]);

define('SPARK_TUTOR_STATUS_ACTIVE_LIST', [
    SPARK_TUTOR_STATUS_APPROVED,
    SPARK_TUTOR_STATUS_CONFIRMED,
    SPARK_TUTOR_STATUS_REGISTERED,
    SPARK_TUTOR_STATUS_REVIEWED,
    SPARK_TUTOR_STATUS_FINISHED,
    SPARK_TUTOR_STATUS_ACTIVE
]);

define('SPARK_TUTOR_REGISTRATION_STATUS_LIST', [
    SPARK_TUTOR_STATUS_INACTIVE,
    SPARK_TUTOR_STATUS_PENDING,
    SPARK_TUTOR_STATUS_REVIEWING,
    SPARK_TUTOR_STATUS_REJECTED,
    SPARK_TUTOR_STATUS_REVIEWED,
    SPARK_TUTOR_STATUS_ACTIVE
]);

define('SPARK_TUTOR_PAYMENT_METHOD_PAYPAL', 'PayPal');
define('SPARK_TUTOR_PAYMENT_METHOD_STRIPE', 'Stripe');

define('SPARK_TUTOR_TYPE_REVIEW', 'review');
define('SPARK_TUTOR_TYPE_REPORT', 'report');


// Define roles
function get_tutor_role () {
    if ( function_exists('tutor')) {
        return tutor()->instructor_role;
    } else {
        return 'tutor_instructor';
    }
}
define('SPARK_TUTOR_ROLE', get_tutor_role ());
define('SPARK_PARENT_ROLE', 'tutor_instructor');


define('SPARK_TUTOR_ROLE_TITLE', __( 'Tutor Instructor', 'tutor' ));
define('SPARK_PARENT_ROLE_TITLE', __( 'Parent', 'parent' ));


// Spark User Meta
define('SPARK_USER_META_TUTOR', '_spark_tutor_id');
define('SPARK_USER_META_ID', '_spark_user_id');

// Icons
define('SPARK_TUTOR_ICON_URL', SPARK_TUTOR_PLUGIN_URL."public/img/tutor-icon.png");


define('SPARK_TUTOR_VIDEO_SOURCE', [
    // "html5" => "HTML 5 (MP4 Upload)",
    "external_url" => "External URL",
    "youtube" => "YouTube",
    "vimeo" => "Vimeo",
    "embedded" => "Embedded"
]);
