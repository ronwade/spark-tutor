<?php
function spark_get_current_view($postData = null) {
	if (isset($postData)) {
		$view = isset($postData['view']) ? sanitize_text_field($postData['view']) : '';
	} else {
		$view = isset($_GET['view']) ? sanitize_text_field($_GET['view']) : '';
	}
	return $view;
}

function spark_get_current_page($postData = null) {
	if (isset($postData)) {
		$page = isset($postData['page']) ? sanitize_text_field($postData['page']) : 'spark-tutor';
	} else {
		$page = isset($_GET['page']) ? sanitize_text_field($_GET['page']) : 'spark-tutor';
	}
	return $page;
}

function spark_get_view_url($viewName, $id = '', $menuName = '') {
	$url = 'admin.php';
	if (empty($menuName)) {
		$page = isset($_GET['page']) ? sanitize_text_field($_GET['page']) : '';
		if (empty($page)) {
			$url .= '?page=spark-tutor';
		} else {
			$url .= '?page=' . $page;
		}
	} else {
		switch ($menuName) {
		case 'tutors':
		case 'spark-tutor-tutors':
			$url .= '?page=spark-tutor-tutors';
			break;
		case 'subjects':
		case 'spark-tutor-subjects':
			$url .= '?page=spark-tutor-subjects';
			break;
		case 'bookings':
		case 'spark-tutor-bookings':
			$url .= '?page=spark-tutor-bookings';
			break;
		case 'calendar':
		case 'spark-tutor-calendar':
			$url .= '?page=spark-tutor-calendar';
			break;
		case 'reports':
		case 'spark-tutor-reports':
			$url .= '?page=spark-tutor-reports';
			break;
		case 'reviews':
		case 'spark-tutor-reviews':
			$url .= '?page=spark-tutor-reviews';
			break;
		case 'receipts':
		case 'spark-tutor-receipts':
			$url .= '?page=spark-tutor-receipts';
			break;
		case 'analytics':
		case 'spark-tutor-analytics':
			$url .= '?page=spark-tutor-analytics';
			break;
		case 'settings':
		case 'spark-tutor-settings':
			$url .= '?page=spark-tutor-settings';
			break;
		default:
			$url .= '?page=spark-tutor';
			break;
		}
	}
	switch ($viewName) {
	case 'view':
	case 'edit':
		$url .= '&view=' . $viewName;
		if (!empty($id)) {
			$url .= '&id=' . $id;
		}
		break;
	case 'working-time':
		$url .= '&view=' . $viewName;
		if (!empty($id)) {
			$url .= '&id=' . $id;
		}
		break;
	default:
		$url .= '&view=' . $viewName;
		break;
	}
	return admin_url($url);
}

function spark_new_entity($entity_type) {
	$entity = null;
	switch ($entity_type) {
	case SPARK_TUTOR_BOOKING_POST_TYPE:
		$entity = new CPBooking();
		break;
	case SPARK_TUTOR_TUTOR_POST_TYPE:
		$entity = new CPTutor();
		break;
	case SPARK_TUTOR_SUBJECT_POST_TYPE:
		$entity = new CPSubject();
		break;
	case SPARK_TUTOR_COURSE_POST_TYPE:
		$entity = new CPCourse();
		break;
	default:
		$entity = new CPEntity();
		break;
	}
	return $entity;
}


function spark_get_list_entity($postType, $args=array()) {
	$defaults = array(
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
		'meta_key' => '',
		'meta_value' => '',
		'post_type' => $postType,
		'suppress_filters' => true,
	);
	$parsed_args = wp_parse_args($args, $defaults);
	$posts = get_posts($parsed_args);
	$entities = array();
	foreach ($posts as $post) {
		$entity = spark_new_entity($postType);
		$entity->setPost($post);
		array_push($entities, $entity);
	}
	return $entities;
}


function spark_get_list_entity_by_id($postType, $args=array()) {
	$defaults = array(
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
		'meta_key' => '',
		'meta_value' => '',
		'post_type' => $postType,
		'suppress_filters' => true,
	);
	$parsed_args = wp_parse_args($args, $defaults);
	$posts = get_posts($parsed_args);
	$entities = array();
	foreach ($posts as $post) {
		$entity = spark_new_entity($postType);
		$entity->setPost($post);
		$entities[ $entity->getId() ] = $entity;
	}
	return $entities;
}



function get_param_from_array($array, $param, $default='', $sanitize=true) {
	if ( !empty($array[$param]) ) {
		if ( $sanitize ) {
			return sanitize_text_field($array[$param]);
		}
		return $array[$param];
	}
	return $default;
}

function post_url_param($param, $default='', $sanitize=true){
	return get_param_from_array($_POST, $param, $default, $sanitize);
}

function get_url_param($param, $default='', $sanitize=true) {
	return get_param_from_array($_GET, $param, $default, $sanitize);
}

function gu_path_join($base, ...$subpath) {
	if ($base[-1] != DIRECTORY_SEPARATOR) {
		$base .= DIRECTORY_SEPARATOR;
	}
	return $base.(implode(DIRECTORY_SEPARATOR, $subpath));
}

function spark_get_entity_by_id($id) {
	$post = get_post($id);
	if (empty($post)) {
		return new CPEntity();
	}
	$postType = $post->post_type;
	$entity = spark_new_entity($postType);
	$entity->setPost($post);
	return $entity;
}

function spark_generateUniqueId($bookingId = 0) {
	$length = 5;
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return ($bookingId % 10) . $randomString;
}
function spark_caculate_revenue($day = '') {
	if ($day != '') {
		$date_range = strtotime($day);
		$now_date = strtotime('now');
		if ($day = 'this year') {
			$arg = array(
				'date_query' => array(
					array(
						'year' => date('Y', $now_date),
					),
				),
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'status',
						'value' => 'Pending',
						'compare' => '!=',
					),
					array(
						'key' => 'status',
						'value' => 'Cancelled',
						'compare' => '!=',
					),
				),
			);
		} else {
			$arg = array(
				'date_query' => array(
					array(
						'after' => array(
							'year' => date('Y', $date_range),
							'month' => date('m', $date_range),
							'day' => date('d', $date_range),
						),
						'before' => array(
							'year' => date('Y', $now_date),
							'month' => date('m', $now_date),
							'day' => date('d', $now_date),
						),
					),
				),
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'status',
						'value' => 'Pending',
						'compare' => '!=',
					),
					array(
						'key' => 'status',
						'value' => 'Cancelled',
						'compare' => '!=',
					),
				),
			);
		}

	}

	$total_revenue = 0;
	$list_booking = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $arg);
	foreach ($list_booking as $key => $value) {
		$total_revenue += (int) $value->getField('total');
	}
	return $total_revenue;
}
function calculate_time_distance($date2, $date1) {
	$diff = abs($date2 - $date1);

	$years = floor($diff / (365 * 60 * 60 * 24));
	$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
	$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
	$hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
	$minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
	if ($years == 0) {
		$result = $months . " months, " . $days . " days, " . $hours . " hours, " . $minutes . " minutes";
	}
	if ($months == 0) {
		$result = $days . " days, " . $hours . " hours, " . $minutes . " minutes";
	}
	if ($days == 0) {
		$result = $hours . " hours, " . $minutes . " minutes";
	}
	if ($hours == 0) {
		$result = $minutes . " minutes";
	}
	return $result . ' ago';
}
function spark_redirect_to_404_page() {
	global $wp_query;
	$wp_query->set_404();
	status_header(404);
	get_template_part(404);
	exit();
}

function spark_substrwords($text, $maxchar, $end = '...') {

	if (strlen($text) > $maxchar && $text != '') {
		$words = preg_split('/\s/', $text);
		$output = '';
		$i = 0;
		while (1) {
			$length = strlen($output) + strlen($words[$i]);
			if ($length > $maxchar) {
				break;
			} else {
				$output .= " " . $words[$i];
				++$i;
			}
		}
		$output .= $end;
	} else {
		$output = $text;
	}
	return $output;
}
function spark_date_format() {
	$spark_tutor_options = get_option(SPARK_TUTOR_OPTIOM);
	if (empty($spark_tutor_options['date-format']) || $spark_tutor_options['date-format'] == 'MM-DD-YYYY') {
		$date_format = 'm-d-Y';
	} elseif ($spark_tutor_options['date-format'] == 'DD-MM-YYYY') {
		$date_format = 'd-m-Y';
	} elseif ($spark_tutor_options['date-format'] == 'YYYY-MM-DD') {
		$date_format = 'Y-m-d';
	}
	return $date_format;
}

function spark_render_text($number, $text) {
	if ($number > 1) {
		if (substr($text, -1, 1) == 'o' || substr($text, -1, 1) == 's' || substr($text, -1, 1) == 'x' || substr($text, -1, 1) == 'z' || substr($text, -2, 2) == 'sh' || substr($text, -2, 2) == 'ch') {
			return $text . 'es';
		} else {
			return $text . 's';
		}

	} else {
		return $text;
	}
}
function spark_shapeSpace_allowed_html() {

	$allowed_tags = array(
		'a' => array(
			'class' => array(),
			'href' => array(),
			'rel' => array(),
			'title' => array(),
		),
		'abbr' => array(
			'title' => array(),
		),
		'b' => array(),
		'blockquote' => array(
			'cite' => array(),
		),
		'cite' => array(
			'title' => array(),
		),
		'code' => array(),
		'del' => array(
			'datetime' => array(),
			'title' => array(),
		),
		'dd' => array(),
		'div' => array(
			'class' => array(),
			'title' => array(),
			'style' => array(),
		),
		'dl' => array(),
		'dt' => array(),
		'em' => array(),
		'h1' => array(),
		'h2' => array(),
		'h3' => array(),
		'h4' => array(),
		'h5' => array(),
		'h6' => array(),
		'i' => array(),
		'img' => array(
			'alt' => array(),
			'class' => array(),
			'height' => array(),
			'src' => array(),
			'width' => array(),
		),
		'li' => array(
			'class' => array(),
		),
		'ol' => array(
			'class' => array(),
		),
		'p' => array(
			'class' => array(),
		),
		'q' => array(
			'cite' => array(),
			'title' => array(),
		),
		'span' => array(
			'class' => array(),
			'title' => array(),
			'style' => array(),
		),
		'strike' => array(),
		'strong' => array(),
		'ul' => array(
			'class' => array(),
		),
	);

	return $allowed_tags;
}

function spark_get_duration_list() {
	return ['45' => '45 mins'];
}

function spark_get_default_duration() {
	return '45';
}

function spark_get_default_price() {
	return '20';
}

function spark_disable_custom_price() {
	return True;
}

function spark_get_contact_email($html=false, $htmlAttr="") {
	$email = "hello@spark-education.com";
	if ($html) {
		return "<a href='mailto:$email' $htmlAttr >$email</a>";
	}
	return $email;
}

function spark_get_contact_number($html=false, $htmlAttr="") {
	$number = "876-422-9566";
	if ($html) {
		return "<a href='tel:$number' $htmlAttr >$number</a>";
	}
	return $number;
}

function spark_bcc_support(){
	return "spark@osoobe.com, brittany@spark-education.com";
}

function spark_manager_role() {
	return "administrator";
}

function spark_access_token () {
	return "xUuyV6l87DHbmYe";
}

function clear_data () {
	foreach(SPARK_TUTOR_CUSTOM_POST_TYPES as $type) {
		$allposts= get_posts( array('post_type'=> $type,'numberposts'=>-1) );
		foreach ($allposts as $eachpost) {
			wp_delete_post( $eachpost->ID, true );
		}
	}
}

function tutor_dashboard_url() {
	return get_home_url()."/dashboard";
}	

function tutor_myprofile_url() {
	return get_home_url()."/dashboard/my-profile/";
}

function tutor_onboarding_course_url($absolute=false) {
	if ( isset($spark_tutor_options['tutor_onboarding_course'])) {
		$path = $spark_tutor_options['tutor_onboarding_course'];
	 } else {
		$path = "/courses/tutor-onboarding/";
	 }
	 if ( ! $absolute ) {
		return $path;
	 }
	 return get_home_url().$path;
}


function remove_general_links_dashboard($links){
	unset($links['reviews']);
	unset($links['wishlist']);
	return $links;
}

function add_general_links_dashboard($links){
	$links['profiles'] = __('Manage Profiles', 'profiles');
	$links['my-profile'] = __('My Profile', 'my-profile');
	$links['manage-profiles'] = __('Manage Profiles', 'manage-profiles');
	return $links;
}

function remove_instructor_links_dashboard($links){
	return $links;
}

function add_instructor_links_dashboard($links){
	$links['live-courses'] = array('title' => __('Schedule Live Session', 'tutor'), 'auth_cap' => tutor()->instructor_role);
	$links['student-bookings'] = array('title' => __('Student Bookings', 'tutor'), 'auth_cap' => tutor()->instructor_role);
	return $links;
}

function get_new_tutor_lms_links($key, $absolute=false) {
	$endpoints = [
		"live-courses" => "live-courses",
		"student-bookings" => "student-bookings",
		"my-courses" => "my-courses",
	];
	$link = $endpoints[$key];
	if ( ! $absolute ) {
		return $link;
	}
	return get_home_url()."/dashboard/$link/";
}

function get_pic_or_default($pic) {
	return (!empty($pic) && $pic != 'no file')? $pic : SPARK_TUTOR_PLUGIN_PUBLIC_IMG_URL."placeholder.jpg";
}


if ( ! function_exists('spark_user_profile')) {
	function spark_user_profile() {

		$user = wp_get_current_user();
        if ( $user->exists() ) {
            return CPUserProfile::get_profile($user);
        }

        return null;
	}
}

function user_or_id($user) {
	if ( is_int($user) ) {
		$user = new \WP_User($user);
	}
	return $user;
}

function spark_registration_step($status) {
	$step = 1;
	if ( in_array($status, SPARK_TUTOR_STATUS_REVIEW_LIST) ) {
		return 1;
	}

	if ($status == SPARK_TUTOR_STATUS_APPROVED || $status == SPARK_TUTOR_STATUS_REVIEWED) {
		return 2;
	}

	if (in_array($status, SPARK_TUTOR_STATUS_ACTIVE_LIST)) {
		return 3;
	}
	return $step;
}

function get_spark_registration_step_image($status) {
	$step = spark_registration_step($status);
	if ($step > 3 || $step < 1) {
		return 'spark-step3.png';
	}
	return "spark-step$step.png";
}

function build_tutor_email_template($user, $template_file) {
	$username = $user->get('user_login');
	$email = $user->get("user_email");
	$last_name = $user->get("last_name");
	$first_name = $user->get("first_name");
	$tutor_id = $user->get('_spark_tutor_id');
	$tutor = spark_get_entity_by_id($tutor_id);
	$registration_status = $tutor->getField('status');
	$website = get_home_url();
	$asset_folder = $website . '/wp-content/uploads/assets';

	$template = new Template(SPARK_TUTOR_PLUGIN_PATH . "includes/email_templates/$template_file");
	$template->set('email', $email);
	$template->set('first_name', $first_name);
	$template->set('last_name', $last_name);
	$template->set('username', $username);
	$template->set('tutor_id', $tutor_id);
	$template->set('user_id', $user->ID);
	$template->set('website', $website);
	$template->set('registration_status', $registration_status);
	$template->set('image_status', get_spark_registration_step_image($registration_status));
	$template->set('asset_folder', $asset_folder);
	$template->set('spark_asset_folder', SPARK_TUTOR_PLUGIN_PUBLIC_URL);
	$template->set('profile_code', CPTutor::gen_profile_hash($email, $username, $tutor_id));
	$template->set('tutor_dashboard_url', tutor_dashboard_url());
	$template->set('tutor_myprofile_url', tutor_myprofile_url());
	$template->set('tutor_onboarding_course_url', tutor_onboarding_course_url(true));
	return $template;
}


/**
 * General HTML form for video source.
 * @param string $source=''			Video source: youtube|vimeo|html5 
 * @param string $url=''			URL of the video
 * @param mixed $required=false		Make the field required
 * 
 * @return void
 */
function generate_tutor_intro_video_source_selector_html($source='', $url='', $required=false) {

	$required = ($required)? 'required': '';
?>


	<h5 class="mt-4"><?php echo esc_html__('Video', 'spark-tutor') ?></h5>
	<p>
		Please submit a 2-minute explainer video teaching us something you are really great at.
		Whether it is baking a cake or tackling algebraic expressions,
		we want to get a sense of how you share knowledge!
		<br>
		<br>
		Create a video on your phone. Upload it to a platform like YouTube or Vimeo and share the link here.
	</p>
	<label for="intro_video_url">Introduction Video</label>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<select <?php echo $required; ?> name="intro_video_source" id="intro_video_source" class="form-control tutor_intro_video_source">
					<option value="" >Select Video Source</option>
					<?php
						foreach(SPARK_TUTOR_VIDEO_SOURCE as $key => $text) {
							$selected = ($source == $key)? 'selected' : '';
							echo "<option value='$key' $selected >$text</option>";
						}
					?>
				</select>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<input type="url" id="intro_video_url" name="intro_video_url" t <?php echo $required; ?>  value="<?php echo $url; ?>" class="form-control" placeholder="External Video URL. E.g. YouTube or Vimeo URL">
			</div>
		</div>
	</div>

<?php
}


function render_tutor_intro_video_html($source='', $url='') {
	if ($source == 'youtube') { 
		if ( strpos($url, "v=") !== false ) {
			$params = parse_url($url, PHP_URL_QUERY);
			parse_str($params, $query);
			$youtube_id = $query['v'];
		} else {
			$youtube_id = ltrim(parse_url($url, PHP_URL_PATH), '/');
		}
		$youtube_embed_url = "https://www.youtube.com/embed/$youtube_id?&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1";

		?>
			<iframe class="intro-video" style="width: 100%" src="<?php echo $youtube_embed_url; ?>" allowfullscreen allowtransparency ></iframe>
	<?php }
}