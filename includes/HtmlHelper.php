<?php
class HtmlHelper {
	public static function render_select($list, $attributes, $selectedValue = null) {
		$selectHtml = '<select ';

		foreach ($attributes as $key => $value) {
			$selectHtml .= " $key='$value' ";
		}
		$selectHtml .= ' >';
		$selectHtml .= "<option value=''>---" . esc_html__('Select', 'spark-tutor') . "---</option>";
		foreach ($list as $value => $title) {
			$selected = $selectedValue == $value ? ' selected ' : '';
			$selectHtml .= "<option value='$value' $selected>$title</option>";
		}
		$selectHtml .= '</select>';
		return $selectHtml;
	}
	public static function render_delete_modal($id) {
		$template = new Template('html_templates/delete_modal.php');
		$template->set('id', $id);
		$modalHtml = $template->render();
		return $modalHtml;
	}
	public static function render_badge_label($status) {
		$btnClass = 'secondary';
		switch ($status) {
		case SPARK_TUTOR_STATUS_CONFIRMED:
		case SPARK_TUTOR_STATUS_ACTIVE:
			$btnClass = 'primary';
			break;
		case SPARK_TUTOR_STATUS_INPROGRESS:
			$btnClass = 'info';
			break;
		case SPARK_TUTOR_STATUS_INACTIVE:
			$btnClass = 'danger';
			break;
		case SPARK_TUTOR_STATUS_REJECTED:
			$btnClass = 'danger';
			break;
		case SPARK_TUTOR_STATUS_FINISHED:
			$btnClass = 'success';
			break;
		// case SPARK_TUTOR_STATUS_CANCELLED:
		case SPARK_TUTOR_STATUS_PENDING:
			$btnClass = 'warning';
			break;
		case SPARK_TUTOR_STATUS_REVIEWING:
			$btnClass = 'warning';
			break;
		case SPARK_TUTOR_STATUS_APPROVED:
			$btnClass = 'success';
			break;
		case SPARK_TUTOR_STATUS_REVIEWED:
			$btnClass = 'success';
			break;
		default:
			break;
		}
		return "<span class='badge badge-$btnClass'>$status</span>";
	}
	public static function render_rating_stars($rating) {
		$html = "<span class='star-color'>";
		for ($i = 1; $i <= $rating; $i++) {
			$html .= "<i class='fa fa-star'></i>";
		}
		for ($i = intval($rating); $i <= 5; $i++) {
			$html .= "<i class='fa fa-star-o'></i>";
		}
		$html .= "</span>";
		return $html;
	}
	public static function render_report_modal() {
		$template = new Template('html_templates/report_modal.php');
		$modalHtml = $template->render();
		return $modalHtml;
	}
	public static function render_review_modal() {
		$template = new Template('html_templates/review_modal.php');
		$modalHtml = $template->render();
		return $modalHtml;
	}
}
