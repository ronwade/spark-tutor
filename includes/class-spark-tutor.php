<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://osoobe.com
 * @since      1.0.0
 *
 * @package    Spark_Tutor
 * @subpackage Spark_Tutor/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Spark_Tutor
 * @subpackage Spark_Tutor/includes
 * @author     Oshane Bailey <b4.oshany@gmail.com>
 */
class Spark_Tutor {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Spark_Tutor_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'SPARK_TUTOR_VERSION' ) ) {
			$this->version = SPARK_TUTOR_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'spark-tutor';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->generate_rewrite_rules();
		$this->tutor_query_vars();

		// if(get_transient( 'sparktutor_flush' )) {
		// 	delete_transient( 'sparktutor_flush' );
		// 	flush_rewrite_rules();
		// }

	}

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		$version = get_option('spark_tutor_version');

		set_transient( 'sparktutor_flush', 1, 60 );
		// flush_rewrite_rules();

		register_taxonomy('weekday', 'gu_tut_available_day', array(
			'label' => 'Weekday',
			'labels' => array(
				'name' => esc_html__('Weekday', 'spark-tutor'),
				'singular_name' => esc_html__('Weekday', 'spark-tutor'),
			),
			'public' => true,
			'hierarchical' => true,

		));
		if (term_exists('monday', 'weekday', null) == null) {
			wp_insert_term('monday', 'weekday');
		}
		if (term_exists('tuesday', 'weekday', null) == null) {
			wp_insert_term('tuesday', 'weekday');
		}
		if (term_exists('wednesday', 'weekday', null) == null) {
			wp_insert_term('wednesday', 'weekday');
		}
		if (term_exists('thursday', 'weekday', null) == null) {
			wp_insert_term('thursday', 'weekday');
		}
		if (term_exists('friday', 'weekday', null) == null) {
			wp_insert_term('friday', 'weekday');
		}
		if (term_exists('saturday', 'weekday', null) == null) {
			wp_insert_term('saturday', 'weekday');
		}
		if (term_exists('sunday', 'weekday', null) == null) {
			wp_insert_term('sunday', 'weekday');
		}


		$tutor = get_role( SPARK_TUTOR_ROLE );
		if ( ! $tutor ) {
			// remove_role(SPARK_TUTOR_ROLE);
			add_role(SPARK_TUTOR_ROLE, __(SPARK_TUTOR_ROLE_TITLE, SPARK_TUTOR_ROLE), array());
		}

		static::settings_init();
		//Save Option
		if ( ! $version ){
			//Rewrite Flush
			update_option('spark_tutor_version', SPARK_TUTOR_VERSION);
		} else {
			update_option('spark_tutor_version', SPARK_TUTOR_VERSION);
		}

		self::create_uploads_directory();
	}

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		flush_rewrite_rules();
	}

	private static function settings_init() {
		$spark_tutor_options = get_option('spark_tutor_option');
		if (empty($spark_tutor_options)) {
			$spark_tutor_options = array();
		}
		$defaults = array(
			'product-description' => '',
			'tax' => '',
			'date-format' => 'MM-DD-YYYY',
			'term_condition_content' => '',
			'booking-page'=> '',
			//payment
			'enabled-paypal' => '',
			'paypal-mode' => '1',
			'paypal-email' => '',
			'enabled_stripe' => '',
			'stripe_secret_key' => '',
			'stripe_public_key' => '',
			//admin email
			'enabled_admin_email' => '1',
			'admin_email_subject' => 'Tutor Booking on Spark Education',
			'admin_email_heading' => '',
			'admin_email' => 'hello@spark-education.com',
			//tutor email
			'enabled_tutor_email' => '1',
			'tutor_email_subject' => 'Tutor Booking on Spark Education',
			'tutor_email_heading' => '',
			//student email
			'enabled_student_email' => '1',
			'student_email_subject' => 'Tutor Booking on Spark Education',
			'student_email_heading' => '',
			//host
			'host' => 'mail.jamaicans.dev',
			'from_email' => 'hello@spark-education.com',
			'from_name' => 'Spark Education',
			'smtp_username' => 'mailer@jamaicans.dev',
			'smtp_password' => '8G$g4rBIwi2HN$kje',
			'smtp_encryption' => '2',
			'port' => '465',
			'test_email' => ''
		);

		foreach($defaults as $key => $val) {
			if(!isset($spark_tutor_options[$key])) {
				$spark_tutor_options[$key] = $val;
			}
		}
		update_option('spark_tutor_option', $spark_tutor_options);
	}

	public function customize_tutor_lms() {
		add_filter('tutor_dashboard/nav_items', 'remove_general_links_dashboard');
		add_filter('tutor_dashboard/nav_items', 'add_general_links_dashboard');
		add_filter('tutor_dashboard/instructor_nav_items', 'remove_instructor_links_dashboard');
		add_filter('tutor_dashboard/instructor_nav_items', 'add_instructor_links_dashboard');
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Spark_Tutor_Loader. Orchestrates the hooks of the plugin.
	 * - Spark_Tutor_i18n. Defines internationalization functionality.
	 * - Spark_Tutor_Admin. Defines all hooks for the admin area.
	 * - Spark_Tutor_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-spark-tutor-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-spark-tutor-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-spark-tutor-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-spark-tutor-public.php';

		$this->loader = new Spark_Tutor_Loader();

	}

	public function generate_rewrite_rules( ) {
		add_filter( 'generate_rewrite_rules', function ( $wp_rewrite ){
			$wp_rewrite->rules = array_merge(
				[
					'gtutor-registration/?$' => 'index.php?gp_type=' . SPARK_TUTOR_TUTOR_POST_TYPE . '&gp_action=register',
					'tutor-registration-process' => 'index.php?gp_type=' . SPARK_TUTOR_TUTOR_POST_TYPE . '&gp_action=register',
					'book-a-tutor/?$' => 'index.php?gp_type=' . SPARK_TUTOR_BOOKING_POST_TYPE . '&gp_action=register',
					'tutor_profile_check?$' => 'index.php?gp_type=' . SPARK_TUTOR_TUTOR_POST_TYPE . '&gp_action=status_check',
					'create-live-courses?$' => 'index.php?gp_type=' . SPARK_TUTOR_COURSE_POST_TYPE . '&gp_action=create-course',
					'edit-live-courses' => 'index.php?gp_type=' . SPARK_TUTOR_COURSE_POST_TYPE . '&gp_action=edit-course',
					'delete-live-courses' => 'index.php?gp_type=' . SPARK_TUTOR_COURSE_POST_TYPE . '&gp_action=delete-course',
				],
				$wp_rewrite->rules
			);
		});
	}

	public function tutor_query_vars() {
		add_filter( 'query_vars', function( $query_vars ){
			$query_vars[] = 'gp_type';
			$query_vars[] = 'gp_action';
			return $query_vars;
		});

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Spark_Tutor_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Spark_Tutor_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Spark_Tutor_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Spark_Tutor_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_action( 'template_include', $plugin_public, 'load_public_templates' );
		$this->loader->add_action( 'template_redirect', $plugin_public, 'redirect_public_endpoints' );
		// $this->loader->add_action( 'customize_tutor_lms', $plugin_public, 'customize_tutor_lms' );
		$this->customize_tutor_lms();

	}


	/**flo
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Spark_Tutor_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	private static function create_uploads_directory() {

		if( !@file_exists(SPARK_TUTOR_PLUGIN_UPLOAD_PATH) ) {
			mkdir(SPARK_TUTOR_PLUGIN_UPLOAD_PATH);

			$src_file = SPARK_TUTOR_PLUGIN_ADMIN_DIR
			             . DIRECTORY_SEPARATOR . 'config'
			             . DIRECTORY_SEPARATOR . '.htaccess';

			$dest_file = SPARK_TUTOR_PLUGIN_UPLOAD_PATH . '.htaccess';

			@copy($src_file, $dest_file);
		}
	}
}
