<?php
class SubjectServices {
	public static function search_subject($keyword) {
		$args = [
			'post_type' => SPARK_TUTOR_SUBJECT_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'name',
					'value' => $keyword,
					'compare' => 'LIKE',
				),

			),
		];
		$subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, $args);
		return $subjects;
	}
	public static function get_subject_by_name($name) {
		$args = [
			'post_type' => SPARK_TUTOR_SUBJECT_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'name',
					'value' => $name,
					'compare' => '=',
				),

			),
		];
		$subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, $args);
		return $subjects;
	}
}
