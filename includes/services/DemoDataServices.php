<?php
class DemoDataServices {
	public static function insert_demo_data() {
		// $allsubjects = get_posts(array('post_type' => SPARK_TUTOR_SUBJECT_POST_TYPE, 'numberposts' => -1));
		// foreach ($allsubjects as $subject) {
		// 	wp_delete_post($subject->ID, true);
		// }
		// DemoDataServices::insert_subject_data();

		// $alltutors = get_posts(array('post_type' => SPARK_TUTOR_TUTOR_POST_TYPE, 'numberposts' => -1));
		// foreach ($alltutors as $tutor) {
		// 	wp_delete_post($tutor->ID, true);
		// }
		// DemoDataServices::insert_tutor_data();

		// $allcourses = get_posts(array('post_type' => SPARK_TUTOR_COURSE_POST_TYPE, 'numberposts' => -1));
		// foreach ($allcourses as $course) {
		// 	wp_delete_post($course->ID, true);
		// }
		// DemoDataServices::insert_course_data();
		// DemoDataServices::update_course_data();
		// DemoDataServices::update_tutor_subjects();
		DemoDataServices::update_tutor_user();

		$allbookings = get_posts(array('post_type' => SPARK_TUTOR_BOOKING_POST_TYPE, 'numberposts' => -1));
		foreach ($allbookings as $booking) {
			wp_delete_post($booking->ID, true);
		}
		// DemoDataServices::insert_booking_data();
	
		$availableDays = get_posts(array('post_type' => SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE, 'numberposts' => -1));
		foreach ($availableDays as $availableDay) {
			wp_delete_post($availableDay->ID, true);
		}
		DemoDataServices::insert_available_day();

	}
	public static function insert_available_day() {
		$tutors = TutorServices::search_tutors_by_status(null, null);
		foreach ($tutors as $tutor) {
			$data = array();
			$data['start_time'] = 7 * 60;
			$data['end_time'] = 17 * 60;
			$data['status'] = 'Active';
			$data['tutor'] = $tutor->getField('ID');
			for ($i = 0; $i < 7; $i++) {
				$data['weekday'] = $i;
				$ad = new CPAvailableDay();
				$ad->save_post_data($data);
			}
		}
	}

	public static function update_tutor_user() {
		$tutors = TutorServices::search_tutors_by_status(null, null);
		foreach ($tutors as $tutor) {
			$email = $tutor->getField("email");
			if ( !empty($email) ) {
				$user = get_user_by('email', $email );
				CPTutor::update_spark_tutor_user_id($user->ID, $tutor->getId());
			}
		}
	}

	public static function update_tutor_subjects() {
		$tutors = TutorServices::search_tutors_by_status(null, null);
		foreach ($tutors as $tutor) {
			$courses = CourseServices::get_courses_by_tutor_id($tutor->getField('ID'));
			$subjects = array();
			foreach ($courses as $course) {
				array_push($subjects, $course->getField('subject'));
			}
			$subjects = array_unique($subjects);
			update_post_meta($tutor->getField('ID'), 'subject', $subjects);
		}
	}

	public static function insert_booking_data() {
		$bookingsArray = DemoDataServices::csv_file_to_array(SPARK_TUTOR_PLUGIN_PATH . DIRECTORY_SEPARATOR . 'demo-data' . DIRECTORY_SEPARATOR . 'Bookings.csv');
		foreach ($bookingsArray as $value) {
			$booking = new CPBooking();
			$subject = SubjectServices::get_subject_by_name($value[5])[0];
			$tutor = TutorServices::get_tutor_by_name($value[4])[0];
			$course = CourseServices::get_courses_by_name($value[6])[0];
			$price = intval($course->getField('price'));
			$tax = $price * 0.1;
			$total = $price + $tax;
			$booking_date = date_create($value[7]);
			$booking_date = date_format($booking_date, "Y-m-d H:i:s");
			$date = strtotime($value[7]);
			$hour = date('H', $date);

			$mins = date('i', $date);

			$start_time = intval($hour) * 60 + intval($mins);
			$end_time = $start_time + intval($course->getField('duration'));
			$unique_id = spark_generateUniqueId(0);

			$bookingData = ['student_name' => $value[0], 'student_phone' => $value[1], 'student_email' => $value[2], 'student_grade' => $value[3], 'tutor' => $tutor->getField('ID'), 'subject' => $subject->getField('ID'), 'course' => $course->getField('ID'), 'booking_date' => $booking_date,
				'status' => $value[8], 'note' => $value[9], 'payment_method' => $value[10], 'price' => $course->getField('price'), 'duration' => $course->getField('duration'), 'tax' => $tax, 'total' => $total, 'start_time' => $start_time, 'end_time' => $end_time];
			$bookingId = $booking->save_post_data($bookingData);
		}
	}

	public static function insert_tutor_data() {
		$tutorsArray = DemoDataServices::csv_file_to_array(SPARK_TUTOR_PLUGIN_PATH . DIRECTORY_SEPARATOR . 'demo-data' . DIRECTORY_SEPARATOR . 'Tutors.csv');
		foreach ($tutorsArray as $value) {
			$tutor = new CPTutor();

			$tutorData = ['name' => $value[0], 'picture' => site_url() . '/wp-content/plugins/spark-tutor/demo-data/images/' . $value[1] . '.jpeg', 'email' => $value[2], 'phone' => $value[3], 'trn_ssn' => $value[4], 'intro' => $value[5]];
			$tutorId = $tutor->save_post_data($tutorData);
		}
	}
	public static function insert_course_data() {
		$coursesArray = DemoDataServices::csv_file_to_array(SPARK_TUTOR_PLUGIN_PATH . DIRECTORY_SEPARATOR . 'demo-data' . DIRECTORY_SEPARATOR . 'Courses.csv');
		foreach ($coursesArray as $value) {
			$course = new CPCourse();
			$subject = SubjectServices::get_subject_by_name($value[0])[0];
			$tutor = TutorServices::get_tutor_by_name($value[4])[0];
			$courseData = ['subject' => $subject->getField('ID'), 'tutor' => $tutor->getField('ID'), 'name' => $value[1], 'duration' => $value[2], 'price' => $value[3]];
			$courseId = $course->save_post_data($courseData);
		}
	}
	public static function update_course_data() {
		$allcourses = get_posts(array('post_type' => SPARK_TUTOR_COURSE_POST_TYPE, 'numberposts' => -1));
		foreach ($allcourses as $coursePost) {
			$course = new CPCourse();
			$course->setPost($coursePost);
			$subject = spark_get_entity_by_id($course->getField('subject'));
			$pictureName = preg_replace("/\s+/", "", $subject->getField('name')) . '_' . random_int(1, 5) . '.jpg';

			update_post_meta($course->getField('ID'), 'description', esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'spark-tutor'));
			update_post_meta($course->getField('ID'), 'detail', esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'spark-tutor'));
			update_post_meta($course->getField('ID'), 'thumbnail', site_url() . '/wp-content/plugins/spark-tutor/demo-data/images/' . $pictureName);

		}

	}

	public static function insert_subject_data() {
		$subjectsArray = ['Geography', 'French', 'Literature', 'History', 'English', 'Spanish', 'Logic', 'Science', 'Computer Science', 'Math'];
		foreach ($subjectsArray as $value) {
			$subject = new CPSubject();
			$data = ['name' => $value];
			$subject_id = $subject->save_post_data($data);
		}
	}

	public static function csv_file_to_array($path) {
		$row = 1;
		$result = array();
		if (($handle = fopen($path, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				$row++;
				$result[] = $data;
			}
			fclose($handle);
		}
		return $result;
	}
}
