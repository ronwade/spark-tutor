<?php
class TutorServices {
	public static function search_tutors_by_status($keyword, $status) {
		$meta_query_array = array(
			'relation' => 'AND',
			array(
				'relation' => 'OR',
				array(
					'key' => 'name',
					'value' => $keyword,
					'compare' => 'LIKE',
				),
				array(
					'key' => 'email',
					'value' => $keyword,
					'compare' => 'LIKE',
				),
				array(
					'key' => 'phone',
					'value' => $keyword,
					'compare' => 'LIKE',
				),
			),
		);
		if (isset($status)) {
			array_push($meta_query_array, array(
				'key' => 'status',
				'value' => $status,
				'compare' => '=',
			));
		}

		$args = [
			'post_type' => SPARK_TUTOR_TUTOR_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => $meta_query_array,
		];
		$tutors = spark_get_list_entity(SPARK_TUTOR_TUTOR_POST_TYPE, $args);
		return $tutors;
	}

	public static function get_tutor_by_name($email) {
		$args = [
			'post_type' => SPARK_TUTOR_TUTOR_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'email',
					'value' => $email,
					'compare' => '=',
				),

			),
		];
		$tutors = spark_get_list_entity(SPARK_TUTOR_TUTOR_POST_TYPE, $args);
		return $tutors;
	}

	public static function get_tutor_by_email($email) {
		$args = [
			'post_type' => SPARK_TUTOR_TUTOR_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'email',
					'value' => $email,
					'compare' => '=',
				),

			),
		];
		$tutors = spark_get_list_entity(SPARK_TUTOR_TUTOR_POST_TYPE, $args);
		return $tutors;
	}

	public static function get_tutor_by_subject($subjectId) {
		$args = [
			'post_type' => SPARK_TUTOR_TUTOR_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'subject',
					'value' => serialize(strval($subjectId)),
					'compare' => 'LIKE',
				),

			),
		];
		$tutors = spark_get_list_entity(SPARK_TUTOR_TUTOR_POST_TYPE, $args);
		return $tutors;
	}
}
