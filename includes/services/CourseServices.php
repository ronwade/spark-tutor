<?php
class CourseServices {
	public static function get_courses_by_tutor_id($tutorId, $callback='spark_get_list_entity') {
		$args = [
			'post_type' => SPARK_TUTOR_COURSE_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'tutor',
					'value' => $tutorId,
					'compare' => '=',
				),

			),
		];
		$courses = call_user_func( $callback, SPARK_TUTOR_COURSE_POST_TYPE, $args);
		return $courses;
	}

	public static function get_course_by_tutor_id($courseId, $tutorId) {
		$args = [
			'post_type' => SPARK_TUTOR_COURSE_POST_TYPE,
			'posts_per_page' => -1,
			'p' => $courseId,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'tutor',
					'value' => $tutorId,
					'compare' => '=',
				)
			),
		];
		$courses = spark_get_list_entity(SPARK_TUTOR_COURSE_POST_TYPE, $args);
		if ( empty($courses) ) {
			return null;
		}
		return $courses[0];
	}

	public static function get_courses_by_subject_id($subjectId) {
		$args = [
			'post_type' => SPARK_TUTOR_COURSE_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'subject',
					'value' => $subjectId,
					'compare' => '=',
				),

			),
		];
		$courses = spark_get_list_entity(SPARK_TUTOR_COURSE_POST_TYPE, $args);
		return $courses;
	}

	public static function get_courses_by_name($name) {
		$args = [
			'post_type' => SPARK_TUTOR_COURSE_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'name',
					'value' => $name,
					'compare' => '=',
				),

			),
		];
		$courses = spark_get_list_entity(SPARK_TUTOR_COURSE_POST_TYPE, $args);
		return $courses;
	}

}
