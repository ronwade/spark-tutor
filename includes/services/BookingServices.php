<?php
class BookingServices {
	public static function get_bookings_by_subject_id($subjectId) {
		$args = [
			'post_type' => SPARK_TUTOR_BOOKING_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'subject',
					'value' => $subjectId,
					'compare' => '=',
				),

			),
		];
		$bookings = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $args);
		return $bookings;
	}

	public static function get_booking_by_unique_id($unique_id) {
		$args = [
			'post_type' => SPARK_TUTOR_BOOKING_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'unique_id',
					'value' => $unique_id,
					'compare' => '=',
				),

			),
		];
		$bookings = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $args);
		if (count($bookings) > 0) {
			return $bookings[0];
		}

		return null;
	}

	public static function search_booking_by_status($keyword, $status) {
		$meta_query_array = array(
			'relation' => 'AND',
			array(
				'relation' => 'OR',
				array(
					'key' => 'unique_id',
					'value' => $keyword,
					'compare' => 'LIKE',
				),
			),
		);
		if (isset($status)) {
			array_push($meta_query_array, array(
				'key' => 'status',
				'value' => $status,
				'compare' => '=',
			));
		} else {
			array_push($meta_query_array, array(
				'relation' => 'OR',
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_CONFIRMED,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_INPROGRESS,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_FINISHED,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_CANCELLED,
					'compare' => '=',
				),
			));
		}

		$args = [
			'post_type' => SPARK_TUTOR_BOOKING_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => $meta_query_array,
		];
		$bookings = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $args);
		return $bookings;
	}

	public static function get_bookings_for_receipt() {
		$meta_query_array = array(
			array(
				'relation' => 'OR',
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_CONFIRMED,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_INPROGRESS,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_FINISHED,
					'compare' => '=',
				),
			),
		);
		$args = [
			'post_type' => SPARK_TUTOR_BOOKING_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => $meta_query_array,
		];
		$bookings = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $args);
		return $bookings;
	}

	public static function count_booking_by_status($status, $tutor_id=null) {
		if (!empty($status)) {
			$meta_query_array = array(
				'relation' => 'AND',
				array(
					'key' => 'status',
					'value' => $status,
					'compare' => '=',
				),
			);
			if ( isset($tutor_id) ) {
				array_push($meta_query_array, array(
					'key' => 'tutor',
					'value' => $tutor_id,
					'compare' => '=',
				));
			}
		} else {
			$meta_query_array = array(
				'relation' => 'OR',
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_CONFIRMED,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_INPROGRESS,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_FINISHED,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_CANCELLED,
					'compare' => '=',
				),
			);
			if ( isset($tutor_id) ) {
				$meta_query_array = array(
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'tutor',
							'value' => $tutor_id,
							'compare' => '=',
						),
						$meta_query_array
					)
				);
			}
		}

		$args = [
			'post_type' => SPARK_TUTOR_BOOKING_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => $meta_query_array,
		];

		$wp_query = new WP_Query($args);
		$count = $wp_query->found_posts;
		return $count;
	}

	public static function get_total_by_tutor_id($tutor_id) {
		$args = [
			'post_type' => SPARK_TUTOR_BOOKING_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'tutor',
					'value' => $tutor_id,
					'compare' => '=',
				),
				array(
					'relation' => 'OR',
					array(
						'key' => 'status',
						'value' => SPARK_TUTOR_STATUS_CONFIRMED,
						'compare' => '=',
					),
					array(
						'key' => 'status',
						'value' => SPARK_TUTOR_STATUS_INPROGRESS,
						'compare' => '=',
					),
					array(
						'key' => 'status',
						'value' => SPARK_TUTOR_STATUS_FINISHED,
						'compare' => '=',
					),
					array(
						'key' => 'status',
						'value' => SPARK_TUTOR_STATUS_CANCELLED,
						'compare' => '=',
					),
				),

			),
		];
		$bookings = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $args);
		$total = 0;
		if (count($bookings) > 0) {
			foreach ($bookings as $booking) {
				$total += $booking->getField('price');
			}
		}
		return $total;
	}

	public static function get_bookings_by_tutor_id($tutor_id, $status=null) {

		if (isset($status)) {
			$status_query = array(
				'key' => 'status',
				'value' => $status,
				'compare' => '=',
			);
		} else {
			$status_query = array(
				'relation' => 'OR',
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_CONFIRMED,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_INPROGRESS,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_FINISHED,
					'compare' => '=',
				),
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_CANCELLED,
					'compare' => '=',
				),
			);
		}

		$args = [
			'post_type' => SPARK_TUTOR_BOOKING_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'tutor',
					'value' => $tutor_id,
					'compare' => '=',
				),
				$status_query
			),
		];
		$bookings = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $args);
		return $bookings;
	}

}
