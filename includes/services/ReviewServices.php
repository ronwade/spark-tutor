<?php
class ReviewServices {
	public static function get_review_by_tutor_id_and_type($tutorId, $type) {
		$args = [
			'post_type' => SPARK_TUTOR_REVIEW_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'tutor',
					'value' => $tutorId,
					'compare' => '=',
				),
				array(
					'key' => 'type',
					'value' => $type,
					'compare' => '=',
				),

			),
		];
		$reviews = spark_get_list_entity(SPARK_TUTOR_REVIEW_POST_TYPE, $args);
		return $reviews;
	}

	public static function get_reviews_by_type($type) {
		$args = [
			'post_type' => SPARK_TUTOR_REVIEW_POST_TYPE,
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'type',
					'value' => $type,
					'compare' => '=',
				),

			),
		];
		$reviews = spark_get_list_entity(SPARK_TUTOR_REVIEW_POST_TYPE, $args);
		return $reviews;
	}

}
