

function showTab(n) {
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
      document.getElementById("prevBtn").style.display = "none";
    } else {
      document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        var nextBtn = document.getElementById("nextBtn");
        nextBtn.innerHTML = "Submit";
        nextBtn.setAttribute('type', 'submit');
        return;
    } else {
      document.getElementById("nextBtn").innerHTML = "Next";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
  }
  
  function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
      //...the form gets submitted:
      document.getElementById("regForm").submit();
      return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
  }

function validateInput(field) {

    var field_type = field.getAttribute('type');
    var value = field.value;
    var pattern = field.getAttribute('pattern');
    if (field_type == 'email') {
        var regex = /^\S+@\S+\.\S+$/;
        if(regex.test(value) === false) {
            console.log(`Invalid Email: ${value}`);
            return false;
            // printError("emailErr", "Please enter a valid email address");
        }
    }

    if( pattern && (new RegExp(pattern)).test(value) === false) {
        console.log("Invalid pattern:");
        console.log(pattern);
        // printError("mobileErr", "Please enter a valid 10 digit mobile number");
        return false;
    }

    return true;
}

  function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
  
      if ( ! y[i].hasAttribute('required') ) {
        continue;
      }
  
      // If a field is empty...
      if (y[i].value == "") {
        // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false:
        valid = false;
      } else if ( ! validateInput( y[i] ) ) {
        y[i].className += " invalid";
        valid = false;
      }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
      document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
  }
  
  function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    try {
        x[n].className += " active";
    } catch (err) {
        console.log(`Unable to set index ${n}`);
        console.log(err);
    }
    
  }