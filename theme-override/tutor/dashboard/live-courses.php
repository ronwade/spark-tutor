<?php
/**
 * File Variables:
 * - $subjectArray - Subject for the course.
 * - $lessionDurationArray - Course duration.
 */

$uid = get_current_user_id();

$tutor = CPTutor::get_user_tutor_profile($uid);
$courses = CourseServices::get_courses_by_tutor_id($tutor->getField('ID'));

?>



<div class="row mb-2">
    <div class="col-sm-12">
        <div class="row mt-4">
            <div class="row">
                <div class="col-sm-12">
                    <h5><?php echo esc_html__('Course', 'spark-tutor') ?></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                            <th><?php echo esc_html__('Subject', 'spark-tutor') ?></th>
                            <th><?php echo esc_html__('Duration', 'spark-tutor') ?></th>
                            <th><?php echo esc_html__('Price', 'spark-tutor') ?></th>
                        </tr>
                        <?php foreach ($courses as $course) {
$subject = spark_get_entity_by_id($course->getField('subject'));
?>
                        <tr>
                            <td><?php echo esc_html($course->getField('name')) ?></td>
                            <td><?php echo esc_html($subject->getField('name')) ?></td>
                            <td><?php echo esc_html($course->getField('duration')) ?> <?php echo esc_html__('mins', 'spark-tutor') ?></td>
                            <td>$<?php echo esc_html($course->getField('price')) ?></td>
                        </tr>
                    <?php }?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

