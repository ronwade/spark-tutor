<?php

$uid = get_current_user_id();
$tutor = CPTutor::get_user_tutor_profile($uid);

$status = isset($_GET['status']) ? sanitize_text_field($_GET['status']) : null;
$keyword = isset($_GET['keyword']) ? sanitize_text_field($_GET['keyword']) : '';

$bookings = BookingServices::get_bookings_by_tutor_id($tutor->getId(), $status);
$all_count = BookingServices::count_booking_by_status(1, $tutor->getId());
$confirmed_count = BookingServices::count_booking_by_status(SPARK_TUTOR_STATUS_CONFIRMED, $tutor->getId());
$inprogress_count = BookingServices::count_booking_by_status(SPARK_TUTOR_STATUS_INPROGRESS, $tutor->getId());
$finished_count = BookingServices::count_booking_by_status(SPARK_TUTOR_STATUS_FINISHED, $tutor->getId());
$canceled_count = BookingServices::count_booking_by_status(SPARK_TUTOR_STATUS_CANCELLED, $tutor->getId());
$date_format = spark_date_format();

?>
<link href="https://app.spark-education.com/wp-content/plugins/spark-tutor/admin/css/spark-tutor-admin.css?ver=1.2.7" type="text/css" rel="stylesheet" />
<link href="https://app.spark-education.com/wp-content/plugins/spark-tutor/assets/css/shards.css?ver=1.2.7" type="text/css" rel="stylesheet" />
<div class="tutor-view">
    <div class="card">
        <div class="action mb-2 d-inline btn-group mt-2" role="group">
            <a href="<?php echo spark_get_view_url('edit') ?>" class="btn btn btn-outline-primary"><i class="fa fa-plus"></i> <?php echo esc_html__('Create New Booking', 'spark-tutor') ?></a>
        </div>
    </div>
</div>
<div class="row tutor-view booking-list">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Bookings', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="tab-status">
                    <div class="row">
                        <div class="col-sm-12 scrollable" >
                            <ul class="nav nav-tabs"  id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?php echo (!isset($status) || $status == '') ? 'active' : '' ?> " href="<?php echo get_new_tutor_lms_links('student-bookings', true) ?>" role="tab" aria-selected="<?php echo (!isset($status) || $status == '') ? 'true' : 'false' ?>"><?php echo esc_html__('All', 'spark-tutor') ?><span class="badge mr-3 badge-pill badge-secondary"><?php echo esc_html($all_count); ?></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($status == 'Confirmed') ? 'active' : '' ?> " id="home-tab" href="<?php echo get_new_tutor_lms_links('student-bookings', true) . '?status=Confirmed' ?>" role="tab" aria-selected="<?php echo ($status == 'Confirmed') ? 'true' : 'false' ?>"><?php echo esc_html__('Confirmed', 'spark-tutor') ?> <span class="badge mr-3 badge-pill badge-primary"><?php echo esc_html($confirmed_count) ?></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($status == 'InProgress') ? 'active' : '' ?> " id="home-tab" href="<?php echo get_new_tutor_lms_links('student-bookings', true) . '?status=InProgress' ?>" role="tab" aria-selected="<?php echo ($status == 'InProgress') ? 'true' : 'false' ?>"><?php echo esc_html__('In Progress', 'spark-tutor') ?> <span class="badge mr-3 badge-pill badge-warning"><?php echo esc_html($inprogress_count); ?></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($status == 'Finished') ? 'active' : '' ?> " id="home-tab" href="<?php echo get_new_tutor_lms_links('student-bookings', true) . '?status=Finished' ?>" role="tab" aria-selected="<?php echo ($status == 'Finished') ? 'true' : 'false' ?>"><?php echo esc_html__('Finished', 'spark-tutor') ?> <span class="badge mr-3 badge-pill badge-success"><?php echo esc_html($finished_count); ?></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($status == 'Cancelled') ? 'active' : '' ?> " id="home-tab" href="<?php echo get_new_tutor_lms_links('student-bookings', true) . '?status=Cancelled' ?>" role="tab" aria-selected="<?php echo ($status == 'Cancelled') ? 'true' : 'false' ?>"><?php echo esc_html__('Cancelled', 'spark-tutor') ?> <span class="badge mr-3 badge-pill badge-danger"><?php echo esc_html($canceled_count); ?></span></a>
                                </li>

                            </ul>

                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12 table-responsive">
                        <table class="table table-striped list-booking gu-datatable">
                            <thead>
                                <tr>
                                    <th><?php echo esc_html__('Unique Booking ID', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Tutor', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Student', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Booking Date & Time', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Status', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Action', 'spark-tutor') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($bookings as $booking) {
	$course = spark_get_entity_by_id($booking->getField('course'));
	$tutor = spark_get_entity_by_id($booking->getField('tutor'));

	?>
                                <tr >
                                    <td class="text-vertical-align"><?php echo esc_html($booking->getField('unique_id')) ?></td>
                                    <td class="text-vertical-align"><?php echo esc_html($course->getField('name')) ?></td>
                                    <td class="text-vertical-align"><?php echo esc_html($tutor->getField('name')) ?></td>
                                    <td class="text-vertical-align"><?php echo esc_html($booking->getField('student_name')) ?></td>
                                    <td class="text-vertical-align"><?php echo esc_html(date($date_format . ' H:i:s', strtotime($booking->getField('booking_date')))) ?></td>
                                    <td class="text-vertical-align"><?php echo HtmlHelper::render_badge_label($booking->getField('status')) ?></td>
                                    <td class="text-vertical-align"><a href="<?php echo spark_get_view_url('view', $booking->getField('ID')) ?>" class="btn btn-outline-primary"><i class="fa fa-eye"></i></a></td>

                                </tr>
                        <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

