<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://osoobe.com
 * @since             1.0.0
 * @package           Spark_Tutor
 *
 * @wordpress-plugin
 * Plugin Name:       Spark Tutor
 * Plugin URI:        https://b4oshany:oshany1991bailey@gitlab.com/osoobe/spark/gu-tutor.git
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.2.2
 * Author:            Oshane Bailey
 * Author URI:        http://osoobe.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       spark-tutor
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('SPARK_TUTOR_VERSION', '1.2.11');
define('SPARK_TUTOR_OPTIOM', 'spark_tutor_option');
//Example: http://abc.com/wp-content/plugins/spark-tutor/
define('SPARK_TUTOR_PLUGIN_URL', plugin_dir_url(__FILE__));
define('SPARK_TUTOR_PLUGIN_PUBLIC_URL', SPARK_TUTOR_PLUGIN_URL . 'public' . DIRECTORY_SEPARATOR);
define('SPARK_TUTOR_PLUGIN_PUBLIC_IMG_URL', SPARK_TUTOR_PLUGIN_PUBLIC_URL . 'img'  . DIRECTORY_SEPARATOR);

//Example: /var/www/html/wp-content/plugins/spark-tutor/
define('SPARK_TUTOR_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('SPARK_TUTOR_PLUGIN_ADMIN_DIR', SPARK_TUTOR_PLUGIN_PATH . 'admin' . DIRECTORY_SEPARATOR);
define('SPARK_TUTOR_PLUGIN_AUTH_DIR', SPARK_TUTOR_PLUGIN_PATH . 'auth' . DIRECTORY_SEPARATOR);
define('SPARK_TUTOR_PLUGIN_PUBLIC_DIR', SPARK_TUTOR_PLUGIN_PATH . 'public' . DIRECTORY_SEPARATOR);
define('SPARK_TUTOR_PLUGIN_THEME_DIR', SPARK_TUTOR_PLUGIN_PATH . 'theme-override' . DIRECTORY_SEPARATOR .'tutor'. DIRECTORY_SEPARATOR);
define('SPARK_TUTOR_PLUGIN_PUBLIC_PAGES', SPARK_TUTOR_PLUGIN_PUBLIC_DIR . 'pages'  . DIRECTORY_SEPARATOR);
define('SPARK_TUTOR_PLUGIN_PUBLIC_IMGS', SPARK_TUTOR_PLUGIN_PUBLIC_DIR . 'img'  . DIRECTORY_SEPARATOR);
define('SPARK_TUTOR_PLUGIN_SHORT_CODES', SPARK_TUTOR_PLUGIN_PATH . 'includes'  . DIRECTORY_SEPARATOR . 'shortcodes' . DIRECTORY_SEPARATOR );
define('SPARK_TUTOR_PLUGIN_UPLOAD_DIR_NAME', 'spark');
define('SPARK_TUTOR_PLUGIN_UPLOAD_PATH', wp_upload_dir()['basedir'] . DIRECTORY_SEPARATOR . SPARK_TUTOR_PLUGIN_UPLOAD_DIR_NAME . DIRECTORY_SEPARATOR);
define('SPARK_TUTOR_PLUGIN_UPLOAD_URL', wp_upload_dir()['basedir'] . '/' . SPARK_TUTOR_PLUGIN_UPLOAD_DIR_NAME );

$offset = get_option('gmt_offset');
$is_DST = TRUE;
$timezone_name = timezone_name_from_abbr('', $offset * 3600, $is_DST);
date_default_timezone_set($timezone_name);

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/libs/mobile-detect/Mobile_Detect.php';
require plugin_dir_path(__FILE__) . 'includes/constants.php';
require plugin_dir_path(__FILE__) . 'includes/class-spark-tutor.php';
require plugin_dir_path(__FILE__) . 'includes/template.php';
require plugin_dir_path(__FILE__) . 'includes/models/include.php';
require plugin_dir_path(__FILE__) . 'includes/services/include.php';
require plugin_dir_path(__FILE__) . 'includes/functions.php';
require plugin_dir_path(__FILE__) . 'includes/HtmlHelper.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-spark-tutor-activator.php
 */
function spark_tutor_activate() {
	Spark_Tutor::activate();
}

function get_query_param($param, $default=''){
	if(!empty($_POST[$param])) {
		return $_POST[$param];
	}
	return $default;
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-spark-tutor-deactivator.php
 */
function spark_tutor_deactivate() {
	Spark_Tutor::deactivate();
}

if ( ! function_exists('tutor_time')) {
	function tutor_time() {
		// return current_time( 'timestamp' );
		return time() + ( get_option( 'gmt_offset' ) * 60 );
	}
}

register_activation_hook(__FILE__, 'spark_tutor_activate');
register_deactivation_hook(__FILE__, 'spark_tutor_deactivate');

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function spark_tutor_run() {

	$plugin = new Spark_Tutor();
	$plugin->run();
}

spark_tutor_run();
