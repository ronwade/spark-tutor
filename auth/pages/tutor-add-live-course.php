<?php
/**
 * File Variables:
 * - $subjectArray - Subject for the course.
 * - $lessionDurationArray - Course duration.
 */

$uid = get_current_user_id();
$tutor_id = post_url_param("tutor", false);
$tutor = CPTutor::get_current_tutor();

if ( $tutor->isValidEntity() && $tutor->getId() == $tutor_id && 
     isset($_POST["tutor"]) && isset($_POST["add_live_course"]) && isset($_POST['name']) 
) {
    do_action('tutor_post_form_save_course');
}

get_header();

require_once SPARK_TUTOR_PLUGIN_PUBLIC_PAGES."404.php";

get_footer();

?>