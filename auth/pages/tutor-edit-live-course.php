<?php
/**
 * File Variables:
 * - $subjectArray - Subject for the course.
 * - $lessionDurationArray - Course duration.
 */

$uid = get_current_user_id();
$tutor = CPTutor::get_user_tutor_profile($uid);
$tutor_id = get_url_param("tutor", false);
$course_id = get_url_param("course", get_url_param("id", false));

$course = CourseServices::get_course_by_tutor_id($course_id, $tutor->getId());
$trigger404 = true;

if ( !empty($course) && $course->isValidEntity() && $tutor->isValidEntity() && $tutor->getId() == $tutor_id && $course->getId() == $course_id ) {

    if ( isset($_POST["tutor"]) && isset($_POST["add_live_course"]) && isset($_POST['name']) ) {
        do_action('tutor_post_form_save_course');
    }
    $trigger404 = false;
}

if ( empty($subjectArray) ) {
    $subjectArray = [];
    $subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, null);
    foreach ($subjects as $value) {
        $subjectArray[$value->getPost()->ID] = $value->getField('name');
    }
 }

 if ( ! isset($lessionDurationArray) ) {
    $lessionDurationArray = spark_get_duration_list();
 }

get_header();

if ($trigger404) {

    require_once SPARK_TUTOR_PLUGIN_PUBLIC_PAGES."404.php";

} else {
?>


<div class="gutut twbs gu-tut-main-content">
    <div class="container gu-container-v-margin" >
<form method="POST" action="<?php echo get_home_url()."/edit-live-courses/?tutor=$tutor_id&course=$course_id";  ?>" enctype="multipart/form-data">
            <input type="hidden" name="action" value="form_save_course">
            <input type="hidden" name="entity_type" value="<?php echo SPARK_TUTOR_COURSE_POST_TYPE; ?>">
            <input type="hidden" name="id" id="course_id" value="<?php echo $course->getId(); ?>">
            <input type="hidden" name="course" value="<?php echo $course->getId(); ?>">
            <input type="hidden" name="tutor" value="<?php echo $tutor->getId(); ?>">
            <input type="hidden" name="modifier" value="<?php echo get_current_user_id(); ?>">
            <h2 ><?php echo esc_html__('Edit Course', 'spark-tutor') ?></h2>
            <div >
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="name" class="col-form-label"><?php echo esc_html__('Course Name', 'spark-tutor') ?></label>
                        <input type="text" class="form-control" id="course_name" name="name" value="<?php echo $course->getField('name'); ?>" required>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="name" class="col-form-label"><?php echo esc_html__('Subject', 'spark-tutor') ?></label>
                        <select class="custom-select form-control" id="course_subject" name="subject">
                            <option value="">---Select---</option>
                            <?php foreach ($subjectArray as $key => $value): ?>
                                <option value="<?php echo esc_attr($key) ?>"  <?php echo (esc_attr($key) == esc_attr($course->getField('subject')) )? 'selected': ''; ?>  ><?php echo $value; ?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="name" class="col-form-label"><?php echo esc_html__('Duration', 'spark-tutor') ?></label>
                        <select class="custom-select form-control" id="course_duration" name="duration">
                            <option value="">---Select---</option>
                            <?php foreach ($lessionDurationArray as $key => $value): ?>
                                <option value="<?php echo esc_attr($key) ?>"  <?php echo (esc_attr($key) == esc_attr($course->getField('duration')) )? 'selected': ''; ?>  ><?php echo $value; ?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="name" class="col-form-label">
                            <i class="fa fa-usd"></i>
                            <?php echo esc_html__('Price', 'spark-tutor') ?>
                        </label>
                            <div class="input-group input-group-seamless">
                            <input type="text" <?php echo (spark_disable_custom_price()) ? 'disabled' : ''; ?>
                                    placeholder="<?php echo spark_get_default_price(); ?>"
                                    value="<?php echo $course->getField("price"); ?>" class="form-control" id="course_price" name="price"  >
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="form1-name" class="col-form-label"><?php echo esc_html__('Thumbnail', 'spark-tutor') ?></label>
                                <div class="form-group">
                                    <input type="file" class="form-control" id="customFileCourse" name="picture"  accept="image/*">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <img src="<?php echo get_pic_or_default( esc_attr($course->getPic() ) ); ?>" style="max-height: 120px" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label class="col-form-label"><?php echo esc_html__('Description', 'spark-tutor') ?></label>
                        <div class="form-group">
                            <textarea class="form-control" id="course_description" rows="2" name="description"><?php echo $course->getField('description'); ?></textarea>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label class="col-form-label"><?php echo esc_html__('Course Details', 'spark-tutor') ?></label>
                        <div class="form-group">
                            <textarea class="form-control" id="course_detail" rows="2" name="detail"><?php echo $course->getField('detail'); ?></textarea>
                        </div>

                    </div>
                </div>
            </div>
            <div >
                <button type="button" class="tutor-modal-btn-cancel"><?php _e('Cancel', 'tutor') ?></button>
                <button type="submit" name="live_course_action" value="edit" class="tutor-primary"><?php _e('Yes, Save Changes', 'tutor') ?></button>
            </div>
        </form>



    </div>
</div>

<?php

}

get_footer();

?>