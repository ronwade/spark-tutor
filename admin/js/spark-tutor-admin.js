(function( $ ) {
	'use strict';

	//Fullcalendar in Availability Page

    function generate_rating(element, rating){
        var i=0;
        element.html('');
        for (i=1;i<= rating; i++){
            element.append( "<i class='fa fa-star'></i>");
        }
        var i=rating+1;
        for (i=rating+1;i<= 5; i++){
            element.append( "<i class='fa fa-star-o'></i>");
        }
    }
	
	$(document).ready(function(){
		if($('#button_test_email').length){
            $( "#button_test_email" ).on( "click", function() {
                  var test_email = jQuery("#test_email").val();
                jQuery("#button_test_email").addClass('disabled');
                jQuery(".load-icon").show();
                jQuery.ajax({
                    url: window.location.origin + "/wp-admin/admin-ajax.php",
                    type: "post",
                    data: {
                        action: "send_test_email",
                        test_email: test_email,
                    },
                    success:function( response ) {
                        jQuery("#button_test_email").removeClass('disabled')
                        jQuery(".load-icon").hide();
                        if(response == true){
                            jQuery("#not-sent").hide();
                            jQuery("#sent").show()
                            jQuery("#sent").html('Test email was sent successfully with the current configuration. Please check your inbox to make sure it is delivered. If you do not receive the test email, please contact with your SMTP service provider.');
                        }
                        else{
                            jQuery("#not-sent").show();
                            jQuery("#sent").hide()
                            jQuery("#not-sent").html('Your current configuration is not correct. Please check with your email service provider.')
                        }

                    },
                    error: function(){
                        console.log(errorThrown);
                    }

                })
            });

        }

		if($('.gu-tut-main-content .btn-form-submit').length){
			$('.gu-tut-main-content .btn-form-submit').on('click', function(e){

                e.preventDefault();

                $('.edit-form').parsley().on('field:validated', function() {});

                $('.edit-form').submit();
                
			});
		}

		if($('.gu-tut-main-content .btn-confirm-delete').length){
			$('.gu-tut-main-content .btn-confirm-delete').on('click', function(e){
                e.preventDefault();
				$('.gu-tut-main-content .btn-confirm-delete').closest('form').submit();	
			});
		}
		if($('.gu-tut-main-content .btn-delete').length){
			$('.gu-tut-main-content .btn-delete').on('click', function(e){
				e.preventDefault();
				if($(this).attr('post-id')){
					$('#delete-object-id').val($(this).attr('post-id'));
				}
			});
			
		}
		if($('.gu-tut-main-content .button-trash').length){
			$(".button-trash").on( "click",  function(e) {
                e.preventDefault();
				$(this).parent().parent().remove();
			});
		}
		if($('.gu-tut-main-content .btn-update-status').length){
			$('.gu-tut-main-content .btn-update-status').on('click', function(e){
				console.log("Update status clicked");
				e.preventDefault();
				if($(this).attr('data-status')){
					var status=$(this).attr('data-status');
					var id=get_url_param('id');
					jQuery.ajax({
                      type: "post",
                      dataType: "json",
                      url: window.location.origin + "/wp-admin/admin-post.php",
                      data: {
                          action: "update_post_status",
                          id: id,
                          status: status,
                      },
                      success: function (response) {
                          if(response.status=='1'){
                          	location.reload();
                          }
                      },
                      error: function (jqXHR, textStatus, errorThrown) {
                          console.log(
                              "The following error occured: " + textStatus,
                              errorThrown
                          );
                      },
                  });
				}
			});
		}

        if($('.gu-tut-main-content .btn-show-report').length){
            $('.gu-tut-main-content .btn-show-report').on('click', function(e){
                e.preventDefault();
                $('#reportModal .delete-object-id').val($(this).attr('data-post-id'));
                $('#reportModal .tutor-name').html($(this).attr('data-tutor-name'));
                $('#reportModal .student-name').html($(this).attr('data-student-name'));
                $('#reportModal .created-date').html($(this).attr('data-created-date'));
                generate_rating($('#reportModal .star-wrapper'),parseInt($(this).attr('data-rating')));
                $('#reportModal .comment').html($(this).attr('data-comment'));
            });
        }

        if($('.gu-tut-main-content .btn-show-review').length){
            $('.gu-tut-main-content .btn-show-review').on('click', function(e){
                e.preventDefault();
                $('#reviewModal .delete-object-id').val($(this).attr('data-post-id'));
                $('#reviewModal .tutor-name').html($(this).attr('data-tutor-name'));
                $('#reviewModal .student-name').html($(this).attr('data-student-name'));
                $('#reviewModal .created-date').html($(this).attr('data-created-date'));
                generate_rating($('#reviewModal .star-wrapper'),parseInt($(this).attr('data-rating')));
                $('#reviewModal .comment').html($(this).attr('data-comment'));
            });
        }
        if($('.gu-tut-main-content .import-sample-data').length){
            $('.import-sample-data').unbind('click').bind('click', function (e) {
            jQuery.ajax({
                type: "POST",                
                dataType : "json",
                url: $(this).attr('data-url'),     
                data: {
                    action     : 'import_sample_data', 
                },
                success:function( response ) {
                   alert("True");
                },
                error: function(){
                    
                }
            });
        })
        }
		

		$('#enabled_admin_email').change(function(){
			if($('#enabled_admin_email').prop('checked')){
				$('.admin-enabled-email').html('<i class="fa fa-check" aria-hidden="true"></i>');
			}
			else{
				$('.admin-enabled-email').html('');
			}
		})
		$('#enabled_tutor_email').change(function(){
			if($('#enabled_tutor_email').prop('checked')){
				$('.tutor-enabled-email').html('<i class="fa fa-check" aria-hidden="true"></i>');
			}
			else{
				$('.tutor-enabled-email').html('');
			}
		})
		$('#enabled_student_email').change(function(){
			if($('#enabled_student_email').prop('checked')){
				$('.student-enabled-email').html('<i class="fa fa-check" aria-hidden="true"></i>');
			}
			else{
				$('.student-enabled-email').html('');
			}
		})

		//Setup datatable
		//
		$('.gu-datatable').DataTable({
	         "dom": 'frt<"bottom"lp><"clear">'
	    });
	    $('.spark-tutor-datatable').DataTable({
	         "dom": 'frt<"bottom"lp><"clear">',
	         "drawCallback": function( settings ) {
			     $(".spark-tutor-datatable thead").remove();
			}   
	    });


	    

		if(document.getElementById('calendar')){
			var calendarEl = document.getElementById('calendar');

			var calendar = new FullCalendar.Calendar(calendarEl, {
				plugins: [ 'interaction', 'dayGrid', 'timeGrid'],
				defaultView: 'timeGridWeek',
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'dayGridMonth,timeGridWeek,timeGridDay'
				},
		  columnHeaderFormat: {
			weekday: 'long'
		  },
		  navLinks: true, // can click day/week names to navigate views
		  businessHours: true, // display business hours
		  editable: false,
		  selectable: true,
		  selectMirror: true,
		  select: function(arg) {
			 //check duplicate event
			  var start_time = convert_time_to_minutes(arg.start.getHours(), arg.start.getMinutes());
			  var end_time = convert_time_to_minutes(arg.end.getHours(), arg.end.getMinutes());
			  var events = calendar.getEvents();
			  var duplicate = false;
			  jQuery.each(events, function(index, value){
					if(arg.start.getDay()==value.start.getDay()){
						var event_start_time = convert_time_to_minutes(value.start.getHours(), value.start.getMinutes());
					  	var event_end_time = convert_time_to_minutes(value.end.getHours(), value.end.getMinutes());
						if((start_time>=event_start_time && start_time<event_end_time) || (end_time>=event_start_time && end_time<=event_end_time) || (start_time<=event_start_time && end_time >= event_end_time)){
							duplicate = true;
						}
					}
			  });

			  if (duplicate == false) {
                  //show time has choose
                  calendar.addEvent({
                      start: arg.start,
                      end: arg.end,
                      allDay: arg.allDay,
                      textColor: '#ffffff',
                  });
                  //request save time has choose
                  jQuery.ajax({
                      type: "post",
                      dataType: "json",
                      url: window.location.origin + "/wp-admin/admin-ajax.php",
                      data: {
                          action: "save_form_info",
                          start_time: convert_time_to_minutes(
                              arg.start.getHours(),
                              arg.start.getMinutes()
                          ),
                          end_time: convert_time_to_minutes(
                              arg.end.getHours(),
                              arg.end.getMinutes()
                          ),
                          tutor: jQuery("#tutor_id").val(),
                          weekday: arg.start.getDay(),
                          entity_type: "gu_tut_available_day",
                      },
                      success: function (response) {
                          console.log("success");
                      },
                      error: function (jqXHR, textStatus, errorThrown) {
                          console.log(
                              "The following error occured: " + textStatus,
                              errorThrown
                          );
                      },
                  });
			  }
			  else{
				jQuery("#notification").fadeIn("slow");
			  }
			
		  	calendar.unselect()
      },
      eventRender: function(info) {
      	//add button delete available time
      	console.log(info)
      	info.el.querySelector('.fc-time').innerHTML += "<i class='fa fa-times close-on pull-right' aria-hidden='true'></i>";
      	$('.fc-time-grid-event .fc-time').css('font-size','16px');
      },
      eventClick: function(info){
      	$('.close-on').click(function(){
      		//remove event in front end
      		info.event.remove();
      		//update in database
      		jQuery.ajax({
      			type : "post", 
      			dataType : "json", 
      			url :window.location.origin+'/wp-admin/admin-ajax.php', 
      			data : {
      				action: "delete_available_day",
      				start_time: convert_time_to_minutes(
      					info.event.start.getHours(),
      					info.event.start.getMinutes()
      					),
      				end_time: convert_time_to_minutes(
      					info.event.end.getHours(),
      					info.event.end.getMinutes()
      					),
      				tutor: jQuery("#tutor_id").val(),
      				weekday: info.event.start.getDay(), 
      			},
      			success: function(response) {
      				
      			},
      			error: function( jqXHR, textStatus, errorThrown ){
      				console.log( 'The following error occured: ' + textStatus, errorThrown );
      			}
      		})
      	})
      },
		});
		// get time in database
		jQuery.ajax({
			type : "post", 
			dataType : "json", 
			url :window.location.origin+'/wp-admin/admin-ajax.php', 
			data : {
				action: "get_available_day",
				tutor_id: jQuery('#tutor_id').val()
			},
			success: function(response) {
				jQuery.each( response, function( index, value ) {
					calendar.addEvent({
						daysOfWeek: [ value['weekday'] ], // these recurrent events move separately
						startTime: convert_minutes_to_time(value['start_time']),
						endTime: convert_minutes_to_time(value['end_time']),
						textColor: '#ffffff',
				})				
				});
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log( 'The following error occured: ' + textStatus, errorThrown );
			}
		})
			calendar.render();
		}
		/* End Tutors */
		/* Start Dashboard*/

		 // Javascript method's body can be found in js/dashboard.js
		 if($('#bigDashboardChart').length){
		 	demo.initDashboardPageCharts();
		 }
		 
		/*End Dashboard*/

		/*Start Calendar*/
		if($('#full-calendar').length){

			var calendarE2 = document.getElementById('full-calendar');

			var calendar = new FullCalendar.Calendar(calendarE2, {
				plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list'],
				defaultView: 'timeGridWeek',
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
				},
	  
		  navLinks: true, // can click day/week names to navigate views
		  businessHours: true, // display business hours
		  editable: false,
		  eventClick: function(info) {
		  	console.log(info);
			info.jsEvent.preventDefault(); // don't let the browser navigate

			if (info.event.url) {
    			window.open(info.event.url);
    		}
			}
		});

			//fillter in calendar page
			$('#fillter').click(function(){
				//remove old data
				var events = calendar.getEvents();
				jQuery.each( events, function( index, value ) {
					value.remove()
				})

				//get new data
				jQuery.ajax({
					type : "post", 
					dataType : "json", 
					url :window.location.origin+'/wp-admin/admin-ajax.php', 
					data : {
						action: "get_pick_available_day",
						fillter_tutor: jQuery('#tutor').val(),
						fillter_subject: jQuery('#subject').val(),
						fillter_status: jQuery('#status').val()
					},
					success: function(response) {
						
						jQuery.each( response, function( index, value ) {
							var date = new Date(value['pick_date']);
							calendar.addEvent({
                        start: value["pick_date"]+"T"+convert_minutes_to_time(value['start_time']),
                        end: value["pick_date"]+"T"+convert_minutes_to_time(value['end_time']),
                        color: value['color'],
                        url: window.location.origin+'/wp-admin/admin.php?page=spark-tutor-bookings&view=view&id='+value['id'],
                        textColor: '#ffffff',
                        title: value['title']
                    })              
						});
						calendar.render();
					},
					error: function( jqXHR, textStatus, errorThrown ){
						console.log( 'The following error occured: ' + textStatus, errorThrown );
					}
				})
			});
			$('#clear').click(function(){
				//get fillter default
				jQuery('#tutor').val('');
				jQuery('#subject').val('');
				jQuery('#status').val('');
				//remove fillter data
				var events = calendar.getEvents();
				jQuery.each( events, function( index, value ) {
					value.remove()
				})
				// get data
				jQuery.ajax({
					type : "post", 
					dataType : "json", 
					url :window.location.origin+'/wp-admin/admin-ajax.php', 
					data : {
						action: "get_pick_available_day",
					},
					success: function(response) {
						
						jQuery.each( response, function( index, value ) {
							var date = new Date(value['pick_date']);
							calendar.addEvent({
                        
                        start: value["pick_date"]+"T"+convert_minutes_to_time(value['start_time']),
                        end: value["pick_date"]+"T"+convert_minutes_to_time(value['end_time']),
                        color: value['color'],
                        url: window.location.origin+'/wp-admin/admin.php?page=spark-tutor-bookings&view=view&id='+value['id'],
                        textColor: '#ffffff',
                         title: value['title']
                    })              
						});
						calendar.render();
					},
					error: function( jqXHR, textStatus, errorThrown ){
						console.log( 'The following error occured: ' + textStatus, errorThrown );
					}
				})
			});
			jQuery.ajax({
			type : "post", 
			dataType : "json", 
			url :window.location.origin+'/wp-admin/admin-ajax.php', 
			data : {
				action: "get_pick_available_day",
			},
			success: function(response) {
				
				jQuery.each( response, function( index, value ) {
                    var date = new Date(value['pick_date']);
                    calendar.addEvent({
                        start: value["pick_date"]+"T"+convert_minutes_to_time(value['start_time']),
                        end: value["pick_date"]+"T"+convert_minutes_to_time(value['end_time']),
                        color: value['color'],
                        url: window.location.origin+'/wp-admin/admin.php?page=spark-tutor-bookings&view=view&id='+value['id'],
                        textColor: '#ffffff',
                         title: value['title']
                })              
                });
                calendar.render();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log( 'The following error occured: ' + textStatus, errorThrown );
			}
		})

			
			
		}
	
	/*End Calendar*/

	$("#customFile, #customFileSubject, #customFileCourse").change(function (e) {
			var fileName = e.target.files[0].name;
			$(this).next().text(fileName);
		});
	jQuery( "#datepicker" ).datepicker();


	jQuery('select[name="subject"]').change(function(){
		
		if($(this).val() != ''){
			jQuery('select[name="tutor"]').html('<option value="">---Select---</option>');
			jQuery.ajax({
				type : "post", 
				dataType : "json", 
				url :window.location.origin+'/wp-admin/admin-ajax.php', 
				data : {
					action: "load_tutors",
					subject: $(this).val(),
				},
				success: function(response) {
					jQuery.each( response, function( index, value ) {
						jQuery('select[name="tutor"]').append('<option value="'+value["id"]+'">'+value['name']+'</option>');
					});
				
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log( 'The following error occured: ' + textStatus, errorThrown );
			}
		})
		} else{
			jQuery('select[name="tutor"]').html('<option value="">---Select---</option>');
		}
	});

	jQuery('select[name="tutor"]').change(function(){
		console.log($(this).val())
		if($(this).val() != ''){
			jQuery('select[name="course"]').html('<option value="">---Select---</option>');
		jQuery.ajax({
			type : "post", 
			dataType : "json", 
			url :window.location.origin+'/wp-admin/admin-ajax.php', 
			data : {
				action: "load_course",
				tutor: $(this).val(),
            	subject: jQuery('select[name="subject"]').val(),
			},
			success: function(response) {
				jQuery.each( response, function( index, value ) {
					jQuery('select[name="course"]').append('<option price="'+value['price']+'" tax="'+value['tax']+'" duration="'+value['duration']+'" value="'+value["id"]+'">'+value['name']+'</option>');
				});
				
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log( 'The following error occured: ' + textStatus, errorThrown );
			}
		})
		
		} 
		else{
			jQuery('select[name="course"]').html('<option value="">---Select---</option>');
		}

		if($(this).val() != ''){
			jQuery('#booking_date').removeAttr('readonly');
		}
		else{
			jQuery('#booking_date').attr('readonly','');
			jQuery('#booking_date').val('');
		}
	});

	//create booking generate modal calendar
	jQuery('select[name="course"]').change(function(){
		jQuery("#course_duration").val(jQuery('option:selected', this).attr('duration'));
		jQuery("#price").val(jQuery('option:selected', this).attr('price'));
		jQuery("#course_tax").val(jQuery('option:selected', this).attr('tax'));
		jQuery("#course_total").val(Number(jQuery("#price").val()) + Number(jQuery("#course_tax").val()));
		get_availability_day(jQuery('select[name="tutor"]').val());
	})

	//create booking - show modal calendar
	jQuery('#booking_date').click(function(){
		if(jQuery('select[name="tutor"]').val() != ''){
			$('#bookingDateModal').modal('show');
		}
	})
	// create booking - student phone field only number
	$("#student_phone").on("keypress keyup blur",function (event) {    
		$(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});
	//create booking - payment detail

	jQuery('select[name="payment_method"]').change(function(){
		if($(this).val() != ''){
			var now_date = new Date();
			$('#payment_method').val($(this).val());
			$("#course_price").val($('#price').val());
			$("#duration").val($('#course_duration').val());
			$("#tax").val($('#course_tax').val());
			$("#total").val($("#course_total").val());
			jQuery.ajax({
				type : "post", 
				dataType : "json", 
				url :window.location.origin+'/wp-admin/admin-ajax.php', 
				data : {
					action: "get_date_format",
				},
				success: function(response) {
					if(response == 'd-m-Y'){
						$("#created_date").val(date_format('DD-MM-YYYY', now_date) +' '+leftPad(now_date.getHours(),2)+':'+leftPad(now_date.getMinutes(),2)+':'+leftPad(now_date.getSeconds(),2))
					}
					if(response == 'm-d-Y'){
						$("#created_date").val(date_format('MM-DD-YYYY', now_date) +' '+leftPad(now_date.getHours(),2)+':'+leftPad(now_date.getMinutes(),2)+':'+leftPad(now_date.getSeconds(),2))
					}
					if(response == 'Y-m-d'){
						$("#created_date").val(date_format('YYYY-MM-DD', now_date) +' '+leftPad(now_date.getHours(),2)+':'+leftPad(now_date.getMinutes(),2)+':'+leftPad(now_date.getSeconds(),2))
					}
				},
				error: function( jqXHR, textStatus, errorThrown ){
					console.log( 'The following error occured: ' + textStatus, errorThrown );
				}
			})
		}
		else {
			$('#payment_method').val('');
		}
	})

	if(jQuery('#showModal').length){
		if(jQuery('#showModal').val() == 'true'){
			$('#modalCourse').modal('show')
		}
	}
});
	
	
})( jQuery );
function add_row(){
	var options = jQuery(".subject-render > option").clone();
	var html = '<tr>'
		+'<td><input type="text" class="form-control" value="" name="course_name[]" required></td>'
		+'<td>'
		+'<select class="custom-select none-option" name="course_subject[]" required>'
		+'</select>'
		+'</td><td>'
		+'<select class="custom-select" name="course_duration[]" required>'
        +'<option value="" disabled>---Select---</option>'
		+'<option value="90" selected="">90 mins</option>'
		+'<option value="30">30 mins</option>'
		+'<option value="60">60 mins</option>'
		+'</select>'
		+'</td>'
		+'<td><div class="form-group">'
		+'<div class="input-group input-group-seamless">'
		+'<span class="input-group-prepend">'
		+'<span class="input-group-text">'
		+'<i class="fa fa-usd"></i>'
		+'</span>'
		+'</span>'
		+'<input type="text" class="form-control" name="course_price[]" required>'
		+'</div>'
		+'</div>'
		+'</td>'
		+'<td><a class="btn btn-outline-danger button-trash"><i class="fa fa-trash"></i></a></td>'
		+'</tr>'	
		
	jQuery('.table-striped').append(html);
	jQuery('.none-option').append(options);
	jQuery('.none-option').removeClass('none-option');
	jQuery(".button-trash").off("click"); 
	jQuery(".button-trash").on( "click",  function() {
		jQuery(this).parent().parent().remove();
	});
}
function add_time_off(){
	
	var html = '<div class="row mb-2">'
	+'<div class="col-sm-3">'
	+'<input type="text" class="form-control" id="datepicker" placeholder="Date">'
	+'</div>'
	+'<div class="col-sm-2">'
	+'<fieldset>'
	+'<div class="custom-control custom-checkbox d-block my-2">'
	+'<input type="checkbox" class="custom-control-input" id="customCheck1">'
	+'<label class="custom-control-label" for="customCheck1">Full Date Off</label>'
	+'</div></fieldset></div>'
	+'<div class="col-sm-3">'
	+'<input type="text" class="form-control" id="form1-name" placeholder="Start Time">'
	+'</div>'
	+'<div class="col-sm-3">'
	+'<input type="text" class="form-control" id="form1-name" placeholder="End Time">'
	+'</div>'
	+'<div class="col-sm-1">'
	+'<button class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>'
	+'</div></div>'
		
	jQuery('.list-date-off').append(html);
	jQuery(".button-trash").off("click"); 
	jQuery(".button-trash").on( "click",  function() {
		jQuery(this).parent().parent().remove();
	});
}
function convert_time_to_minutes(hour, minute){
    return Number(hour)*60 + Number(minute)
}
function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}
function convert_minutes_to_time(minutes){
	var hour = Math.floor(Number(minutes)/60);
	var minute = Number(minutes)%60;
	return leftPad(hour, 2)+":"+leftPad(minute, 2);
}
function get_url_param(name){
	var url = new URL(window.location.href);
	return url.searchParams.get(name);
}

function get_availability_day(tutor_id){
			jQuery('#availability').html('');
            var calendarE2 = document.getElementById('availability');
            calendar = new FullCalendar.Calendar(calendarE2, {
                plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list'],
                defaultView: 'timeGridWeek',
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'next'
                },
      
        columnHeaderFormat: { weekday: 'short', month: 'short', day: 'numeric' },
        slotLabelFormat: { hour: 'numeric',minute: 'numeric'},
        titleFormat: {year: 'numeric', month: 'long'},
          navLinks: true, // can click day/week names to navigate views
          businessHours: true, // display business hours
          editable: false,
          eventColor: '#81C784',
          displayEventTime: false,
          selectable: false,
          eventOverlap: true,
          height: 400,
          validRange: {
                start: new Date().setDate(new Date().getDate()+1),
            },
            firstDay: new Date().getDay() + 1,
          dateClick: function(arg) {
            
            var start_time = convert_time_to_minutes(arg.date.getHours(),arg.date.getMinutes());
            var end_time = start_time + Number(jQuery('#course_duration').val());
            console.log(start_time);
            console.log(end_time)
            var booking_date = arg.date.getFullYear() +"-" + leftPad(arg.date.getMonth()+1, 2) + "-"+leftPad(arg.date.getDate(), 2);
            var events = calendar.getEvents();
              var invalid = true;
              //check valid date
              jQuery.each(events, function(index, value){
                    if(arg.date.getDay()==value.start.getDay()){
                        var event_start_time = convert_time_to_minutes(value.start.getHours(), value.start.getMinutes());
                        var event_end_time = convert_time_to_minutes(value.end.getHours(), value.end.getMinutes());
                        if(start_time >= event_start_time && start_time < event_end_time && end_time > event_start_time && end_time <= event_end_time){
                            invalid = false;
                        }
                    }
              });
              if(invalid == false) {
                    //remove old event
                    var old_events = calendar.getEventById('booked');
                    console.log(old_events);
                    if(old_events != null){
                        old_events.remove()
                    }
                    //add new event
                    jQuery('#start_time').val(start_time);
                    jQuery('#end_time').val(end_time);
                    jQuery('#course_booking_date').val(booking_date + " " + convert_minutes_to_time(start_time));
                    calendar.addEvent({
                        id: 'booked',
                        start: arg.date,
                        end: booking_date + "T" + convert_minutes_to_time(end_time),
                        allDay: arg.allDay,
                        backgroundColor: '#4CAF50',
                    });
                    jQuery.ajax({
                        type : "post", 
                        dataType : "json", 
                        url :window.location.origin+'/wp-admin/admin-ajax.php', 
                        data : {
                            action: "get_date_format",
                        },
                        success: function(response) {
                            if(response == 'd-m-Y'){
                                jQuery('#booking_date').val(date_format('DD-MM-YYYY', arg.date) + " " + convert_minutes_to_time(start_time));
                            }
                            if(response == 'm-d-Y'){
                                jQuery('#booking_date').val(date_format('MM-DD-YYYY', arg.date) + " " + convert_minutes_to_time(start_time));
                            }
                            if(response == 'Y-m-d'){
                                jQuery('#booking_date').val(date_format('YYYY-MM-DD', arg.date) + " " + convert_minutes_to_time(start_time));
                            }
                        },
                        error: function( jqXHR, textStatus, errorThrown ){
                            console.log( 'The following error occured: ' + textStatus, errorThrown );
                        }
                    })
            }
            else{
                jQuery("#notification").fadeIn().fadeOut(2000);
            }
            console.log(arg);
            },
        });
            // get time in database
        jQuery.ajax({
            type : "post", 
            dataType : "json", 
            url :window.location.origin+'/wp-admin/admin-ajax.php', 
            data : {
                action: "get_available_day",
                tutor_id: tutor_id
            },
            success: function(response) {
                jQuery.each( response, function( index, value ) {
                    calendar.addEvent({
                        daysOfWeek: [ value['weekday'] ], // these recurrent events move separately
                        startTime: convert_minutes_to_time(value['start_time']),
                        endTime: convert_minutes_to_time(value['end_time']),
                        rendering: 'background',
                        color: "#81C784"
                })              
                });
            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log( 'The following error occured: ' + textStatus, errorThrown );
            }
        })
        jQuery.ajax({
            type : "post", 
            dataType : "json", 
            url :window.location.origin+'/wp-admin/admin-ajax.php', 
            data : {
                action: "get_pick_available_day",
                tutor_id: tutor_id
            },
            success: function(response) {
                jQuery.each( response, function( index, value ) {
                    var date = new Date(value['pick_date']);
                    calendar.addEvent({
                        
                        start: value["pick_date"]+"T"+convert_minutes_to_time(value['start_time']),
                        end: value["pick_date"]+"T"+convert_minutes_to_time(value['end_time']),
                        color: "#CCCCCC",
                })              
                });
            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log( 'The following error occured: ' + textStatus, errorThrown );
            }
        })

            
            setTimeout(function(){ 
            	calendar.render();
            }, 1000);
}

function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
    return true;

}
function date_format(format, date){
    var result = '';
    if(format == 'DD-MM-YYYY'){
        result = leftPad(date.getDate(), 2) +"-" + leftPad(date.getMonth()+1, 2) + "-"+ date.getFullYear()  ;
    }
    if(format == 'MM-DD-YYYY'){
        result = leftPad(date.getMonth()+1, 2) + "-"+ leftPad(date.getDate(), 2) +"-" +  date.getFullYear()  ;
    }
    if(format == 'YYYY-MM-DD'){
        result = date.getFullYear() +"-" + leftPad(date.getMonth()+1, 2) + "-"+ leftPad(date.getDate(), 2)     ;
    }
    return result;
}
function get_course_info (course_id = ''){
	console.log(course_id);
	if(course_id != ''){
		jQuery('#modalCouseTitle').html('Edit Course');
		jQuery.ajax({
			type : "post", 
			dataType : "json", 
			url :window.location.origin+'/wp-admin/admin-ajax.php', 
			data : {
				action: "get_course_info",
				course_id: course_id
			},
			success: function(response) {
				jQuery('#course_name').val(response['name']);
				jQuery('#course_subject').val(response['subject']);
				jQuery('#course_duration').val(response['duration']);
				jQuery('#course_price').val(response['price']);
				jQuery('#course_id').val(response['id']);
				jQuery('#course_description').val(response['description']);
				jQuery('#course_detail').val(response['detail']);
				// jQuery('#customFileCourse').val(response['thumbnail']);
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log( 'The following error occured: ' + textStatus, errorThrown );
			}
		})
	}
	else{
		jQuery('#modalCouseTitle').html('Add Course');
		jQuery('#course_name').val('');
		jQuery('#course_subject').val('')
		jQuery('#course_duration').val('')
		jQuery('#course_price').val('')
		jQuery('#course_id').val('')
		jQuery('#course_description').val('')
		jQuery('#course_detail').val('')
		jQuery('#customFileCourse').val('')
	}
}
