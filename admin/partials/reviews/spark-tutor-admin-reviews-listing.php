<?php
$reviews = ReviewServices::get_reviews_by_type(SPARK_TUTOR_TYPE_REVIEW);
$date_format = spark_date_format();
?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Reviews', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-striped gu-datatable">
                                        <thead>
                                            <tr>
                                                <th><?php echo esc_html__('Review', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Unique Booking ID', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Tutor', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Student', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Course Date', 'spark-tutor') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($reviews as $review) {?>
                                            <tr>
                                                <td><a href="#" data-toggle="modal" data-target="#reviewModal" class="btn-show-review" data-post-id="<?php echo esc_attr($review->getField('ID')) ?>" data-tutor-name="<?php echo esc_attr($review->getField('tutor_name')) ?>" data-student-name="<?php echo esc_attr($review->getField('student_name')) ?>" data-created-date="<?php echo esc_attr(date($date_format . ' H:i:s', strtotime($review->getField('created_date')))) ?>" data-rating="<?php echo esc_attr($review->getField('rating')) ?>" data-comment="<?php echo esc_attr($review->getField('comment')) ?>"><?php echo esc_html(spark_substrwords($review->getField('comment'), 20)) ?></a></td>
                                                <td><?php echo esc_html($review->getField('booking_unique_id')) ?></td>
                                                <td><?php echo esc_html($review->getField('course_name')) ?></td>
                                                <td><?php echo esc_html($review->getField('tutor_name')) ?></td>
                                                <td><?php echo esc_html($review->getField('student_name')) ?></td>
                                                <td><?php echo esc_html(date($date_format . ' H:i:s', strtotime($review->getField('created_date')))) ?></td>
                                            </tr>
                                        <?php }?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo HtmlHelper::render_review_modal() ?>
