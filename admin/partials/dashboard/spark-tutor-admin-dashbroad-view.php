<?php
$last_7_days_revenue = spark_caculate_revenue('-7 day');
$last_28_days_revenue = spark_caculate_revenue('-28 day');
$this_year_revenue = spark_caculate_revenue('this year');
$arg = array(
	'posts_per_page' => 10,
);
$recent_booking = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $arg);
$now_date = strtotime('now');
?>
<div class="wrapper ">
	<div class="main-panel" id="main-panel">
		<div class="row">
			<div class="col-md-12">
				<div class="card revenue-snapshot">
					<h5 class="text-light"><?php echo esc_html__('Revenue Snapshot', 'spark-tutor') ?></h5>
					<div class="row">
						<div class="col-sm-4">
							<label class="text-light">7 <?php echo esc_html__('Day Revenue', 'spark-tutor') ?></label>
							<p class="amount text-light display-4">$<?php echo esc_html($last_7_days_revenue) ?></p>
						</div>
						<div class="col-sm-4 text-align-center">
							<label class="text-light abc">28 <?php echo esc_html__('Day Revenue', 'spark-tutor') ?></label>
							<p class="amount text-light display-4">$<?php echo esc_html($last_28_days_revenue); ?></p>
						</div>
						<div class="col-sm-4">
							<label class="text-light"><?php echo esc_html__('Total This Year', 'spark-tutor') ?></label>
							<p class="amount text-light display-4">$<?php echo esc_html($this_year_revenue); ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Navbar -->
		<div class="card panel-header panel-header-lg mt-2">
<?php echo esc_html__('Revenue over Time', 'spark-tutor') ?>
			<h5 class="mt-2"></h5>
			<canvas id="bigDashboardChart"></canvas>
		</div>
		<div class="content">
			<div class="row">
				<div class="col-md-6">
					<div class="card card-chart">
						<div class="">
							<h5 class="card-title mt-2"><?php echo esc_html__('Booking Insight', 'spark-tutor') ?></h5>
						</div>
						<div class="">
							<div class="chart-area mt-3">
								<canvas id="lineChartExampleWithNumbersAndGrid"></canvas>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card recent-booking">
						<div class="">
							<h5 class="card-title mt-2"><?php echo esc_html__('Recent Bookings', 'spark-tutor') ?></h5>
						</div>
						<div class="<?php echo (count($recent_booking) > 4) ? 'scroll' : '' ?>">
							<div class="table-responsive">
								<table class="table">
									<?php foreach ($recent_booking as $key => $value) {
	$booking_date = strtotime($value->getField('created_date'));
	?>
										<tr>
											<td>
												<a href="<?php echo get_admin_url() . 'admin.php?page=spark-tutor-bookings&view=view&id=' . $value->getField('ID') ?>"><span class="font-weight-bold"><?php echo esc_html($value->getField('student_name')) ?></span> <?php echo esc_html__('booked a Course', 'spark-tutor') ?></a>
											</td>
											<td class="float-right">
												<?php echo calculate_time_distance($booking_date, $now_date) ?>
											</td>
										</tr>
									<?php }?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
