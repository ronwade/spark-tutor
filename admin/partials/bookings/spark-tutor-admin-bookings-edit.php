<?php require_once 'components/spark-tutor-admin-bookings-button-menu.php';
if (isset($_GET['id'])) {
	$booking = spark_get_entity_by_id($_GET['id']);
	$tutor = spark_get_entity_by_id($booking->getField('tutor'));
	$isEdit = true;
} else {
	$booking = new CPBooking();
	$tutor = new CPTutor();
	$isEdit = false;
}
$subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, null);
$subjectArray = array();
foreach ($subjects as $value) {
	$subjectArray[$value->getPost()->ID] = $value->getField('name');
}

$courses = CourseServices::get_courses_by_subject_id($booking->getField('subject'));
$courseArray = array();
foreach ($courses as $course) {
	$courseArray[$course->getPost()->ID] = $course->getField('name');
}

$paymentMethodArray = [
	SPARK_TUTOR_PAYMENT_METHOD_PAYPAL => SPARK_TUTOR_PAYMENT_METHOD_PAYPAL,
	SPARK_TUTOR_PAYMENT_METHOD_STRIPE => SPARK_TUTOR_PAYMENT_METHOD_STRIPE];

$statusArray = [
	SPARK_TUTOR_STATUS_CONFIRMED => SPARK_TUTOR_STATUS_CONFIRMED,
	SPARK_TUTOR_STATUS_INPROGRESS => SPARK_TUTOR_STATUS_INPROGRESS,
	SPARK_TUTOR_STATUS_FINISHED => SPARK_TUTOR_STATUS_FINISHED,
	SPARK_TUTOR_STATUS_CANCELLED => SPARK_TUTOR_STATUS_CANCELLED];
$date_format = spark_date_format();
?>
<form class="edit-form" id="form-tutor" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
    <input type="hidden" name="action" value="save_form_info">
    <input type="hidden" name="entity_type" value="<?php echo SPARK_TUTOR_BOOKING_POST_TYPE; ?>">
    <input type="hidden" name="id" value="<?php echo esc_attr($booking->getField('ID')) ?>">
    <input type="hidden" name="page" value="<?php echo esc_attr(spark_get_current_page()) ?>">
    <input type="hidden" name="view" value="<?php echo esc_attr(spark_get_current_view()) ?>">
    <input type="hidden" name="is_edit" value="<?php echo esc_attr($isEdit) ?>">
    <div class="row tutor-view">
        <div class="col-md-12">
            <div class="card">
                <div class="content-view mt-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="name">
                                <?php if (!$isEdit) {?>
                                    <h3 class="mb-4"><?php echo esc_html__('Create new Booking', 'spark-tutor') ?></h3>
                                <?php } else {?>
                                <h3 class="mb-4"><?php echo esc_html__('Edit Booking Details', 'spark-tutor') ?> - <?php echo esc_html($booking->getField('unique_id')) ?></h3>
                                <?php }?>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-2">
                        <h5><?php echo esc_html__('Student Information', 'spark-tutor') ?></h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="student_name" class="col-form-label"><?php echo esc_html__('Name', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="student_name" placeholder="<?php echo esc_html__('Student Name', 'spark-tutor') ?>" required="" value="<?php echo esc_attr($booking->getField('student_name')) ?>" name="student_name">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="student_phone" class="col-form-label"><?php echo esc_html__('Phone', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="student_phone" placeholder="<?php echo esc_html__('Phone', 'spark-tutor') ?>" required="" value="<?php echo esc_attr($booking->getField('student_phone')) ?>" name="student_phone">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="student_email" class="col-form-label"><?php echo esc_html__('Email', 'spark-tutor') ?></label>
                                        <input type="email" class="form-control" id="student_email" placeholder="<?php echo esc_html__('Email', 'spark-tutor') ?>" required="" value="<?php echo esc_attr($booking->getField('student_email')) ?>" name="student_email">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="student_grade" class="col-form-label"><?php echo esc_html__('Skype ID', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="student_grade" placeholder="<?php echo esc_html__('Skype ID', 'spark-tutor') ?>" required="" value="<?php echo esc_attr($booking->getField('student_grade')) ?>" name="student_grade">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($isEdit) {
	?>
                    <div class="col-sm-12 mt-4">
                        <h5><?php echo esc_html__('Tutor Information', 'spark-tutor') ?></h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="tutor_name" class="col-form-label"><?php echo esc_html__('Name', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="tutor_name" placeholder="" required="" value="<?php echo esc_attr($tutor->getField('name')) ?>" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="form1-password" class="col-form-label"><?php echo esc_html__('Phone', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="tutor_phone" placeholder="" required="" value="<?php echo esc_attr($tutor->getField('phone')) ?>" disabled>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="form1-email" class="col-form-label"><?php echo esc_html__('Email', 'spark-tutor') ?></label>
                                        <input type="email" class="form-control" id="tutor-email" placeholder="" required="" value="<?php echo esc_attr($tutor->getField('email')) ?>" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="form1-password" class="col-form-label"><?php echo esc_html__('TRN / SSN', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="tutor_trn_ssn" placeholder="TRN / SSN" required="" value="<?php echo esc_attr($tutor->getField('trn_ssn')) ?>" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php require_once 'components/spark-tutor-admin-bookings-edit-booking-information-form.php';
} else {
	require_once 'components/spark-tutor-admin-bookings-new-booking-information-form.php';
}?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row information">
                                <div class="col-sm-12">
                                <label for="note" class="col-form-label"><?php echo esc_html__('Note', 'spark-tutor') ?></label>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" id="note" name="note"><?php echo esc_textarea($booking->getField('note')) ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-4">
                        <h5><?php echo esc_html__('Payment Details', 'spark-tutor') ?></h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="payment_method" class="col-form-label"><?php echo esc_html__('Payment Method', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="payment_method" placeholder="<?php echo esc_html__('Payment Method', 'spark-tutor') ?>" value="<?php echo esc_attr($booking->getField('payment_method')) ?>" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price" class="col-form-label"><?php echo esc_html__('Price', 'spark-tutor') ?> ($)</label>
                                        <input type="text" class="form-control" id="course_price" placeholder="<?php echo esc_html__('Price', 'spark-tutor') ?>"  value="<?php echo esc_attr($booking->getField('price')) ?>" disabled>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="duration" class="col-form-label"><?php echo esc_html__('Duration', 'spark-tutor') ?> (mins)</label>
                                        <input type="text" class="form-control" id="duration" placeholder="<?php echo esc_html__('Duration', 'spark-tutor') ?>"  value="<?php echo esc_attr($booking->getField('duration')) ?>" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="form1-email" class="col-form-label"><?php echo esc_html__('Tax', 'spark-tutor') ?> ($)</label>
                                        <input type="text" class="form-control" id="tax" placeholder="<?php echo esc_html__('Tax', 'spark-tutor') ?>"  value="<?php echo esc_attr($booking->getField('tax')) ?>" disabled>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="form-group col-md-6">
                                        <label for="created_date" class="col-form-label"><?php echo esc_html__('Created Date', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="created_date" placeholder="<?php echo esc_html__('Created Date', 'spark-tutor') ?>"  value="<?php echo ($isEdit) ? date($date_format . ' H:i:m', strtotime($booking->getField('created_date'))) : '' ?>" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="total" class="col-form-label"><?php echo esc_html__('Total', 'spark-tutor') ?> ($)</label>
                                        <input type="text" class="form-control" id="total" placeholder="<?php echo esc_html__('Total', 'spark-tutor') ?>" value="<?php echo esc_attr($booking->getField('total')) ?>" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php require_once 'components/spark-tutor-admin-bookings-button-save.php'?>

                </div>
            </div>
        </div>
    </div>
</form>
