<div class="gutut twbs gu-tut-main-content">
    <div class="container">
        <?php
$view = spark_get_current_view();
if ($view == 'view') {
	require_once 'spark-tutor-admin-bookings-view.php';
} elseif ($view == 'edit') {
	require_once 'spark-tutor-admin-bookings-edit.php';
} else {
	require_once 'spark-tutor-admin-bookings-listing.php';
}
?>
    </div>
</div>
