<?php
require_once 'components/spark-tutor-admin-bookings-button-menu.php';
$booking = spark_get_entity_by_id($_GET['id']);
$tutor = spark_get_entity_by_id($booking->getField('tutor'));
$course = spark_get_entity_by_id($booking->getField('course'));
$subject = spark_get_entity_by_id($booking->getField('subject'));
$date_format = spark_date_format();
?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('View Booking Details', 'spark-tutor') ?> - <?php echo esc_html($booking->getField('unique_id')) ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="example col-md-12 ml-auto mr-auto">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo esc_html__('Student Information', 'spark-tutor') ?></h5>
                                    <p><?php echo esc_html__('Name', 'spark-tutor') ?>: <?php echo esc_html($booking->getField('student_name')) ?></p>
                                    <p><?php echo esc_html__('Phone', 'spark-tutor') ?>: <?php echo esc_html($booking->getField('student_phone')) ?> </p>
                                    <p><?php echo esc_html__('Email', 'spark-tutor') ?>: <?php echo esc_html($booking->getField('student_email')) ?></p>
                                    <p><?php echo esc_html__('Skype ID', 'spark-tutor') ?>: <?php echo esc_html($booking->getField('student_grade')) ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-12 mb-4 sm-hidden">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo esc_html__('Tutor Information', 'spark-tutor') ?></h5>
                                    <p><?php echo esc_html__('Name', 'spark-tutor') ?>: <?php echo esc_html($tutor->getField('name')) ?></p>
                                    <p><?php echo esc_html__('Phone', 'spark-tutor') ?>: <?php echo esc_html($tutor->getField('phone')) ?> </p>
                                    <p><?php echo esc_html__('Email', 'spark-tutor') ?>: <?php echo esc_html($tutor->getField('email')) ?></p>
                                    <p><?php echo esc_html__('TRN/SSN', 'spark-tutor') ?>: <?php echo esc_html($tutor->getField('trn_ssn')) ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 mb-4 sm-hidden">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo esc_html__('Booking Information', 'spark-tutor') ?></h5>
                                    <p><?php echo esc_html__('Subject', 'spark-tutor') ?>: <?php echo esc_html($subject->getField('name')) ?></p>
                                    <p><?php echo esc_html__('Course', 'spark-tutor') ?>: <?php echo esc_html($course->getField('name')) ?></p>
                                    <p><?php echo esc_html__('Booking Date', 'spark-tutor') ?>: <?php echo esc_html(date($date_format . ' H:i:s', strtotime($booking->getField('booking_date')))) ?></p>
                                    <p><?php echo esc_html__('Status', 'spark-tutor') ?>: <?php echo HtmlHelper::render_badge_label($booking->getField('status')) ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h5><?php echo esc_html__('Payment Details', 'spark-tutor') ?></h5>
                        <div class="col-sm-12">
                            <div class="information">
                                <table class="table table-striped">
                                    <tr>

                                        <th><?php echo esc_html__('Payment Method', 'spark-tutor') ?></th>
                                        <th><?php echo esc_html__('Created Date', 'spark-tutor') ?></th>
                                        <th><?php echo esc_html__('Duration', 'spark-tutor') ?></th>
                                        <th><?php echo esc_html__('Price', 'spark-tutor') ?></th>
                                        <th><?php echo esc_html__('Tax', 'spark-tutor') ?></th>
                                        <th><?php echo esc_html__('Total', 'spark-tutor') ?></th>
                                    </tr>
                                    <tr>
                                        <td><?php echo esc_html($booking->getField('payment_method')) ?></td>
                                        <td><?php echo esc_html(date($date_format . ' H:i:s', strtotime($booking->getField('created_date')))) ?></td>
                                        <td><?php echo esc_html($booking->getField('duration')) ?> <?php echo esc_html__('mins', 'spark-tutor') ?></td>
                                        <td>$<?php echo esc_html($booking->getField('price')) ?></td>
                                        <td>$<?php echo esc_html($booking->getField('tax')) ?></td>
                                        <td>$<?php echo esc_html($booking->getField('total')) ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row information">
                            <div class="col-sm-12 note-italic">
                                <p><?php echo esc_html__('Note', 'spark-tutor') ?>: <?php echo esc_html($booking->getField('note')) ?></p>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
