<?php
$view = spark_get_current_view();
$id = isset($_GET['id']) ? sanitize_text_field($_GET['id']) : null;
?>
<div class="tutor-view">
    <div class="card">
        <div class="action mb-2 d-inline btn-group mt-2" role="group">
            <a href="<?php echo spark_get_view_url('list') ?>" class="btn btn btn-outline-primary"><?php echo esc_html__('Back', 'spark-tutor') ?></a>
            <?php if ($view != 'edit') {?>
                <a href="<?php echo spark_get_view_url('edit', $id) ?>" class="btn btn btn-outline-primary"><i class="fa fa-edit"></i> <?php echo esc_html__('Edit', 'spark-tutor') ?></a>
                <button class="btn btn btn-outline-primary btn-update-status" data-status="Finished"><i class="fa fa-check"></i> <?php echo esc_html__('Mark Complete', 'spark-tutor') ?></button>
                <button class="btn dropdown-toggle btn-outline-primary" type="button" id="set-status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo esc_html__('Set Status', 'spark-tutor') ?>
                </button>
                <div class="dropdown-menu" aria-labelledby="set-status">
                    <button class="dropdown-item btn-update-status" data-status="Confirmed"><?php echo esc_html__('Confirmed', 'spark-tutor') ?></button>
                    <button class="dropdown-item btn-update-status" data-status="Cancelled"><?php echo esc_html__('Cancelled', 'spark-tutor') ?></button>
                </div>
            <?php } else {?>
                <a href="<?php echo spark_get_view_url('view') ?>" class="btn btn btn-outline-primary btn-form-submit" name="save_and_view"><i class="fa fa-eye"></i> <?php echo esc_html__('Save & View', 'spark-tutor') ?></a>

            <?php }?>


            <button class="btn btn btn-outline-primary btn-delete" data-toggle="modal" data-target="#deleteModal" post-id="<?php echo esc_attr($id) ?>"  ><i class="fa fa-trash"></i> <?php echo esc_html__('Delete', 'spark-tutor') ?></button>
        </div>
    </div>
</div>
<?php echo HtmlHelper::render_delete_modal($id) ?>
