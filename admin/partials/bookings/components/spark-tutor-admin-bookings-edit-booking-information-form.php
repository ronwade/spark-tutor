<div class="col-sm-12 mt-4">
    <h5><?php echo esc_html__('Booking Information', 'spark-tutor') ?></h5>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="subject" class="col-form-label"><?php echo esc_html__('Subject', 'spark-tutor') ?></label>
                    <fieldset>
                        <?php echo HtmlHelper::render_select($subjectArray, ['class' => 'custom-select col-sm-12', 'name' => 'subject', 'required' => 'true'], $booking->getField('subject')) ?>
                    </fieldset>
                </div>
                <div class="form-group col-md-6">
                    <label for="course" class="col-form-label"><?php echo esc_html__('Course', 'spark-tutor') ?></label>
                    <fieldset>
                        <?php echo HtmlHelper::render_select($courseArray, ['class' => 'custom-select col-sm-12', 'name' => 'course', 'required' => 'true'], $booking->getField('course')) ?>

                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="booking_date" class="col-form-label"><?php echo esc_html__('Booking Date', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="booking_date" placeholder="<?php echo esc_html__('Booking Date', 'spark-tutor') ?>" required="" value="<?php echo date($date_format . ' H:i:m', strtotime($booking->getField('booking_date'))) ?>" name="booking_date">
                </div>
                <div class="form-group col-md-6">
                    <label for="status" class="col-form-label"><?php echo esc_html__('Status', 'spark-tutor') ?></label>
                    <fieldset>
                        <?php echo HtmlHelper::render_select($statusArray, ['class' => 'custom-select col-sm-12', 'name' => 'status', 'required' => 'true'], $booking->getField('status')) ?>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
