<div class="col-sm-12 mt-4">
    <h5><?php echo esc_html__('Booking Information', 'spark-tutor') ?></h5>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="subject" class="col-form-label"><?php echo esc_html__('Subject', 'spark-tutor') ?></label>
                    <fieldset>
                        <?php echo HtmlHelper::render_select($subjectArray, ['class' => 'custom-select col-sm-12', 'name' => 'subject', 'required' => 'true']) ?>
                    </fieldset>
                </div>
                <div class="form-group col-md-6">
                    <label for="course" class="col-form-label"><?php echo esc_html__('Tutor', 'spark-tutor') ?></label>
                    <fieldset>
                        <?php echo HtmlHelper::render_select($courseArray, ['class' => 'custom-select col-sm-12', 'name' => 'tutor', 'required' => 'true']) ?>

                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="course" class="col-form-label"><?php echo esc_html__('Course', 'spark-tutor') ?></label>
                    <fieldset>
                        <?php echo HtmlHelper::render_select($courseArray, ['class' => 'custom-select col-sm-12', 'name' => 'course', 'required' => 'true']) ?>

                    </fieldset>
                </div>
                <div class="form-group col-md-6">
                    <label for="booking_date" class="col-form-label"><?php echo esc_html__('Booking Date', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="booking_date" placeholder="<?php echo esc_html__('Booking Date', 'spark-tutor') ?>" required="" value="<?php echo esc_attr($booking->getField('booking_date')) ?>" readonly>
                </div>

            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="payment_method" class="col-form-label"><?php echo esc_html__('Payment Method', 'spark-tutor') ?></label>
                    <fieldset>
                        <?php echo HtmlHelper::render_select($paymentMethodArray, ['class' => 'custom-select col-sm-12', 'name' => 'payment_method', 'required' => 'true']) ?>
                    </fieldset>
                </div>
                <div class="form-group col-md-6">
                    <label for="status" class="col-form-label"><?php echo esc_html__('Status', 'spark-tutor') ?></label>
                    <fieldset>
                        <?php echo HtmlHelper::render_select($statusArray, ['class' => 'custom-select col-sm-12', 'name' => 'status', 'required' => 'true']) ?>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="duration" id='course_duration' value="">
<input type="hidden" name="start_time" id="start_time" value = "">
<input type="hidden" name="end_time" id="end_time" value = "">
<input type="hidden" name="price" id="price" value = "">
<input type="hidden" name="tax" id="course_tax" value = "">
<input type="hidden" name="total" id="course_total" value = "">
<input type="hidden" name="booking_date" id="course_booking_date" value = "">
<div class="modal fade" id="bookingDateModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reportModalLabel"><?php echo esc_html__('Report', 'spark-tutor') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                   <div id="notification" class="alert alert-primary card" role="alert">
                    This time invalid !!!
                </div>
                    <div id="availability"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Save & Close', 'spark-tutor') ?></button>
            </div>
        </div>
    </div>
</div>
