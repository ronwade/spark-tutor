<div class="tutor-view">
    <div class="card">
        <div class="action mb-2 d-inline btn-group mt-2" role="group">
            <button class="btn btn btn-outline-primary"><?php echo esc_html__('Filter', 'spark-tutor') ?></button>
        </div>
    </div>
</div>
