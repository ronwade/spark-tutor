<div class="gutut twbs gu-tut-main-content">
    <div class="container">
        <?php
$view = spark_get_current_view();
if ($view == 'subject') {
	require_once 'spark-tutor-admin-analytics-subjects.php';
} else {
	require_once 'spark-tutor-admin-analytics-tutors.php';
}
?>
    </div>
</div>
