<?php
$bookings = BookingServices::search_booking_by_status(null, null);
$subject_stats = array();
foreach ($bookings as $booking) {
	$subjectId = $booking->getField('subject');
	if (!array_key_exists($subjectId, $subject_stats)) {
		$subject_stats[$subjectId]['all_booking'] = 0;
		$subject_stats[$subjectId][SPARK_TUTOR_STATUS_CONFIRMED] = 0;
		$subject_stats[$subjectId][SPARK_TUTOR_STATUS_FINISHED] = 0;
		$subject_stats[$subjectId][SPARK_TUTOR_STATUS_CANCELLED] = 0;
		$subject_stats[$subjectId]['total'] = 0;
	}
	$subject_stats[$subjectId]['all_booking'] += 1;
	$subject_stats[$subjectId][$booking->getField('status')] += 1;
	$subject_stats[$subjectId]['total'] += $booking->getField('price');

}
?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Analytics', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-10">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link " href="<?php echo spark_get_view_url('tutor') ?>" role="tab" aria-selected="<?php echo (!isset($status) || $status == '') ? 'true' : 'false' ?>"><?php echo esc_html__('Tutor', 'spark-tutor') ?></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="<?php echo spark_get_view_url('subject') ?>" role="tab" aria-selected="<?php echo ($status == 'active') ? 'true' : 'false' ?>"><?php echo esc_html__('Subject', 'spark-tutor') ?></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-2 mb-2">
								<form action="<?php echo (admin_url('admin-post.php')); ?>" method="POST">
									<input type="hidden" name="action" value="download_csv">
									<input type="hidden" name="type" value="subject">
									<button class="btn btn btn-outline-primary pull-right full-wdith"><?php echo esc_html__('Download', 'spark-tutor') ?></button>
								</form>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped mt-2 gu-datatable">
                                    <thead>
                                        <tr>
                                            <th class="width-th-25"><?php echo esc_html__('Subject', 'spark-tutor') ?></th>
                                            <th class="width-th-15"><?php echo esc_html__('All Bookings', 'spark-tutor') ?></th>
                                            <th class="width-th-15"><?php echo esc_html__('Confirmed', 'spark-tutor') ?></th>
                                            <th class="width-th-15"><?php echo esc_html__('Finished', 'spark-tutor') ?></th>
                                            <th class="width-th-15"><?php echo esc_html__('Cancelled', 'spark-tutor') ?></th>
                                            <th class="width-th-15"><?php echo esc_html__('Total Earnings', 'spark-tutor') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($subject_stats as $key => $value) {
	$subject = spark_get_entity_by_id($key);?>
                                        <tr>
                                            <td><?php echo esc_html($subject->getField('name')) ?></td>
                                            <td><?php echo esc_html($value['all_booking']) ?></td>
                                            <td><?php echo esc_html($value[SPARK_TUTOR_STATUS_CONFIRMED]) ?></td>
                                            <td><?php echo esc_html($value[SPARK_TUTOR_STATUS_FINISHED]) ?></td>
                                            <td><?php echo esc_html($value[SPARK_TUTOR_STATUS_CANCELLED]) ?></td>
                                            <td>$<?php echo esc_html($value['total']) ?></td>
                                        </tr>
                                    <?php }?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
