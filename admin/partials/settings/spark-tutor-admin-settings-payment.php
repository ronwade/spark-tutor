<?php $view = spark_get_current_view();?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Settings', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="scrollable">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link <?php echo (!isset($view) || $view == '' || $view == 'general') ? 'active' : '' ?> " id="home-tab" href="<?php echo spark_get_view_url('general') ?>" role="tab" aria-selected="<?php echo (!isset($view) || $view == '') ? 'true' : 'false' ?>"><?php echo esc_html__('General Setting', 'spark-tutor') ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($view == 'payment') ? 'active' : '' ?>"  href="<?php echo spark_get_view_url('payment') ?>" role="tab" aria-selected="<?php echo ($view == 'payment') ? 'true' : 'false' ?>"><?php echo esc_html__('Payment Setting', 'spark-tutor') ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($view == 'email') ? 'active' : '' ?>" href="<?php echo spark_get_view_url('email') ?>" role="tab" aria-selected="<?php echo ($view == 'email') ? 'true' : 'false' ?>"><?php echo esc_html__('Email Setting', 'spark-tutor') ?></a>
                            </li>
                        </ul>
                        </div>
                        <div class="paymethod-setting">
                            <div class="row">
                                <div class="custom-control custom-checkbox d-block my-2 col-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="enabled-paypal" <?php echo (isset($spark_tutor_options['enabled-paypal']) && $spark_tutor_options['enabled-paypal'] == 'on') ? 'checked=""' : '' ?>  name="enabled-paypal">
                                    <label class="custom-control-label checkbox-label" for="enabled-paypal"><?php echo esc_html__('PayPal', 'spark-tutor') ?></label>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="paypal-mode" class="col-form-label"><?php echo esc_html__('Mode', 'spark-tutor') ?></label>
                                        <fieldset>
                                            <select class="custom-select w-100" required="" name="paypal-mode">
                                                <option value="1" <?php echo (isset($spark_tutor_options['paypal-mode']) && $spark_tutor_options['paypal-mode'] == '1') ? 'selected' : '' ?> ><?php echo esc_html__('Test Mode', 'spark-tutor') ?></option>
                                                <option value="2" <?php echo (isset($spark_tutor_options['paypal-mode']) && $spark_tutor_options['paypal-mode'] == '2') ? 'selected' : '' ?>><?php echo esc_html__('Live Mode', 'spark-tutor') ?></option>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="paypal-email" class="col-form-label"> <?php echo esc_html__('PayPal Email', 'spark-tutor') ?></label>
                                        <input type="email" class="form-control" id="paypal-email" placeholder="<?php echo esc_html__('PayPal Email', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['paypal-email'])) ? $spark_tutor_options['paypal-email'] : '' ?>" name="paypal-email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="custom-control custom-checkbox d-block my-2 col-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="enabled_stripe" <?php echo (isset($spark_tutor_options['enabled_stripe']) && $spark_tutor_options['enabled_stripe'] == 'on') ? 'checked=""' : '' ?> name="enabled_stripe">
                                    <label class="custom-control-label checkbox-label" for="enabled_stripe"><?php echo esc_html__('Stripe', 'spark-tutor') ?></label>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="stripe_secret_key" class="col-form-label"> <?php echo esc_html__('Stripe Secret Key', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="stripe_secret_key" placeholder="<?php echo esc_html__('Stripe Secret Key', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['stripe_secret_key'])) ? $spark_tutor_options['stripe_secret_key'] : '' ?>" name="stripe_secret_key">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="stripe_public_key" class="col-form-label"><?php echo esc_html__('Stripe Public Key', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="stripe_public_key" placeholder="<?php echo esc_html__('Stripe Public Key', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['stripe_public_key'])) ? $spark_tutor_options['stripe_public_key'] : '' ?>" name="stripe_public_key">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="setting_option" value="payment">
                            <button type="submit" class="btn btn-outline-primary pull-right mb-2"><?php echo esc_html__('Save Changes', 'spark-tutor') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
