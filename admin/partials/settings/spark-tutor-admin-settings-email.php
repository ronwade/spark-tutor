<?php $view = spark_get_current_view();?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Settings', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="scrollable">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?php echo (!isset($view) || $view == '' || $view == 'general') ? 'active' : '' ?> " id="home-tab" href="<?php echo spark_get_view_url('general') ?>" role="tab" aria-selected="<?php echo (!isset($view) || $view == '') ? 'true' : 'false' ?>"><?php echo esc_html__('General Setting', 'spark-tutor') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($view == 'payment') ? 'active' : '' ?>" href="<?php echo spark_get_view_url('payment') ?>" role="tab" aria-selected="<?php echo ($view == 'payment') ? 'true' : 'false' ?>"><?php echo esc_html__('Payment Setting', 'spark-tutor') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($view == 'email') ? 'active' : '' ?>"  href="<?php echo spark_get_view_url('email') ?>" role="tab" aria-selected="<?php echo ($view == 'email') ? 'true' : 'false' ?>"><?php echo esc_html__('Email Setting', 'spark-tutor') ?></a>
                                </li>
                            </ul>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped mt-2">
                                <tbody>
                                    <tr>
                                        <th class="width-th-10"><?php echo esc_html__('Enabled', 'spark-tutor') ?></th>
                                        <th class="width-th-50"><?php echo esc_html__('Email', 'spark-tutor') ?></th>
                                        <th class="width-th-30"><?php echo esc_html__('Recipient', 'spark-tutor') ?></th>
                                        <th class="width-th-10"><?php echo esc_html__('Action', 'spark-tutor') ?></th>
                                    </tr>
                                    <tr>
                                        <td class="text-vertical-align admin-enabled-email"><?php echo (isset($spark_tutor_options['enabled_admin_email']) && $spark_tutor_options['enabled_admin_email'] == 'on') ? '<i class="fa fa-check" aria-hidden="true"></i>' : '' ?></td>
                                        <td class="bold-text text-vertical-align"><?php echo esc_html__('Admin Notification', 'spark-tutor') ?> - <?php echo esc_html__('Successful Booking', 'spark-tutor') ?></td>
                                        <td class="text-vertical-align"><?php echo esc_html__('Admin Email', 'spark-tutor') ?></td>
                                        <td><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#adminNotifictionModal"><i class="fa fa-cog" aria-hidden="true"></i></button></td>
                                    </tr>
                                        <td class="text-vertical-align tutor-registration-enabled-email"><?php echo (isset($spark_tutor_options['enabled_tutor_registration_email']) && $spark_tutor_options['enabled_tutor_registration_email'] == 'on') ? '<i class="fa fa-check" aria-hidden="true"></i>' : '' ?></td>
                                        <td class="bold-text text-vertical-align"><?php echo esc_html__('Tutor Notification', 'spark-tutor') ?> - <?php echo esc_html__('Registration', 'spark-tutor') ?></td>
                                        <td class="text-vertical-align"><?php echo esc_html__('Tutor Email', 'spark-tutor') ?></td>
                                        <td><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#tutorRegistrationNotifictionModal"><i class="fa fa-cog" aria-hidden="true"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td class="text-vertical-align tutor-enabled-email"><?php echo (isset($spark_tutor_options['enabled_tutor_email']) && $spark_tutor_options['enabled_tutor_email'] == 'on') ? '<i class="fa fa-check" aria-hidden="true"></i>' : '' ?></td>
                                        <td class="bold-text text-vertical-align"><?php echo esc_html__('Tutor Notification', 'spark-tutor') ?> - <?php echo esc_html__('Successful Booking', 'spark-tutor') ?></td>
                                        <td class="text-vertical-align"><?php echo esc_html__('Tutor Email', 'spark-tutor') ?></td>
                                        <td><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#tutorNotifictionModal"><i class="fa fa-cog" aria-hidden="true"></i></button></td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <td class="text-vertical-align student-enabled-email"><?php echo (isset($spark_tutor_options['enabled_student_email']) && $spark_tutor_options['enabled_student_email'] == 'on') ? '<i class="fa fa-check" aria-hidden="true"></i>' : '' ?></td>
                                        <td class="bold-text text-vertical-align"><?php echo esc_html__('Student Notification', 'spark-tutor') ?> - <?php echo esc_html__('Successful Booking', 'spark-tutor') ?></td>
                                        <td class="text-vertical-align"><?php echo esc_html__('Student Email', 'spark-tutor') ?></td>
                                        <td><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#studentNotifictionModal"><i class="fa fa-cog" aria-hidden="true"></i></button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <h5 class="col-sm-6"><?php echo esc_html__('SMTP', 'spark-tutor') ?> <?php echo esc_html__('Settings', 'spark-tutor') ?></h5>
                            <h5 class="col-sm-6"><?php echo esc_html__('Send Test Email', 'spark-tutor') ?></h5>
                        </div>
                        <div class="row mb-2 general-setting">
                            <div class="col-md-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="host" class="col-form-label"><?php echo esc_html__('Host', 'spark-tutor') ?></label>
                                            <input type="text" class="form-control" id="host" placeholder="<?php echo esc_html__('Host', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['host'])) ? $spark_tutor_options['host'] : 'mail.jamaicans.dev' ?>" name="host">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="from_email" class="col-form-label"><?php echo esc_html__('From Email', 'spark-tutor') ?></label>
                                            <input type="email" class="form-control" id="from_email" placeholder="<?php echo esc_html__('From Email', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['from_email'])) ? $spark_tutor_options['from_email'] : 'hello@spark-education.com' ?>" name="from_email">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="from_name" class="col-form-label"><?php echo esc_html__('From Name', 'spark-tutor') ?></label>
                                            <input type="text" class="form-control" id="from_name" placeholder="<?php echo esc_html__('From Name', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['from_name'])) ? $spark_tutor_options['from_name'] : 'Spark Education' ?>" name="from_name">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="smtp_username" class="col-form-label"><?php echo esc_html__('Username', 'spark-tutor') ?></label>
                                            <input type="email" class="form-control" id="smtp_username" placeholder="<?php echo esc_html__('Username', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['smtp_username'])) ? $spark_tutor_options['smtp_username'] : 'mailer@jamaicans.dev' ?>" name="smtp_username">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="smtp_password" class="col-form-label"><?php echo esc_html__('Password', 'spark-tutor') ?></label>
                                            <input type="password" class="form-control" id="smtp_password" placeholder="<?php echo esc_html__('Password', 'spark-tutor') ?>" required="" name="smtp_password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="smtp_encryption" class="col-form-label"><?php echo esc_html__('Encryption', 'spark-tutor') ?></label>
                                        <div class="custom-controls-example col-md-3 col-sm-3 col-xs-12">

                                            <fieldset>
                                                <div class=" d-block my-2">
                                                    <input type="radio" name="smtp_encryption" value="0" class="" <?php echo (isset($spark_tutor_options['smtp_encryption']) && $spark_tutor_options['smtp_encryption'] == '0') ? 'checked=""' : '' ?>>
                                                    <label class="" for="smtp_encryption"><?php echo esc_html__('None', 'spark-tutor') ?></label>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="custom-controls-example col-md-3 col-sm-3 col-xs-12">
                                            <fieldset>
                                                <div class="d-block my-2">
                                                    <input type="radio" name="smtp_encryption" value="1" class="" <?php echo (isset($spark_tutor_options['smtp_encryption']) && $spark_tutor_options['smtp_encryption'] == '1') ? 'checked=""' : '' ?>>
                                                    <label class="" for="smtp_encryption"><?php echo esc_html__('SSL', 'spark-tutor') ?></label>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="custom-controls-example col-md-3 col-sm-3 col-xs-12">
                                            <fieldset>
                                                <div class="d-block my-2">
                                                    <input type="radio" name="smtp_encryption" value="2" class="" <?php echo (isset($spark_tutor_options['smtp_encryption']) && $spark_tutor_options['smtp_encryption'] == '2') ? 'checked=""' : '' ?>>
                                                    <label class="" for="smtp_encryption"><?php echo esc_html__('TLS', 'spark-tutor') ?></label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="port" class="col-form-label"><?php echo esc_html__('Port', 'spark-tutor') ?></label>
                                            <input type="text" class="form-control" id="port" placeholder="<?php echo esc_html__('Port', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['port'])) ? $spark_tutor_options['port'] : '465' ?>" name="port">
                                        </div>
                                    </div>
                                    <input type="hidden" name="setting_option" value="email">

                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group">
										<div id="sent" style="color: green ;max-width: 430px; display: flex; flex-wrap: wrap"></div>
										<div id="not-sent" style="color: red; max-width: 430px"></div>
                                        <label for="test_email" class="col-form-label"><?php echo esc_html__('Input Test Email', 'spark-tutor') ?></label>
                                        <input type="text" class="form-control" id="test_email" placeholder="<?php echo esc_html__('Input Test Email', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['test_email'])) ? $spark_tutor_options['test_email'] : '' ?>" name="test_email">
                                    </div>
                                </div>
                                <button id="button_test_email" type="button"  class="btn btn-outline-primary pull-right"><?php echo esc_html__('Send', 'spark-tutor') ?></button>

								<span style="padding: 3px 0px 3px; display: none" class="load-icon"><img src="<?php echo SPARK_TUTOR_PLUGIN_URL . "admin/img/load.gif" ?>" height="42" width="42"> <?php echo esc_html__('Email sending', 'spark-tutor') ?></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-outline-primary pull-right mb-2"><?php echo esc_html__('Save Changes', 'spark-tutor') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="adminNotifictionModal" tabindex="-1" role="dialog" aria-labelledby="adminNotifictionLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="adminNotifictionLabel"><?php echo esc_html__('Admin Email', 'spark-tutor') ?> - <?php echo esc_html__('Successful Booking', 'spark-tutor') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset>
                    <div class="custom-control custom-checkbox d-block my-2">
                        <input type="checkbox" class="custom-control-input" id="enabled_admin_email" <?php echo (isset($spark_tutor_options['enabled_admin_email']) && $spark_tutor_options['enabled_admin_email'] == 'on') ? 'checked=""' : '' ?> name="enabled_admin_email">
                        <label class="custom-control-label checkbox-label" for="enabled_admin_email"><?php echo esc_html__('Enable this email notification', 'spark-tutor') ?></label>
                    </div>
                </fieldset>
                <div class="form-group col-md-12">
                    <label for="admin_email_subject" class="col-form-label"><?php echo esc_html__('Email Subject', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="admin_email_subject" placeholder="<?php echo esc_html__('Email Subject', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['admin_email_subject'])) ? $spark_tutor_options['admin_email_subject'] : '' ?>" name="admin_email_subject">
                </div>
                <div class="form-group col-md-12">
                    <label for="admin_email_heading" class="col-form-label"><?php echo esc_html__('Email Heading', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="admin_email_heading" placeholder="<?php echo esc_html__('Email Heading', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['admin_email_heading'])) ? $spark_tutor_options['admin_email_heading'] : '' ?>" name="admin_email_heading">
                </div>
                <div class="form-group col-md-12">
                    <label for="admin_email_heading" class="col-form-label"><?php echo esc_html__('Email Admin', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="admin_email" placeholder="<?php echo esc_html__('Email Admin', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['admin_email'])) ? $spark_tutor_options['admin_email'] : '' ?>" name="admin_email">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Save & Close', 'spark-tutor') ?></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tutorRegistrationNotifictionModal" tabindex="-1" role="dialog" aria-labelledby="tutorRegistrationNotifictionLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tutorRegistrationNotifictionLabel"><?php echo esc_html__('Tutor Email', 'spark-tutor') ?> - <?php echo esc_html__('Registration', 'spark-tutor') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset>
                    <div class="custom-control custom-checkbox d-block my-2">
                        <input type="checkbox" class="custom-control-input" id="enabled_tutor_registration_email" <?php echo (isset($spark_tutor_options['enabled_tutor_registration_email']) && $spark_tutor_options['enabled_tutor_registration_email'] == 'on') ? 'checked=""' : '' ?> name="enabled_tutor_registration_email">
                        <label class="custom-control-label checkbox-label" for="enabled_tutor_registration_email"><?php echo esc_html__('Enable this email notification', 'spark-tutor') ?></label>
                    </div>
                </fieldset>
                <div class="form-group col-md-12">
                    <textarea  class="form-control"  placeholder="Enter the email to send to tutors upon registration" name="tutor_registration_message" id="tutor_registration_message"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Save & Close', 'spark-tutor') ?></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tutorNotifictionModal" tabindex="-1" role="dialog" aria-labelledby="tutorNotifictionLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tutorNotifictionLabel"><?php echo esc_html__('Tutor Email', 'spark-tutor') ?> - <?php echo esc_html__('Successful Booking', 'spark-tutor') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset>
                    <div class="custom-control custom-checkbox d-block my-2">
                        <input type="checkbox" class="custom-control-input" id="enabled_tutor_email" <?php echo (isset($spark_tutor_options['enabled_tutor_email']) && $spark_tutor_options['enabled_tutor_email'] == 'on') ? 'checked=""' : '' ?> name="enabled_tutor_email">
                        <label class="custom-control-label checkbox-label" for="enabled_tutor_email"><?php echo esc_html__('Enable this email notification', 'spark-tutor') ?></label>
                    </div>
                </fieldset>
                <div class="form-group col-md-12">
                    <label for="tutor_email_subject" class="col-form-label"><?php echo esc_html__('Email Subject', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="tutor_email_subject" placeholder="<?php echo esc_html__('Email Subject', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['tutor_email_subject'])) ? $spark_tutor_options['tutor_email_subject'] : '' ?>" name="tutor_email_subject">
                </div>
                <div class="form-group col-md-12">
                    <label for="tutor_email_heading" class="col-form-label"><?php echo esc_html__('Email Heading', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="tutor_email_heading" placeholder="<?php echo esc_html__('Email Heading', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['tutor_email_heading'])) ? $spark_tutor_options['tutor_email_heading'] : '' ?>" name="tutor_email_heading">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Save & Close', 'spark-tutor') ?></button>
            </div>
        </div>
    </div>
</div>






<div class="modal fade" id="studentNotifictionModal" tabindex="-1" role="dialog" aria-labelledby="studentNotifictionLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="studentNotifictionLabel"><?php echo esc_html__('Student Email', 'spark-tutor') ?> - <?php echo esc_html__('Successful Booking', 'spark-tutor') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset>
                    <div class="custom-control custom-checkbox d-block my-2">
                        <input type="checkbox" class="custom-control-input" id="enabled_student_email"  <?php echo (isset($spark_tutor_options['enabled_student_email']) && $spark_tutor_options['enabled_student_email'] == 'on') ? 'checked=""' : '' ?> name="enabled_student_email">
                        <label class="custom-control-label checkbox-label" for="enabled_student_email"><?php echo esc_html__('Enable this email notification', 'spark-tutor') ?></label>
                    </div>
                </fieldset>
                <div class="form-group col-md-12">
                    <label for="student_email_subject" class="col-form-label"><?php echo esc_html__('Email Subject', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="student_email_subject" placeholder="<?php echo esc_html__('Email Subject', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['student_email_subject'])) ? $spark_tutor_options['student_email_subject'] : '' ?>" name="student_email_subject">
                </div>
                <div class="form-group col-md-12">
                    <label for="student_email_heading" class="col-form-label"><?php echo esc_html__('Email Heading', 'spark-tutor') ?></label>
                    <input type="text" class="form-control" id="student_email_heading" placeholder="<?php echo esc_html__('Email Heading', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['student_email_heading'])) ? $spark_tutor_options['student_email_heading'] : '' ?>" name="student_email_heading">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Save & Close', 'spark-tutor') ?></button>
            </div>
        </div>
    </div>
</div>
