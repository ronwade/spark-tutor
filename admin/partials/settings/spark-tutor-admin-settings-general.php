<?php
$view = spark_get_current_view();

global $wpdb;
$query = "SELECT ID, post_title FROM " . $wpdb->posts . " WHERE post_content LIKE '%[%' AND post_status = 'publish'";
$results = $wpdb->get_results($query);
?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Settings', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="scrollable">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?php echo (!isset($view) || $view == '' || $view == 'general') ? 'active' : '' ?> " id="home-tab" href="<?php echo spark_get_view_url('general') ?>" role="tab" aria-selected="<?php echo (!isset($view) || $view == '') ? 'true' : 'false' ?>"><?php echo esc_html__('General Setting', 'spark-tutor') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($view == 'payment') ? 'active' : '' ?>"  href="<?php echo spark_get_view_url('payment') ?>" role="tab" aria-selected="<?php echo ($view == 'payment') ? 'true' : 'false' ?>"><?php echo esc_html__('Payment Setting', 'spark-tutor') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($view == 'email') ? 'active' : '' ?>" href="<?php echo spark_get_view_url('email') ?>" role="tab" aria-selected="<?php echo ($view == 'email') ? 'true' : 'false' ?>"><?php echo esc_html__('Email Setting', 'spark-tutor') ?></a>
                                </li>
                            </ul>
                        </div>

                        <div class="row mb-2 general-setting">
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="booking-page" class="col-form-label"><?php echo esc_html__('Booking Page', 'spark-tutor') ?></label>
                                            <select class="custom-select w-100" id="booking-page" name="booking-page">
	                                            <?php foreach ($results as $results) {?>
                                                    <option value="none" selected disabled hidden>
                                                        ----Select----
                                                    </option>
                                                    <option value="<?php echo esc_attr($results->ID) ?>" <?php echo (isset($spark_tutor_options['booking-page']) && $spark_tutor_options['booking-page'] == $results->ID) ? 'selected' : '' ?> >
                                                        <?php echo esc_html($results->post_title); ?>
                                                    </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="product-description" class="col-form-label"><?php echo esc_html__('Product Description', 'spark-tutor') ?></label>
                                            <input type="text" class="form-control" id="product-description" placeholder="<?php echo esc_html__('Product Description', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['product-description']) ? $spark_tutor_options['product-description'] : '') ?>" name="product-description">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="tutor_onboarding_course" class="col-form-label"><?php echo esc_html__('URL or Path to Tutor onboarding course', 'spark-tutor') ?> </label>
                                            <input type="text" class="form-control" id="form1-name" placeholder="<?php echo esc_html__('tutor_onboarding_course', 'spark-tutor') ?>" value="<?php echo tutor_onboarding_course_url(); ?>" name="tutor_onboarding_course">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="tax" class="col-form-label"><?php echo esc_html__('Tax', 'spark-tutor') ?> (%)</label>
                                            <input type="text" class="form-control" id="form1-name" placeholder="<?php echo esc_html__('Tax', 'spark-tutor') ?>" value="<?php echo (isset($spark_tutor_options['tax'])) ? $spark_tutor_options['tax'] : '' ?>" name="tax">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="date-format" class="mb-3 mt-1"><?php echo esc_html__('Date Format', 'spark-tutor') ?></label>
                                            <select class="custom-select w-100" name="date-format">
                                                    <option <?php echo (isset($spark_tutor_options['date-format']) && $spark_tutor_options['date-format'] == 'MM-DD-YYYY') ? 'selected' : '' ?>>MM-DD-YYYY</option>
                                                    <option <?php echo (isset($spark_tutor_options['date-format']) && $spark_tutor_options['date-format'] == 'DD-MM-YYYY') ? 'selected' : '' ?>>DD-MM-YYYY</option>
                                                    <option <?php echo (isset($spark_tutor_options['date-format']) && $spark_tutor_options['date-format'] == 'YYYY-MM-DD') ? 'selected' : '' ?>>YYYY-MM-DD</option>
                                                </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <div>
                                                <label for="form1-name" class="col-form-label"><?php echo esc_html__('Terms and Conditions', 'spark-tutor') ?></label>
                                            </div>
                                            <div>
                                                <button type="button" class="btn btn-outline-info w-100" data-toggle="modal" data-target="#edit-term-and-condition"><?php echo esc_html__('Edit', 'spark-tutor') ?></button>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <div>
                                                <label for="form1-name" class="col-form-label"><?php echo esc_html__('Import Sample Data', 'spark-tutor') ?></label>
                                            </div>
                                            <div>
                                                <button type="button" class="btn btn-outline-info w-100 import-sample-data" onclick="return confirm('Are you sure to delete all appointments data and import sample data?')" data-url='<?php echo admin_url('admin-ajax.php'); ?>'><?php echo esc_html__('Import', 'spark-tutor') ?></button>
                                            </div>
                                        </div>
                                    </div>

                            </div>
                        </div>
                        <input type="hidden" name="setting_option" value="general">
                                    <button type="submit" class="btn btn-outline-primary pull-right mb-2"><?php echo esc_html__('Save Changes', 'spark-tutor') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-term-and-condition" tabindex="-1" role="dialog" aria-labelledby="edit-term-and-condition-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reviewModalLabel"><?php echo esc_html__('Edit Terms And Conditions', 'spark-tutor') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div>
                       <?php
wp_editor($spark_tutor_options['term_condition_content'], "term_condition_content", $settings = array("media_buttons" => false));
?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Save & Close', 'spark-tutor') ?></button>
            </div>
        </div>
    </div>
</div>
