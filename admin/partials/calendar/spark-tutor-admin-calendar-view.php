<?php
$list_tutor = spark_get_list_entity(SPARK_TUTOR_TUTOR_POST_TYPE, []);
$list_subject = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, []);
?>
<div class="tutor-view">
	<div class="card">
		<div class="action mb-2 d-inline btn-group mt-2" role="group">
			<a href="<?php echo get_admin_url() . "admin.php?page=spark-tutor-bookings&view=edit" ?>" class="btn btn btn-outline-primary"><i class="fa fa-plus"></i> <?php echo esc_html__('Add New Booking', 'spark-tutor') ?></a>
		</div>
	</div>
</div>
<div class="row tutor-view">
	<div class="col-md-12">
		<div class="card">
			<div class="content-view mt-4">
				<div class="row">
					<div class="col-sm-12">
						<div class="name">
							<h3 class="mb-4"><?php echo esc_html__('Calendar', 'spark-tutor') ?></h3>
						</div>
						<hr>
					</div>
				</div>
				<div class="row calendar-view">
					<div class="col-sm-10">
						<div id="full-calendar"></div>
					</div>
					<div class="col-sm-2 mt-2">
						<h5><?php echo esc_html__('Fillters', 'spark-tutor') ?></h5>
						<div class="form-group">
							<label for="tutor"><?php echo esc_html__('Tutor', 'spark-tutor') ?></label>
							<select  class="form-control" id="tutor">
								<option value=""><?php echo esc_html__('All Tutors', 'spark-tutor') ?></option>
								<?php foreach ($list_tutor as $key => $value): ?>
									<option value="<?php echo esc_attr($value->getField('ID')) ?>"><?php echo esc_html($value->getField('name')) ?></option>
								<?php endforeach?>
							</select>
						</div>
						<div class="form-group">
							<label for="subject"><?php echo esc_html__('Subject', 'spark-tutor') ?></label>
							<select  class="form-control" id="subject">
								<option value=""><?php echo esc_html__('All Subjects', 'spark-tutor') ?></option>
								<?php foreach ($list_subject as $key => $value): ?>
									<option value="<?php echo esc_attr($value->getField('ID')) ?>"><?php echo esc_html($value->getField('name')) ?></option>
								<?php endforeach?>
							</select>
						</div>
						<div class="form-group">
							<label for="status"><?php echo esc_html__('Status', 'spark-tutor') ?></label>
							<select  class="form-control" id="status">
								<option value=""><?php echo esc_html__('All Statuses', 'spark-tutor') ?></option>
								<option><?php echo esc_html__('Confirmed', 'spark-tutor') ?></option>
								<option><?php echo esc_html__('InProgress', 'spark-tutor') ?></option>
								<option><?php echo esc_html__('Finished', 'spark-tutor') ?></option>
								<option><?php echo esc_html__('Cancelled', 'spark-tutor') ?></option>
							</select>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-6"><button type="button" id='fillter' class="btn btn-outline-primary btn-sm"><?php echo esc_html__('Fillters', 'spark-tutor') ?></button></div>
								<div class="col-sm-6"><button type="button" id="clear" class="btn btn-outline-secondary btn-sm"><?php echo esc_html__('Clear', 'spark-tutor') ?></button></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
