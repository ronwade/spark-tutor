<?php
require_once 'components/spark-tutor-admin-subjects-button-menu.php';
$subject = spark_get_entity_by_id($_GET['id']);
$courses = CourseServices::get_courses_by_subject_id($subject->getField('ID'));
$bookings = BookingServices::get_bookings_by_subject_id($subject->getField('ID'));
$date_format = spark_date_format();
?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html($subject->getField('name')) ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row mt-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5><?php echo esc_html__('Course List', 'spark-tutor') ?></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-striped">
                                        <tr>
                                            <th class="width-th-25"><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                                            <th class="width-th-25"><?php echo esc_html__('Tutor', 'spark-tutor') ?></th>
                                            <th class="width-th-25"><?php echo esc_html__('Duration', 'spark-tutor') ?></th>
                                            <th class="width-th-25"><?php echo esc_html__('Price', 'spark-tutor') ?></th>
                                        </tr>
                                        <?php foreach ($courses as $course) {
	$tutor = spark_get_entity_by_id($course->getField('tutor'))?>
                                        <tr>
                                            <td><?php echo esc_html($course->getField('name')) ?></td>
                                            <td><?php echo esc_html($tutor->getField('name')) ?></td>
                                            <td><?php echo esc_html($course->getField('duration')) ?> <?php echo esc_html__('mins', 'spark-tutor') ?></td>
                                            <td>$<?php echo esc_html($course->getField('price')) ?></td>
                                        </tr>
                                    <?php }?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row mt-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5><?php echo esc_html__('Enrollment List', 'spark-tutor') ?></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-striped">
                                        <tr>
                                            <th class="width-th-25"><?php echo esc_html__('Unique Booking ID', 'spark-tutor') ?></th>
                                            <th class="width-th-25"><?php echo esc_html__('Student', 'spark-tutor') ?></th>
                                            <th class="width-th-25"><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                                            <th class="width-th-25"><?php echo esc_html__('Booking Date', 'spark-tutor') ?></th>
                                        </tr>
                                        <?php foreach ($bookings as $booking) {
	$course = spark_get_entity_by_id($booking->getField('tutor'))?>
                                        <tr>
                                            <td><?php echo esc_html($booking->getField('unique_id')) ?></td>
                                            <td><?php echo esc_html($booking->getField('student_name')) ?></td>
                                            <td><?php echo esc_html($course->getField('name')) ?></td>
                                            <td><?php echo date($date_format . ' H:i:s', strtotime($booking->getField('booking_date'))) ?></td>
                                        </tr>
                                    <?php }?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

