<?php
$subject = spark_get_entity_by_id($_GET['id']);
$courses = CourseServices::get_courses_by_subject_id($subject->getField('ID'));
require_once 'components/spark-tutor-admin-subjects-button-menu.php';
?>
<form id="subject-form" class="edit-form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
    <input type="hidden" name="action" value="save_form_info">
    <input type="hidden" name="entity_type" value="<?php echo SPARK_TUTOR_SUBJECT_POST_TYPE; ?>">
    <input type="hidden" name="id" value="<?php echo esc_attr($subject->getField('ID')) ?>">
    <input type="hidden" name="page" value="<?php echo esc_attr(spark_get_current_page()) ?>">
    <input type="hidden" name="view" value="<?php echo esc_attr(spark_get_current_view()) ?>">
    <div class="row tutor-view">
        <div class="col-md-12">
            <div class="card">
                <div class="content-view mt-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="name">
                                <?php if (isset($_GET['id'])) {?>
                                <h3 class="mb-4"><?php echo esc_html__('Edit Subject', 'spark-tutor') ?></h3>
                            <?php } else {?>
                                <h3 class="mb-4"><?php echo esc_html__('New Subject', 'spark-tutor') ?></h3>
                                <?php }?>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-6 mb-4">
                                <h5><?php echo esc_html__('Subject Name', 'spark-tutor') ?></h5>
                                <input type="text" id="subject_name" class="form-control"  name="name" placeholder="<?php echo esc_html__('Subject Name', 'spark-tutor') ?>" value="<?php echo esc_attr($subject->getField('name')) ?>" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-form-label"><?php echo esc_html__('Picture', 'spark-tutor') ?></label>
                                <fieldset>
                                    <div class="custom-file w-100">
                                        <input type="file" class="custom-file-input" id="customFileSubject" name="picture" <?php echo ($subject->getField('picture') == null || $subject->getField('picture') == '') ? 'required' : '' ?> >
                                        <label class="custom-file-label" for="picture"><?php echo esc_html__('Choose file', 'spark-tutor') ?>...</label>
                                    </div>
                                </fieldset>
                            </div>
                            <?php if (isset($_GET['id'])) {
	?>
                            <div class="col-sm-12 mt-4 mb-4">
                                <div class="row">
                                    <h5><?php echo esc_html__('Course List', 'spark-tutor') ?></h5>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 table-responsive">
                                        <table class="table table-striped">
                                            <tr>
                                                <th class="width-th-20"><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                                                <th class="width-th-20"><?php echo esc_html__('Tutor', 'spark-tutor') ?></th>
                                                <th class="width-th-20"><?php echo esc_html__('Duration', 'spark-tutor') ?></th>
                                                <th class="width-th-20"><?php echo esc_html__('Price', 'spark-tutor') ?></th>
                                                <th class="width-th-20"><?php echo esc_html__('Action', 'spark-tutor') ?></th>
                                            </tr>
                                            <?php foreach ($courses as $course) {
		$tutor = spark_get_entity_by_id($course->getField('tutor'))?>
                                            <tr>
                                                <td class="text-vertical-align"><?php echo esc_html($course->getField('name')) ?></td>
                                                <td class="text-vertical-align"><?php echo esc_html($tutor->getField('name')) ?></td>
                                                <td class="text-vertical-align"><?php echo esc_html($course->getField('duration')) ?> <?php echo esc_html__('mins', 'spark-tutor') ?></td>
                                                <td class="text-vertical-align">$<?php echo esc_html($course->getField('price')) ?></td>
                                                <td class="position-relative fix-child-item-middle"><button data-toggle="modal" data-target="#deleteModal" class="btn btn-outline-danger btn-delete position-absolute" post-id='<?php echo esc_attr($course->getField('ID')) ?>'><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                            <?php }?>
                                        </table>
                                    </div>
                                </div>
                            </div>
<?php }?>

                    </div>
                    <?php require_once 'components/spark-tutor-admin-subjects-button-save.php'?>
                </div>
            </div>
        </div>
    </div>
</form>
