<?php
$keyword = isset($_GET['keyword']) ? sanitize_text_field($_GET['keyword']) : '';
if (empty($keyword)) {
	$subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, null);
} else {
	$subjects = SubjectServices::search_subject($keyword);
}
$date_format = spark_date_format();
?>
<div class="tutor-view">
    <div class="card">
        <div class="action mb-2 d-inline btn-group mt-2" role="group">
            <a href="<?php echo spark_get_view_url('edit') ?>" class="btn btn btn-outline-primary"><i class="fa fa-plus"></i> <?php echo esc_html__('Create New Subject', 'spark-tutor') ?></a>
        </div>
    </div>
</div>
<div class="row tutor-view booking-list">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Subjects', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-sm-12 table-responsive">
                        <table class="table table-striped list-subject gu-datatable ">
                            <thead>
                                <tr>
                                    <th><?php echo esc_html__('Subject', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Course Count', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Student Enrollment', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Created Date', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Action', 'spark-tutor') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
foreach ($subjects as $subject) {
	$courses = CourseServices::get_courses_by_subject_id($subject->getField('ID'));
	$bookings = BookingServices::get_bookings_by_subject_id($subject->getField('ID'));
	?>
                                <tr >
                                    <td class="text-vertical-align"><?php echo esc_html($subject->getField('name')) ?></td>
                                    <td class="text-vertical-align"><?php echo count($courses) ?></td>
                                    <td class="text-vertical-align"><?php echo count($bookings) ?></td>
                                    <td class="text-vertical-align"><?php echo date($date_format . ' H:i:s', strtotime($subject->getField('created_date'))) ?></td>
                                    <td class="text-vertical-align">
                                        <a href="<?php echo spark_get_view_url('view', $subject->getField('ID')) ?>" class="btn btn-outline-primary"><i class="fa fa-eye"></i></a></td>


                                </tr>


                                <?php
}?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
