<div class="gutut twbs gu-tut-main-content">
    <div class="container">
        <?php
$view = spark_get_current_view();
if ($view == 'edit') {
	require_once 'spark-tutor-admin-subjects-edit.php';
} elseif ($view == 'view') {
	require_once 'spark-tutor-admin-subjects-view.php';
} else {
	require_once 'spark-tutor-admin-subjects-listing.php';
}

?>
    </div>
</div>
