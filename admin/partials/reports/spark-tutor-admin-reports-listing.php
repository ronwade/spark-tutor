<?php
$reports = ReviewServices::get_reviews_by_type(SPARK_TUTOR_TYPE_REPORT);
$date_format = spark_date_format();
?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Reports', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-striped gu-datatable">
                                        <thead>
                                            <tr>
                                                <th><?php echo esc_html__('Report', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Unique Booking ID', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Tutor', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Student', 'spark-tutor') ?></th>
                                                <th><?php echo esc_html__('Booking Date', 'spark-tutor') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($reports as $report) {?>
                                            <tr>
                                                <td><a href="#" data-toggle="modal" data-target="#reportModal" class="btn-show-report" data-post-id="<?php echo esc_attr($report->getField('ID')) ?>" data-tutor-name="<?php echo esc_attr($report->getField('tutor_name')) ?>" data-student-name="<?php echo esc_attr($report->getField('student_name')) ?>" data-created-date="<?php echo esc_attr($report->getField('created_date')) ?>" data-rating="<?php echo esc_attr($report->getField('rating')) ?>" data-comment="<?php echo esc_attr($report->getField('comment')) ?>"><?php echo esc_html(spark_substrwords($report->getField('comment'), 20)) ?></a></td>
                                                <td><?php echo esc_html($report->getField('booking_unique_id')) ?></td>
                                                <td><?php echo esc_html($report->getField('course_name')) ?></td>
                                                <td><?php echo esc_html($report->getField('tutor_name')) ?></td>
                                                <td><?php echo esc_html($report->getField('student_name')) ?></td>
                                                <td><?php echo esc_html(date($date_format . ' H:i:s', strtotime($report->getField('created_date')))) ?></td>
                                            </tr>
                                        <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo HtmlHelper::render_report_modal() ?>
