<?php
$subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, null);
$isEdit = false;
if (isset($_GET['id'])) {
	$tutor = spark_get_entity_by_id($_GET['id']);
	$courses = CourseServices::get_courses_by_tutor_id($tutor->getField('ID'));
	$reviews = ReviewServices::get_review_by_tutor_id_and_type($tutor->getField('ID'), SPARK_TUTOR_TYPE_REVIEW);
	$reports = ReviewServices::get_review_by_tutor_id_and_type($tutor->getField('ID'), SPARK_TUTOR_TYPE_REPORT);
	$now_date = strtotime('now');
	$isEdit = true;
} else {
	$tutor = new CPTutor();
	$courses = array();
}

foreach ($subjects as $value) {
	$subjectArray[$value->getPost()->ID] = $value->getField('name');
}
$lessionDurationArray = spark_get_duration_list();
$statusArray = [
	SPARK_TUTOR_STATUS_ACTIVE => SPARK_TUTOR_STATUS_ACTIVE,
	SPARK_TUTOR_STATUS_INACTIVE => SPARK_TUTOR_STATUS_INACTIVE,
];

require_once 'components/spark-tutor-admin-tutors-button-menu.php';
?>
<form class="edit-form" id="form-tutor" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
    <input type="hidden" name="action" value="save_form_info">
    <input type="hidden" name="entity_type" value="<?php echo SPARK_TUTOR_TUTOR_POST_TYPE; ?>">
    <input type="hidden" name="id" value="<?php echo esc_attr($tutor->getField('ID')) ?>">
    <input type="hidden" name="page" value="<?php echo spark_get_current_page() ?>">
    <input type="hidden" name="view" value="<?php echo spark_get_current_view() ?>">
    <input type="hidden" name="redirect_view" value="course-edit">
    <input type="hidden" name="modal" value="<?php echo ($isEdit == false) ? esc_html__('1', 'spark-tutor') : '' ?>">
    <div class="row tutor-view">
        <div class="col-md-12">
            <div class="card">
                <div class="content-view mt-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="name">
                                <h3 class="mb-4"><?php echo ($isEdit == true) ? esc_html__('Edit Tutor Details', 'spark-tutor') : esc_html__('Create New Tutor', 'spark-tutor') ?></h3>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-4" style="display: none">
                            <h5 class="mt-4"><?php echo esc_html__('Subject', 'spark-tutor') ?></h5>
                            <?php echo HtmlHelper::render_select($subjectArray, ['class' => 'form-control mw-100 subject-render ', 'id' => 'subject']) ?>

                        </div>
                        <div class="col-sm-12">
                            <?php require_once 'components/spark-tutor-admin-course-edit.php'; ?>
                        </div>

                    </div>
                    <br/>
                    <button class="btn btn-success btn-form-submit" name="save_and_view">
                        <i class="fa fa-eye"></i>
                        <?php echo esc_html__('Save & View', 'spark-tutor') ?>
                    </button>
                    <a href="<?php echo spark_get_view_url('view', $tutor->getField('ID')) ?>" class="pull-right btn btn-dark">
                        <?php echo esc_html__('Cancel', 'spark-tutor') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
    // Require modal.
    require_once 'components/spark-tutor-modal-add-course.php';
?>

<?php echo HtmlHelper::render_review_modal() ?>
    <?php echo HtmlHelper::render_report_modal() ?>
