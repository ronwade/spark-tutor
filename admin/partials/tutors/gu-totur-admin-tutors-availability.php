<?php require_once 'components/spark-tutor-admin-tutors-button-menu.php';
$id = isset($_GET['id']) ? sanitize_text_field($_GET['id']) : '';
?>
<div class="row tutor-view">
    <div class="col-md-12">
    	<input id="tutor_id" type="hidden" value="<?php echo esc_attr($id); ?>">
        <div id="notification" class="alert alert-primary card" role="alert">
            This event has been duplicated !!!
        </div>

        <div class="card">
            <div id="calendar"></div>
        </div>
    </div>
</div>
