<?php require_once 'components/spark-tutor-admin-tutors-button-menu.php';?>
<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Availability', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row mb-2">
                            <div class="col-sm-10">
                                <h5><?php echo esc_html__('Off Time', 'spark-tutor') ?></h5>
                            </div>
                            <div class="col-sm-2 ">
                                <button onclick="add_time_off()" class="btn btn-outline-primary pull-right"><?php echo esc_html__('Add off time', 'spark-tutor') ?></button>
                            </div>
                        </div>
                        <div class="list-date-off">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="reviewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reviewModalLabel"><?php echo esc_html__('Review', 'spark-tutor') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        Tutor: <a href="#"> <?php echo esc_html__('Tutor Name', 'spark-tutor') ?></a>
                    </div>
                    <div class="col-sm-4">
                        Student: <a href="#"> <?php echo esc_html__('Student Name', 'spark-tutor') ?></a>
                    </div>
                    <div class="col-sm-4">
                        Date Written: 30/01/2019
                    </div>
                </div>
                <div class="row mt-4 review-rating">
                    <div class="col-sm-12 ">
                        <h5><?php echo esc_html__('Review Rating', 'spark-tutor') ?></h5>
                    </div>
                    <div class="col-sm-12 star-rating-modal">
                        <span class="star-color"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                    </div>
                </div>
                <div class="row mt-4">
                    <h5><?php echo esc_html__('Review Details', 'spark-tutor') ?></h5>
                    <div>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo esc_html__('Delete', 'spark-tutor') ?></button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Close', 'spark-tutor') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reportModalLabel"><?php echo esc_html__('Report', 'spark-tutor') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        Tutor: <a href="#"> <?php echo esc_html__('Tutor Name', 'spark-tutor') ?></a>
                    </div>
                    <div class="col-sm-4">
                        Student: <a href="#"> <?php echo esc_html__('Student Name', 'spark-tutor') ?></a>
                    </div>
                    <div class="col-sm-4">
                        Date Written: 30/01/2019
                    </div>
                </div>
                <div class="row mt-4 review-rating">
                    <div class="col-sm-12 ">
                        <h5><?php echo esc_html__('Report Rating', 'spark-tutor') ?></h5>
                    </div>
                    <div class="col-sm-12 star-rating-modal">
                        <span class="star-color"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                    </div>
                </div>
                <div class="row mt-4">
                    <h5><?php echo esc_html__('Report Details', 'spark-tutor') ?></h5>
                    <div>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo esc_html__('Unpublish', 'spark-tutor') ?></button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Close', 'spark-tutor') ?></button>
            </div>
        </div>
    </div>
</div>
