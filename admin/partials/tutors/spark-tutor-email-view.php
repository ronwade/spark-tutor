<?php

// https://app.spark-education.com/wp-admin/admin.php?page=spark-tutor-tutors&user_id=13&view=email-view&template=tutor-registration-template.php

if( current_user_can('administrator') && isset($_GET['user_id']) &&  isset($_GET['template']) ) {

    $user = new \WP_User($_GET['user_id']);
    $template_file = $_GET['template'];
    $email = $user->get("user_email");
    $template = build_tutor_email_template($user, $template_file);
    echo $template->render();
} else {
    echo "Either the user id or the template file wasn't provided";
}

?>