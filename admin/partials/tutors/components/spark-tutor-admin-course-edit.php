<?php
/**
 * File variables
 * - $isEdit
 * - $courses
 * - $subjectArray
 * - $lessionDurationArray
 */

?>
<div class="mt-4 mb-4">
    <div class="col-sm-12">
        <div class="row mb-2">
            <div class="col-sm-10">
                <h5><?php echo esc_html__('Course', 'spark-tutor') ?></h5>
            </div>
            <div class="col-sm-2 ">
                <input type="hidden" id="showModal" value="<?php echo (isset($_GET['modal']) && sanitize_text_field($_GET['modal']) == '1') ? 'true' : 'false' ?>">
                <?php if ($isEdit) {?>
                    <a data-toggle="modal" data-target="#modalCourse" class="btn btn-outline-primary pull-right" onclick="get_course_info()"><?php echo esc_html__('Add course', 'spark-tutor') ?></a>
                <?php } else {?>
                    <button class="btn btn-outline-primary pull-right btn-form-submit"><?php echo esc_html__('Add course', 'spark-tutor') ?></button>
                    <?php }?>
            </div>
        </div>
    </div>
    <div>
        <div class="col-sm-12 table-responsive">
            <table class="table table-striped">
                <tr>
                    <th><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                    <th><?php echo esc_html__('Subject', 'spark-tutor') ?></th>
                    <th><?php echo esc_html__('Duration', 'spark-tutor') ?></th>
                    <th><?php echo esc_html__('Price', 'spark-tutor') ?></th>
                    <th><i class="fa fa-cog"></i></th>
                </tr>
                <?php foreach ($courses as $course) {?>
                <tr>
                    <td><input type="hidden" name="course_id[]" value="<?php echo esc_attr($course->getField('ID')) ?>" required>
                    <input type="text" class="form-control" value="<?php echo esc_attr($course->getField('name')) ?>" name="course_name[]"></td>
                    <td>
                        <?php echo HtmlHelper::render_select($subjectArray, ['class' => 'custom-select', 'name' => 'course_subject[]'], $course->getField('subject')) ?>
                    </td>
                    <td>

                        <?php echo HtmlHelper::render_select($lessionDurationArray, ['class' => 'custom-select', 'name' => 'course_duration[]'], $course->getField('duration')) ?>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="input-group input-group-seamless">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-usd"></i>
                                    </span>
                                </span>
                                <input type="text" class="form-control" <?php echo (spark_disable_custom_price()) ? 'disabled' : ''; ?> 
                                    name="course_price[]" value="<?php echo esc_attr($course->getField('price')) ?>" required>
                            </div>
                        </div>
                    </td>
                    <td>
                    <a href="#" class="btn btn-outline-primary button-edit" data-toggle="modal" data-target="#modalCourse" onclick="get_course_info(<?php echo esc_attr($course->getField('ID')) ?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <a class="btn btn-outline-danger button-trash"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php }?>
            </table>

        </div>
    </div>
</div>