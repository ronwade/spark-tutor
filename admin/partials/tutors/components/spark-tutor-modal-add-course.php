<?php
/**
 * File Variables:
 * - $subjectArray - Subject for the course.
 * - $lessionDurationArray - Course duration.
 */

 if ( empty($subjectArray) ) {
    $subjectArray = [];
    $subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, null);
    foreach ($subjects as $value) {
        $subjectArray[$value->getPost()->ID] = $value->getField('name');
    }
 }

 if ( ! isset($lessionDurationArray) ) {
    $lessionDurationArray = spark_get_duration_list();
 }

?>
<div class="modal fade" id="modalCourse" tabindex="-1" role="dialog" aria-labelledby="modalCourselLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" action="<?php echo admin_url('admin-post.php'); ?>" enctype="multipart/form-data" data-parsley-validate>
                <input type="hidden" name="action" value="form_save_course">
                <input type="hidden" name="entity_type" value="<?php echo SPARK_TUTOR_COURSE_POST_TYPE; ?>">
                <input type="hidden" name="id" id="course_id">
                <input type="hidden" name="tutor" value="<?php echo esc_attr($_GET['id']) ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCouseTitle"><?php echo esc_html__('Edit Course', 'spark-tutor') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name" class="col-form-label"><?php echo esc_html__('Course Name', 'spark-tutor') ?></label>
                            <input type="text" class="form-control" id="course_name" name="name" value="" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="name" class="col-form-label"><?php echo esc_html__('Subject', 'spark-tutor') ?></label>
                            <select class="custom-select form-control" id="course_subject" name="subject">
                                <option value="">---Select---</option>
                                <?php foreach ($subjectArray as $key => $value): ?>
                                    <option value="<?php echo esc_attr($key) ?>"><?php echo $value; ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="name" class="col-form-label"><?php echo esc_html__('Duration', 'spark-tutor') ?></label>
                            <select class="custom-select form-control" id="course_duration" name="duration">
                                <option value="">---Select---</option>
                                <?php foreach ($lessionDurationArray as $key => $value): ?>
                                    <option value="<?php echo esc_attr($key) ?>"><?php echo $value; ?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name" class="col-form-label"><?php echo esc_html__('Price', 'spark-tutor') ?></label>
                             <div class="input-group input-group-seamless">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-usd"></i>
                                    </span>
                                </span>
                                <input type="text" <?php echo (spark_disable_custom_price()) ? 'disabled' : ''; ?>
                                     placeholder="<?php echo spark_get_default_price(); ?>"
                                     value="<?php echo spark_get_default_price(); ?>" class="form-control" id="course_price" name="price"  >
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="form1-name" class="col-form-label"><?php echo esc_html__('Thumbnail', 'spark-tutor') ?></label>
                            <fieldset>
                                <div class="custom-file w-100">
                                    <input type="file" class="custom-file-input" id="customFileCourse" name="picture"  accept="image/*">
                                    <label class="custom-file-label" for="picture"><?php echo esc_html__('Choose file', 'spark-tutor') ?>...</label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-form-label"><?php echo esc_html__('Description', 'spark-tutor') ?></label>
                            <div class="form-group">
                                <textarea class="form-control" id="course_description" rows="2" name="description"></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-form-label"><?php echo esc_html__('Course Details', 'spark-tutor') ?></label>
                            <div class="form-group">
                                <textarea class="form-control" id="course_detail" rows="2" name="detail"></textarea>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo esc_html__('Save', 'spark-tutor') ?></button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Close', 'spark-tutor') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>