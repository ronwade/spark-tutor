
<div class="row">
    <div class="col-sm-12">
        <div class="name">
            <h3 class="mb-4"><?php echo esc_html($tutor->getField('name')) ?></h3>
        </div>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="row information">
            <div class="col-sm-4">
                <div class="avt tutor-photo bkg-img" style="background-image: url('<?php echo $tutor_photo; ?>'), url('<?php echo SPARK_TUTOR_ICON_URL;?>');"></div>
            </div>
            <div class="col-sm-4">
                <p><?php echo esc_html__('Email', 'spark-tutor') ?>: 
                    <a href="mailto:<?php echo $tutor_email; ?>">
                        <?php echo $tutor_email; ?>
                    </a>
                </p>
                <p><?php echo esc_html__('TRN / SSN', 'spark-tutor') ?>: <?php echo esc_html($tutor->getField('trn_ssn')) ?></p>
                <p><?php echo esc_html__('Tutor ID', 'spark-tutor') ?>: <?php echo esc_html($tutor->getId()) ?></p>

                <p><?php echo esc_html__('Earning', 'spark-tutor') ?>: $<?php echo esc_html($total_earned); ?></p>
            </div>
            <div class="col-sm-4">
                <p><?php echo esc_html__('Phone', 'spark-tutor') ?>:
                    <a href="tel:<?php echo $tutor_phone; ?>">
                    <?php echo $tutor_phone; ?>
                    </a>
                </p>
                <p>
                    <?php echo esc_html__('Status', 'spark-tutor') ?>:
                    <?php echo HtmlHelper::render_badge_label( $tutor->getField('status').( ( ! $tutor->has_account() ) ? ': No User Account' : '' ) ) ?>
                </p>
                <p>
                <?php echo esc_html__('Average Review Rating', 'spark-tutor') ?>: <span class="star-color"><i class="fa fa-star"></i></span> <?php if ($total_review > 0) {
                    echo number_format($total_review, 2);
                } else {
                    echo '0.00 (No reviews)';
                }
                ?>
                </p>
            </div>
        </div>
    </div>
</div>