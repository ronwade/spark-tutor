<?php
$view = spark_get_current_view();
$id = isset($_GET['id']) ? sanitize_text_field($_GET['id']) : null;

?>
<div class="tutor-view">
    <div class="card">
        <div class="row">
            <div class="col-md-8">
                <div class="action mb-2 d-inline btn-group mt-2" role="group">
                    <?php if ($view != 'edit') {?>
                        <a href="<?php echo spark_get_view_url('list') ?>" class="btn btn btn-outline-primary"><?php echo esc_html__('Back', 'spark-tutor') ?></a>
                        <a href="<?php echo spark_get_view_url('edit', $id) ?>" class="btn btn btn-outline-primary"><i class="fa fa-edit"></i> <?php echo esc_html__('Edit', 'spark-tutor') ?></a>
                    <?php } else {?>
                        <button class="btn btn-outline-success btn-form-submit" name="save_and_view"><i class="fa fa-eye"></i> <?php echo esc_html__('Save & View', 'spark-tutor') ?></button>
                    <?php }?>
                    <button class="btn dropdown-toggle btn-outline-primary" type="button" id="set-status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo esc_html__('Availability', 'spark-tutor') ?>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="set-status">
                        <a class="dropdown-item" href="<?php echo spark_get_view_url('working-time', $id) ?>"><?php echo esc_html__('Working Time', 'spark-tutor') ?></a>
                    </div>
                    <?php 

                    if (current_user_can( spark_manager_role() ) ) {
                        if ($view != 'edit') {
                            // Include `Set Status Dropdown` menu
                            require_once gu_path_join(SPARK_TUTOR_PLUGIN_ADMIN_DIR, 'partials', 'tutors', 'components', 'spark-tutor-admin-tutors-status-dropdown.php');
                        }
                    ?>
                    <!-- <button class="btn btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i> <?php // echo esc_html__('Delete', 'spark-tutor') ?></button> -->
               
                    <?php } ?>
                </div>

            </div>
            <div class="col-md-4" style="text-align: right">
                <?php if ($view == 'edit') { ?>
                    <a href="<?php echo spark_get_view_url('view', $id) ?>" class="btn btn-dark">
                        <?php echo esc_html__('Cancel', 'spark-tutor') ?>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php echo HtmlHelper::render_delete_modal($id) ?>
