<?php

// $post_portal_action = ( empty(get_user_with_email()->ID) ) ? 'create-user' : 'link-user';
$post_portal_action = 'link-user';
$tutor_exp_subjects = explode(',', $tutor->getField('subjects'));
$tutor_exp_grades = explode(',', $tutor->getField('grades'));

$tutor_grade_badges = "";
$tutor_subject_badges = "";

foreach($tutor_exp_grades as $badge) {
    $tutor_grade_badges .= "<span class='badge badge-primary' style='margin-right: 5px; display: inline-block'>$badge</span>";
}
foreach($tutor_exp_subjects as $badge) {
    $tutor_subject_badges .= "<span class='badge badge-primary' style='margin-right: 5px; display: inline-block'>$badge</span>";
}

?>

<?php if ($tutor->has_account() ) { ?>
    <table class="table">
        <tbody>
            <tr>
                <td>username</td>
                <td><?php echo $tutor_user->get('user_login'); ?></td>
            </tr>
            <tr>
                <td>display_name</td>
                <td><?php echo $tutor_user->get('user_login'); ?></td>
            </tr>
            <tr>
                <td>Subjects Taught</td>
                <td><?php echo $tutor_subject_badges ?></td>
            </tr>
            <tr>
                <td>Grades Level</td>
                <td><?php echo $tutor_grade_badges ?></td>
            </tr>
        </tbody>
    </table>
<?php } else { ?>
    <div class="alert alert-warning" role="alert">
        <h4 class="alert-heading">Client Portal is Disable!</h4>
        <p>There's no login account for <?php echo $tutor->getName(); ?>.</p>
        <hr>
        <p class="mb-0">
            Please contact us at <span >Email: <?php echo spark_get_contact_email(true, "style='color: white;'"); ?></span>
            to enable the <strong>Tutor Portal</strong> for <?php echo $tutor->getName(); ?>.
        </p>
        <?php if (current_user_can( spark_manager_role() ) ) { ?>
            <br>
            <form action="<?php echo admin_url() . 'admin.php?page=spark-tutor-tutors&view=enable-portal&id='.$tutor->getField('ID'); ?>"
                    method="post">
                <input type="hidden" name="_gu-token" value="<?php echo spark_access_token(); ?>" >
                <button type="submit" class="btn btn-dark" name="portal-action" value="<?php echo $post_portal_action; ?>" >
                    Enable Tutor Portal
                </button>
            </form>
        <?php } ?>
    </div>
<?php } ?>