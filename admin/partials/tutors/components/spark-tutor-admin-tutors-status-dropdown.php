
<button class="btn dropdown-toggle btn-outline-primary" type="button" id="set-status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<?php echo esc_html__('Set Status', 'spark-tutor') ?>
</button>
<div class="dropdown-menu" aria-labelledby="set-status">
    <button class="dropdown-item btn-update-status" data-status="<?php echo SPARK_TUTOR_STATUS_ACTIVE; ?>"><?php echo esc_html__('Active', 'spark-tutor') ?></button>
    <button class="dropdown-item btn-update-status" data-status="<?php echo SPARK_TUTOR_STATUS_REVIEWED; ?>"><?php echo esc_html__('Reviewed', 'spark-tutor') ?></button>
    <button class="dropdown-item btn-update-status" data-status="<?php echo SPARK_TUTOR_STATUS_REJECTED; ?>"><?php echo esc_html__('Rejected', 'spark-tutor') ?></button>
    <button class="dropdown-item btn-update-status" data-status="<?php echo SPARK_TUTOR_STATUS_REVIEWING; ?>"><?php echo esc_html__('Reviewing', 'spark-tutor') ?></button>
    <button class="dropdown-item btn-update-status" data-status="<?php echo SPARK_TUTOR_STATUS_PENDING; ?>"><?php echo esc_html__('Pending', 'spark-tutor') ?></button>
    <button class="dropdown-item btn-update-status" data-status="<?php echo SPARK_TUTOR_STATUS_INACTIVE; ?>"><?php echo esc_html__('Inactive', 'spark-tutor') ?></button>
</div>