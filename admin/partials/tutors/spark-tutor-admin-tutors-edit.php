<?php
$subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, null);
$isEdit = false;
if (isset($_GET['id'])) {
	$tutor = spark_get_entity_by_id($_GET['id']);
	$courses = CourseServices::get_courses_by_tutor_id($tutor->getField('ID'));
	$reviews = ReviewServices::get_review_by_tutor_id_and_type($tutor->getField('ID'), SPARK_TUTOR_TYPE_REVIEW);
	$reports = ReviewServices::get_review_by_tutor_id_and_type($tutor->getField('ID'), SPARK_TUTOR_TYPE_REPORT);
	$now_date = strtotime('now');
	$isEdit = true;
} else {
	$tutor = new CPTutor();
	$courses = array();
}


$exp_grades = json_encode(explode(",", $tutor->getField('grades', '')));
$exp_subjects = json_encode(explode(",", $tutor->getField('subjects', '')));

$subject_options = "";
$grade_options = "";

foreach ($subjects as $value) {
    $subject_title = $value->getField('name');
	$subjectArray[$value->getPost()->ID] = $subject_title;
    $subject_options .= "<option value='$subject_title'>$subject_title</option>";
}

for($g = 1; $g <= 12; $g++) {
    $grade_options .= "<option value='$g'>Grade $g</option>";
}

$lessionDurationArray = spark_get_duration_list();
$statusArray = [
	SPARK_TUTOR_STATUS_ACTIVE => SPARK_TUTOR_STATUS_ACTIVE,
	SPARK_TUTOR_STATUS_INACTIVE => SPARK_TUTOR_STATUS_INACTIVE,
];

require_once 'components/spark-tutor-admin-tutors-button-menu.php';

?>
<form class="edit-form" id="form-tutor" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
    <input type="hidden" name="action" value="save_form_info">
    <input type="hidden" name="entity_type" value="<?php echo SPARK_TUTOR_TUTOR_POST_TYPE; ?>">
    <input type="hidden" name="id" value="<?php echo esc_attr($tutor->getField('ID')) ?>">
    <input type="hidden" name="page" value="<?php echo spark_get_current_page() ?>">
    <input type="hidden" name="view" value="<?php echo spark_get_current_view() ?>">
    <input type="hidden" name="modal" value="<?php echo ($isEdit == false) ? esc_html__('1', 'spark-tutor') : '' ?>">
    <div class="row tutor-view">
        <div class="col-md-12">
            <div class="card">
                <div class="content-view mt-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="name">
                                <h3 class="mb-4"><?php echo ($isEdit == true) ? esc_html__('Edit Tutor Details', 'spark-tutor') : esc_html__('Create New Tutor', 'spark-tutor') ?></h3>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <h5><?php echo esc_html__('Basic Information', 'spark-tutor') ?></h5>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="name" class="col-form-label"><?php echo esc_html__('Name', 'spark-tutor') ?></label>
                                    <input type="text" class="form-control" id="form1-name" placeholder="<?php echo esc_html__('Name', 'spark-tutor') ?>" name="name" value="<?php echo esc_attr($tutor->getField('name')) ?>" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="form1-name" class="col-form-label"><?php echo esc_html__('Picture', 'spark-tutor') ?></label>
                                    <fieldset>
                                        <div class="custom-file w-100">
                                            <input type="file" class="custom-file-input" id="customFile" name="picture" accept="image/*" <?php echo ($tutor->getField('picture') == null || $tutor->getField('picture') == '') ? 'required' : '' ?> >
                                            <label class="custom-file-label" for="picture"><?php echo esc_html__('Choose file', 'spark-tutor') ?>...</label>
                                        </div>
                                    </fieldset>
                                </div>
                                <p class="form-group col-md-6">
                                    <label for="form-grade">Select the grades you have experience with.</label>
                                    <select style="width: 100%;" require="" class="select2 form-control" id="form-grades" multiple name="grades[]" >
                                        <?php echo $grade_options; ?>
                                    </select>
                                </p>
                                <p class="form-group col-md-6">
                                    <label for="form-grade">Select the subjects you have experience teaching.</label>
                                    <select style="width: 100%;" require="" class="select2 form-control" id="form-subjects" multiple name="subjects[]" >
                                        <?php echo $subject_options; ?>
                                    </select>
                                </p>
                                <div class="form-group col-md-6">
                                    <label for="resume" class="col-form-label"><?php echo esc_html__('Resume', 'spark-tutor') ?></label>
                                    <fieldset>
                                        <div class="custom-file w-100">
                                            <input type="file" class="custom-file-input" id="customFile" name="resume" accept="application/pdf, application/msword" <?php echo ($tutor->getField('resume') == null || $tutor->getField('resume') == '') ? 'required' : '' ?> >
                                            <label class="custom-file-label" for="resume"><?php echo esc_html__('Choose file', 'spark-tutor') ?>...</label>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <label for="email" class="col-form-label"><?php echo esc_html__('Email', 'spark-tutor') ?></label>
                                    <input type="email" class="form-control" id="form1-email" placeholder="<?php echo esc_html__('Email', 'spark-tutor') ?>" value="<?php echo esc_attr($tutor->getField('email')) ?>" name="email" required>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <label for="phone" class="col-form-label"><?php echo esc_html__('Phone', 'spark-tutor') ?></label>
                                    <input type="text" class="form-control" id="form1-name" onKeyPress="return isNumberKey(event)" placeholder="<?php echo esc_html__('Phone', 'spark-tutor') ?>" value="<?php echo esc_attr($tutor->getField('phone')) ?>" name="phone" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <label for="trn_ssn" class="col-form-label"><?php echo esc_html__('TRN / SSN', 'spark-tutor') ?></label>
                                    <input type="text" class="form-control" placeholder="<?php echo esc_html__('TRN / SSN', 'spark-tutor') ?>" value="<?php echo esc_attr($tutor->getField('trn_ssn')) ?>" name="trn_ssn" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="mt-4"><?php echo esc_html__('Intro', 'spark-tutor') ?></h5>
                            <div class="form-group">
                                <textarea class="form-control" id="intro" rows="3" name="intro" required><?php echo esc_textarea($tutor->getField('intro')) ?></textarea>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <?php 
                                generate_tutor_intro_video_source_selector_html( 
                                    esc_attr($tutor->getField('intro_video_source')),
                                    esc_attr($tutor->getField('intro_video_url')) 
                                ); 
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-4" style="display: none">
                            <h5 class="mt-4"><?php echo esc_html__('Subject', 'spark-tutor') ?></h5>
                            <?php echo HtmlHelper::render_select($subjectArray, ['class' => 'form-control mw-100 subject-render ', 'id' => 'subject']) ?>

                        </div>
                        <div class="col-sm-12">
                            <?php require_once 'components/spark-tutor-admin-course-edit.php'; ?>
                        </div>

                    </div>
                    <?php if ($isEdit) {?>
                    <div class="row ">
                        <div class="col-sm-12 mb-2">
                            <h5><?php echo esc_html__('Tutor Review', 'spark-tutor') ?></h5>
                            <div class="col-sm-12">
                                <?php if (count($reviews) != 0) {?>
                                <table class="table">
                                    <?php foreach ($reviews as $review) {?>
                                    <tr class="text-vertical-align">
                                        <td><a href="#" data-toggle="modal" data-target="#reviewModal" class="btn-show-review" data-post-id="<?php echo esc_attr($review->getField('ID')) ?>" data-tutor-name="<?php echo esc_attr($review->getField('tutor_name')) ?>" data-student-name="<?php echo esc_attr($review->getField('student_name')) ?>" data-created-date="<?php echo esc_attr($review->getField('created_date')) ?>" data-rating="<?php echo esc_attr($review->getField('rating')) ?>" data-comment="<?php echo esc_attr($review->getField('comment')) ?>"><?php echo esc_html__('Reviewed by', 'spark-tutor') ?> <?php echo esc_attr($review->getField('student_name')) ?></a> - <?php echo HtmlHelper::render_rating_stars($review->getField('rating')) ?>
                                        <span><?php echo calculate_time_distance(strtotime($review->getField('created_date')), $now_date) ?></span>
                                    </td>
                                        <td><button class="btn btn-outline-danger btn-delete" data-toggle="modal" data-target="#deleteModal" post-id="<?php echo esc_attr($review->getField('ID')) ?>"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                <?php }?>
                                </table>
                                 <?php } else {echo esc_html__('"No reviews yet"', 'spark-tutor');}?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-2">
                            <h5><?php echo esc_html__('Student Report', 'spark-tutor') ?></h5>
                            <?php if (count($reports) != 0) {?>
                            <table class="table">
                                <?php foreach ($reports as $report) {?>
                                <tr class="text-vertical-align">
                                    <td><a href="#" data-toggle="modal" data-target="#reportModal" class="btn-show-report" data-post-id="<?php echo esc_attr($report->getField('ID')) ?>" data-tutor-name="<?php echo esc_attr($report->getField('tutor_name')) ?>" data-student-name="<?php echo esc_attr($report->getField('student_name')) ?>" data-created-date="<?php echo esc_attr($report->getField('created_date')) ?>" data-rating="<?php echo esc_attr($report->getField('rating')) ?>" data-comment="<?php echo esc_attr($report->getField('comment')) ?>"><?php echo esc_html__('Rerport to', 'spark-tutor') ?> <?php echo esc_attr($report->getField('student_name')) ?></a> - <?php echo HtmlHelper::render_rating_stars($report->getField('rating')) ?>
                                        <span><?php echo calculate_time_distance(strtotime($report->getField('created_date')), $now_date) ?></span>
                                    </td>
                                    <td><button class="btn btn-outline-danger btn-delete" post-id="<?php echo esc_attr($report->getField('ID')) ?>" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i></button></td>
                                </tr>
                                <?php }?>
                            </table>
                        <?php } else {echo esc_html__('"No reviews yet"', 'spark-tutor');}?>
                        </div>
                    </div>
                <?php }?>
                    <br/>
                    <button class="btn btn-success btn-form-submit" name="save_and_view">
                        <i class="fa fa-eye"></i>
                        <?php echo esc_html__('Save & View', 'spark-tutor') ?>
                    </button>
                    <a href="<?php echo spark_get_view_url('view', $tutor->getField('ID')) ?>" class="pull-right btn btn-dark">
                        <?php echo esc_html__('Cancel', 'spark-tutor') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
    // Require modal.
    require_once 'components/spark-tutor-modal-add-course.php';

?>


<script>
window.addEventListener('load', function () {
    jQuery("#form-grades").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    }).val(<?php echo $exp_grades; ?>)
        .trigger('change');
    jQuery("#form-subjects").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    }).val(<?php echo $exp_subjects; ?>)
        .trigger('change');
})
</script>
<?php echo HtmlHelper::render_review_modal() ?>
    <?php echo HtmlHelper::render_report_modal() ?>
