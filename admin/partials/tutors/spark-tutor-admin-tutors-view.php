<?php
require_once 'components/spark-tutor-admin-tutors-button-menu.php';
$tutor = spark_get_entity_by_id($_GET['id']);
$courses = CourseServices::get_courses_by_tutor_id($tutor->getField('ID'));
$total_earned = BookingServices::get_total_by_tutor_id($tutor->getField('ID'));
$total_earned = number_format($total_earned, 2);
$reviews = ReviewServices::get_review_by_tutor_id_and_type($tutor->getField('ID'), SPARK_TUTOR_TYPE_REVIEW);
$reports = ReviewServices::get_review_by_tutor_id_and_type($tutor->getField('ID'), SPARK_TUTOR_TYPE_REPORT);
$now_date = strtotime('now');

$total_review = 0;
foreach ($reviews as $review) {
	$total_review += $review->getField('rating');
}
if ($total_review > 0) {
	$total_review /= count($reviews);
} else {
	$total_review = 0;
}

$tutor_email = esc_html($tutor->getField('email'));
$tutor_phone = esc_html($tutor->getField('phone'));
$tutor_photo = esc_attr($tutor->getField('picture'));
$tutor_missing_fields = $tutor->getMissingFields();
$tutor_user = $tutor->user();

$tutor_missing_html = "";
foreach($tutor_missing_fields as $field) {
    $fieldTrans = ucfirst($field);
    $tutor_missing_html .= "<li>- $fieldTrans</li>";
}

// $this->send_tutor_template_email_action(
//     $tutor->getField('ID'),
//     "tutor-welcome-template.php"
// )
// clear_data ();

?>

<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <?php require_once 'components/spark-tutor-profile-top.php'; ?>
                <br/>
                <br/>
                <div class="row ">
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="gu-subjects-tab" data-toggle="tab" 
                                        href="#tab-gu-subjects" role="tab" aria-controls="tab-subjects" aria-selected="true">
                                        Subjects
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="gu-profile-tab" data-toggle="tab" 
                                        href="#tab-gu-profile" role="tab" aria-controls="tab-profile" aria-selected="false">
                                        Profile
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="gu-students-tab" data-toggle="tab" 
                                        href="#tab-gu-students" role="tab" aria-controls="tab-students" aria-selected="false">
                                        Students
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="gu-reviews-tab" data-toggle="tab" 
                                        href="#tab-gu-reviews" role="tab" aria-controls="tab-reviews" aria-selected="false">
                                        Review
                                    </a>
                                </li>
                            </ul>
                            
                            <div class="tab-content" id="spark-tutor-content">
                                <div class="tab-pane fade show active gu-top-padding " id="tab-gu-subjects" role="tabpanel" aria-labelledby="subjects-tab">
                                    <div class="row mb-2">
                                        <div class="col-sm-12">
                                            <h5><?php echo esc_html__('Subject', 'spark-tutor') ?></h5>
                                            <?php $subjects = $tutor->getField('subject');
                                                if (empty($subjects)) {
                                                    $subjects = array();
                                                }
                                                foreach ($subjects as $subject_id) {
                                                    $subject = spark_get_entity_by_id($subject_id);?>

                                            <span class="badge mr-3 badge-primary"><?php echo esc_html($subject->getField('name')); ?></span>
                                        <?php }?>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-sm-12">
                                            <div class="row mt-4">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h5><?php echo esc_html__('Course', 'spark-tutor') ?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 table-responsive">
                                                        <table class="table table-striped">
                                                            <tr>
                                                                <th><?php echo esc_html__('Course Name', 'spark-tutor') ?></th>
                                                                <th><?php echo esc_html__('Subject', 'spark-tutor') ?></th>
                                                                <th><?php echo esc_html__('Duration', 'spark-tutor') ?></th>
                                                                <th><?php echo esc_html__('Price', 'spark-tutor') ?></th>
                                                            </tr>
                                                            <?php foreach ($courses as $course) {
                                                                $subject = spark_get_entity_by_id($course->getField('subject'));
                                                            ?>
                                                            <tr>
                                                                <td><?php echo esc_html($course->getField('name')) ?></td>
                                                                <td><?php echo esc_html($subject->getField('name')) ?></td>
                                                                <td><?php echo esc_html($course->getField('duration')) ?> <?php echo esc_html__('mins', 'spark-tutor') ?></td>
                                                                <td>$<?php echo esc_html($course->getField('price')) ?></td>
                                                            </tr>
                                                        <?php }?>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade gu-top-padding " id="tab-gu-profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <?php require_once 'components/spark-tutor-portal-notification.php'; ?>
                                            <h5 class="mt-4">
                                                <?php 
                                                    echo esc_html__('Introduction / Biography', 'spark-tutor');
                                                    $tutor_resume = esc_attr($tutor->getField('resume'));
                                                    
                                                    if ( ! empty($tutor_resume)) {
                                                ?>
                                                    <a target="_blank" class="btn btn-primary btn-sm pull-right" href="<?php echo $tutor_resume ?>" alt="resume">
                                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                        View Resume
                                                    </a>
                                                <?php
                                                    }
                                                ?>
                                            </h5>
                                            <p><?php echo esc_html($tutor->getField('intro')) ?> </p>
                                            <br/>
                                        </div>
                                        <div class="col-md-4">
                                            <?php
                                                if ($tutor->has_video()) {
                                                    echo "<div class='video-container tutor-video-container'>";
                                                    $tutor->render_video();
                                                    echo '</div><br>';
                                                }
                                            ?>
                                            <?php 
                                            if ( in_array($tutor->getField('status'), SPARK_TUTOR_STATUS_REVIEW_LIST) ) {
                                            ?>
                                                <div class="alert alert-info" role="alert">
                                                    <h4 class="alert-heading">
                                                        We're reviewing your profile!
                                                    </h4>
                                                    <br>
                                                    <p>
                                                        This usually takes 24 hours, but in some cases it can take up to 3 days.
                                                        We'll notify you when everything’s ready.
                                                    </p>
                                                </div>
                                            <?php } else if ( in_array($tutor->getField('status'), SPARK_TUTOR_STATUS_ACTIVE_LIST) ) { ?>
                                                <div class="alert alert-success" role="alert">
                                                    <h4 class="alert-heading">
                                                        Well Done!
                                                    </h4>
                                                    <br>
                                                    <p>Account has been verified:</p>
                                                </div>
                                            <?php } else if ( in_array($tutor->getField('status'), SPARK_TUTOR_STATUS_REJECT_LIST) ) { ?>
                                                <div class="alert alert-danger" role="alert">
                                                    <h4 class="alert-heading">
                                                        Your account is inactive!
                                                    </h4>
                                                    <br>
                                                    <p>
                                                        If you need any help using the website please contact us with the contact information provided below:
                                                        <br/>
                                                        <span >Email: <?php echo spark_get_contact_email(true, "style='color: white;'"); ?></span> 
                                                        <br/>
                                                        <span>Tel: <?php echo spark_get_contact_number(true, "style='color: white;'"); ?></span> 
                                                    </p>
                                                </div>
                                                
                                            <?php } if (! empty($tutor_missing_fields)) { ?>
                                                <div class="alert alert-danger" role="alert">
                                                    <h4 class="alert-heading">
                                                        Action Required!
                                                    </h4>
                                                    <p>The following information is requred for activating your account:</p>
                                                    <hr>
                                                    <ul>
                                                        <?php echo $tutor_missing_html; ?>
                                                    </ul>
                                                </div>

                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade  gu-top-padding " id="tab-gu-students" role="tabpanel" aria-labelledby="students-tab">
                                    <div class="row">
                                        <div class="col-sm-12 mb-2">
                                            <h5><?php echo esc_html__('Student Report', 'spark-tutor') ?></h5>
                                            <div class="col-sm-12">
                                                <?php if (count($reports) != 0) {?>

                                                <table class="table">
                                                    <?php foreach ($reports as $report) {?>
                                                    <tr>
                                                        <td><a href="#" data-toggle="modal" data-target="#reportModal" class="btn-show-report" data-post-id="<?php echo esc_attr($report->getField('ID')) ?>" data-tutor-name="<?php echo esc_attr($report->getField('tutor_name')) ?>" data-student-name="<?php echo esc_attr($report->getField('student_name')) ?>" data-created-date="<?php echo esc_attr($report->getField('created_date')) ?>" data-rating="<?php echo esc_attr($report->getField('rating')) ?>" data-comment="<?php echo esc_attr($report->getField('comment')) ?>"><?php echo esc_html__('Rerport to', 'spark-tutor') ?> <?php echo esc_html($report->getField('student_name')) ?></a> - <?php echo HtmlHelper::render_rating_stars($report->getField('rating')) ?>
                                                            <span><?php echo calculate_time_distance(strtotime($report->getField('created_date')), $now_date) ?></span>
                                                        </td>
                                                    </tr>
                                                    <?php }?>
                                                </table>
                                                <?php } else {echo esc_html__('"No reviews yet"', 'spark-tutor');}?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade  gu-top-padding " id="tab-gu-reviews" role="tabpanel" aria-labelledby="reviews-tab">    
                                    <div class="row">
                                        <div class="col-sm-12 mb-2">
                                            <h5><?php echo esc_html__('Tutor Review', 'spark-tutor') ?></h5>
                                            <div class="col-sm-12">
                                                <?php if (count($reviews) != 0) {?>
                                                <table class="table">
                                                    <?php foreach ($reviews as $review) {?>
                                                    <tr>
                                                        <td><a href="#" data-toggle="modal" data-target="#reviewModal" class="btn-show-review" data-post-id="<?php echo esc_attr($review->getField('ID')) ?>" data-tutor-name="<?php echo esc_attr($review->getField('tutor_name')) ?>" data-student-name="<?php echo esc_attr($review->getField('student_name')) ?>" data-created-date="<?php echo esc_attr($review->getField('created_date')) ?>" data-rating="<?php echo esc_attr($review->getField('rating')) ?>" data-comment="<?php echo esc_attr($review->getField('comment')) ?>" ><?php echo esc_html__('Reviewed by', 'spark-tutor') ?> <?php echo esc_html($review->getField('student_name')) ?></a> - <?php echo HtmlHelper::render_rating_stars($review->getField('rating')) ?>
                                                            <span><?php echo calculate_time_distance(strtotime($review->getField('created_date')), $now_date) ?></span>
                                                        </td>
                                                    </tr>
                                                <?php }?>
                                                </table>
                                            <?php } else {echo esc_html__('"No reviews yet"', 'spark-tutor');}?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo HtmlHelper::render_review_modal() ?>
<?php echo HtmlHelper::render_report_modal() ?>
