<?php
require_once 'components/spark-tutor-admin-tutors-button-menu.php';
$tutor = spark_get_entity_by_id($_GET['id']);
$courses = CourseServices::get_courses_by_tutor_id($tutor->getField('ID'));
$total_earned = BookingServices::get_total_by_tutor_id($tutor->getField('ID'));
$total_earned = number_format($total_earned, 2);
$reviews = ReviewServices::get_review_by_tutor_id_and_type($tutor->getField('ID'), SPARK_TUTOR_TYPE_REVIEW);
$reports = ReviewServices::get_review_by_tutor_id_and_type($tutor->getField('ID'), SPARK_TUTOR_TYPE_REPORT);
$now_date = strtotime('now');

$user_portal_id = false;
$portal_action = post_url_param('portal-action', false);

if (post_url_param('_gu-token', false) == spark_access_token()) {
    if ( $portal_action == 'create-user' ) {
        $user_portal_id = $tutor->register_instructor(
            $tutor->getId(),
            $tutor->getName(),
            $tutor->getField('email'),
            wp_generate_password()
        );
    }
}

$total_review = 0;
foreach ($reviews as $review) {
	$total_review += $review->getField('rating');
}
if ($total_review > 0) {
	$total_review /= count($reviews);
} else {
	$total_review = 0;
}

$tutor_name = esc_html($tutor->getName());
$tutor_email = esc_html($tutor->getField('email'));
$tutor_phone = esc_html($tutor->getField('phone'));
$tutor_photo = esc_attr($tutor->getField('picture'));
$tutor_missing_fields = $tutor->getMissingFields();
$tutor_user = $tutor->user();

$tutor_missing_html = "";
foreach($tutor_missing_fields as $field) {
    $fieldTrans = ucfirst($field);
    $tutor_missing_html .= "<li>- $fieldTrans</li>";
}

?>

<div class="row tutor-view">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <?php require_once 'components/spark-tutor-profile-top.php'; ?>
                        <br/>
                        <br/>
                        <?php if ($user_portal_id) {
                            if ($portal_action == 'create-user') {
                                echo "<div class='alert alert-success' role='alert'>Portal enabled for $tutor_name upon creation!</div>";
                            }
                        } else {
                            echo "<div class='alert alert-danger' role='alert'>Unable to enable the portal for $tutor_name!</div>";
                        } ?>
                        <br/>
                        <br/>
                        <?php require_once 'components/spark-tutor-portal-notification.php'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>