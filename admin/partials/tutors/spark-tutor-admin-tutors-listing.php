<?php
$status = isset($_GET['status']) ? sanitize_text_field($_GET['status']) : null;
$keyword = isset($_GET['keyword']) ? sanitize_text_field($_GET['keyword']) : '';
$tutors = TutorServices::search_tutors_by_status($keyword, $status);
$date_format = spark_date_format();

?>
<div class="tutor-view">
    <div class="card">
        <div class="action mb-2 d-inline btn-group mt-2" role="group">
            <a href="<?php echo spark_get_view_url('edit') ?>" class="btn btn btn-outline-primary"><i class="fa fa-plus"></i> <?php echo esc_html__('Create New Tutor', 'spark-tutor') ?></a>
        </div>
    </div>
</div>
<div class="row tutor-list">
    <div class="col-md-12">
        <div class="card">
            <div class="content-listing">
            <div class="row">
                    <div class="col-sm-12 mt-4">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Tutors', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="tab-status">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?php echo (!isset($status) || $status == '') ? 'active' : '' ?>" id="home-tab"  href="<?php echo spark_get_view_url('list') ?>" role="tab" aria-selected="<?php echo (!isset($statuss) || $status == '') ? 'true' : 'false' ?>"><?php echo esc_html__('All', 'spark-tutor') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($status == 'active') ? 'active' : '' ?>"  href="<?php echo spark_get_view_url('list') . '&status=active' ?>" role="tab" aria-selected="<?php echo ($status == 'active') ? 'true' : 'false' ?>"><?php echo esc_html__('Active', 'spark-tutor') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($status == 'inactive') ? 'active' : '' ?>" id="contact-tab"  href="<?php echo spark_get_view_url('list') . '&status=inactive' ?>" role="tab" aria-selected="<?php echo ($status == 'inactive') ? 'true' : 'false' ?>"><?php echo esc_html__('Inactive', 'spark-tutor') ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="list-details mt-2">
                    <div class="detail-item">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table table-bordered spark-tutor-datatable">
                                    <thead>
                                        <tr>
                                            <th class="width-th-25"><?php echo esc_html__('Tutors', 'spark-tutor') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
foreach ($tutors as $tutor) {
	?>
                                        <tr>
                                            <td>
                                                <div class="name">
                                                    <h5><a href="<?php echo spark_get_view_url('view', $tutor->getField('ID')) ?>"><?php echo esc_html($tutor->getField('name')) ?></a></h5>
                                                </div>
                                                <div class="row information" style="margin-top: 10px">
                                                    <div class="col-sm-4">
                                                        <a href="<?php echo spark_get_view_url('view', $tutor->getField('ID')) ?>"><img class="avt" src="<?php echo esc_attr($tutor->getField('picture')) ?>"></a>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <p><?php echo esc_html__('Email', 'spark-tutor') ?>: <?php echo esc_html($tutor->getField('email')) ?></p>
                                                        <p><?php echo esc_html__('Status', 'spark-tutor') ?>: <?php echo HtmlHelper::render_badge_label($tutor->getField('status')) ?></p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <p><?php echo esc_html__('Date Created', 'spark-tutor') ?>: <?php echo date($date_format, strtotime($tutor->getField('created_date'))) ?></p>
                                                        <p><?php echo esc_html__('Phone', 'spark-tutor') ?>: <?php echo esc_html($tutor->getField('phone')) ?></p>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>























            </div>
        </div>
    </div>
</div>
