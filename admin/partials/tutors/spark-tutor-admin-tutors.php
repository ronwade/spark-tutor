<div class="gutut twbs gu-tut-main-content">
    <div class="container">
        <?php
$view = spark_get_current_view();
if ($view == 'view') {
	require_once 'spark-tutor-admin-tutors-view.php';
} elseif ($view == 'edit') {
	require_once 'spark-tutor-admin-tutors-edit.php';
} elseif ($view == 'course-edit') {
	require_once 'spark-tutor-admin-tutors-course-edit.php';
} elseif ($view == 'working-time') {
	require_once 'gu-totur-admin-tutors-availability.php';
} elseif ($view == 'off-time') {
	require_once 'gu-totur-admin-tutors-off-time.php';
} elseif ($view == 'enable-portal') {
	require_once 'gu-totur-admin-enable-portal.php';
} elseif ($view == 'email-view') {
	require_once 'spark-tutor-email-view.php';
} else {
	require_once 'spark-tutor-admin-tutors-listing.php';
}
?>
    </div>
</div>
