<?php
$bookings = BookingServices::get_bookings_for_receipt();
$render_bookings = array();
$view = isset($_GET['view']) ? sanitize_text_field($_GET['view']) : '';
$paypal_amount = 0;
$stripe_amount = 0;
foreach ($bookings as $booking) {
	if ($booking->getField('payment_method') == SPARK_TUTOR_PAYMENT_METHOD_PAYPAL) {
		if ($view == "paypal") {
			array_push($render_bookings, $booking);
		}
		$paypal_amount += intval($booking->getField('price'));
	}
	if ($booking->getField('payment_method') == SPARK_TUTOR_PAYMENT_METHOD_STRIPE) {
		if ($view == "stripe") {
			array_push($render_bookings, $booking);
		}
		$stripe_amount += intval($booking->getField('price'));
	}
	if (empty($view) || $view == 'all') {
		array_push($render_bookings, $booking);
	}

}
$date_format = spark_date_format();
?>
<div class="row tutor-view booking-list">
    <div class="col-md-12">
        <div class="card">
            <div class="content-view mt-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="name">
                            <h3 class="mb-4"><?php echo esc_html__('Receipts', 'spark-tutor') ?></h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="example col-md-12 ml-auto mr-auto">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12 mb-4">
                            <div class="card">
                                <div class="card-text"><?php echo esc_html__('Total Receipts', 'spark-tutor') ?></div>
                                <div class="row receipt_stats">
                                    <div class="col-10 col-sm-10"><?php echo count($bookings) ?></div>
                                    <div class="col-2 col-sm-2"><i class="fa fa-file-text" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-12 mb-4 sm-hidden">
                            <div class="card">
                                <div class="card-text"><?php echo esc_html__('Total Amount', 'spark-tutor') ?></div>
                                <div class="row receipt_stats">
                                    <div class="col-10 col-sm-10">$<?php echo esc_html($paypal_amount + $stripe_amount) ?></div>
                                    <div class="col-2 col-sm-2"><i class="fa fa-money" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-12 mb-4 sm-hidden">
                            <div class="card">
                                <div class="card-text"><?php echo esc_html__('Total PayPal Amount', 'spark-tutor') ?></div>
                                <div class="row receipt_stats">
                                    <div class="col-10 col-sm-10">$<?php echo esc_html($paypal_amount) ?></div>
                                    <div class="col-2 col-sm-2"><i class="fa fa-cc-paypal" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-12 mb-4 sm-hidden">
                            <div class="card">
                                <div class="card-text"><?php echo esc_html__('Total Stripe Amount', 'spark-tutor') ?></div>
                                <div class="row receipt_stats">
                                    <div class="col-10 col-sm-10">$<?php echo esc_html($stripe_amount) ?></div>
                                    <div class="col-2 col-sm-2"><i class="fa fa-cc-stripe" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-status">
                    <div class="row">
                        <div class="col-sm-12 scrollable">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?php echo (!isset($view) || $view == '') ? 'active' : '' ?>" href="<?php echo spark_get_view_url('') ?>" role="tab" ><?php echo esc_html__('All', 'spark-tutor') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($view == 'paypal') ? 'active' : '' ?>"  href="<?php echo spark_get_view_url('paypal') ?>" role="tab" aria-selected="false"><?php echo esc_html__('PayPal', 'spark-tutor') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo ($view == 'stripe') ? 'active' : '' ?>" id="contact-tab" href="<?php echo spark_get_view_url('stripe') ?>" role="tab" aria-selected="false"><?php echo esc_html__('Stripe', 'spark-tutor') ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12 table-responsive">
                        <table class="table table-striped gu-datatable">
                            <thead>
                                <tr>
                                    <th><?php echo esc_html__('Unique Booking ID', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Payment Method', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Student', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Tutor', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Created Date', 'spark-tutor') ?></th>
                                    <th><?php echo esc_html__('Amount', 'spark-tutor') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($render_bookings as $booking) {?>
                                <tr>
                                    <td><?php echo esc_html($booking->getField('unique_id')) ?></td>
                                    <td><?php echo esc_html($booking->getField('payment_method')) ?></td>
                                    <td><?php echo esc_html($booking->getField('student_name')) ?></td>
                                    <td><?php echo esc_html($booking->getField('tutor_name')) ?></td>
                                    <td><?php echo esc_html(date($date_format . ' H:i:s', strtotime($booking->getField('created_date')))) ?></td>
                                    <td>$<?php echo esc_html($booking->getField('price')) ?></td>
                                </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
