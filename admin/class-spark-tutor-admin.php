<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://osoobe.com
 * @since      1.0.0
 *
 * @package    Spark_Tutor
 * @subpackage Spark_Tutor/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Spark_Tutor
 * @subpackage Spark_Tutor/admin
 * @author     Oshane Bailey <b4.oshany@gmail.com>
 */
class Spark_Tutor_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		//Add actions

		//Add actions to set and delete admin cookies
		add_action('init', array($this, 'gu_tut_admin_set_cookie'));
		add_action('wp_logout', array($this, 'gu_tut_admin_delete_cookie'));

		//Add admin menu action
		add_action('admin_menu', array($this, 'gu_tut_admin_menu'));

		//Init custom post type
		add_action('init', array($this, 'gu_tut_create_post_type_handle'));

		//Init register taxonomy
		add_action('init', array($this, 'gu_tut_create_taxonomy_handle'));

		//Add save form action
		add_action('wp_ajax_save_form_info', array($this, 'gu_tut_save_form_info_handle'));
		add_action('admin_post_save_form_info', array($this, 'gu_tut_save_form_info_handle'));

		//Add delete post ajax
		add_action('admin_post_delete_object', array($this, 'gu_tut_delete_object_handle'));

		//Add update post status ajax
		add_action('admin_post_update_post_status', array($this, 'gu_tut_update_post_status_handle'));

		//save settings
		add_action('admin_post_spark_tutor_save_settings', array($this, 'spark_tutor_save_settings_handle'));

		//load tutors
		add_action('wp_ajax_load_tutors', array($this, 'spark_tutors_load_tutors'));
		add_action('wp_ajax_nopriv_load_tutors', array($this, 'spark_tutors_load_tutors'));

		//load course
		add_action('wp_ajax_load_course', array($this, 'spark_tutors_load_course'));
		add_action('wp_ajax_nopriv_load_course', array($this, 'spark_tutors_load_course'));

		//load review
		add_action('wp_ajax_load_review', array($this, 'spark_tutors_load_review'));
		add_action('wp_ajax_nopriv_load_review', array($this, 'spark_tutors_load_review'));

		//save booking
		add_action('wp_ajax_save_booking', array($this, 'spark_tutors_save_booking'));
		add_action('wp_ajax_nopriv_save_booking', array($this, 'spark_tutors_save_booking'));

		//get available day
		add_action('wp_ajax_get_available_day', array($this, 'spark_tutors_get_available_day'));
		add_action('wp_ajax_nopriv_get_available_day', array($this, 'spark_tutors_get_available_day'));

		//get picked available day
		add_action('wp_ajax_get_pick_available_day', array($this, 'spark_tutors_get_pick_available_day'));
		add_action('wp_ajax_nopriv_get_pick_available_day', array($this, 'spark_tutors_get_pick_available_day'));

		//get revenue over time
		add_action('wp_ajax_get_revenue_over_time', array($this, 'spark_tutors_get_revenue_over_time'));

		//get revenue recent year
		add_action('wp_ajax_get_revenue_recent_year', array($this, 'spark_tutors_get_revenue_recent_year'));

		//config smtp
		add_action('phpmailer_init', array($this, 'phpmailer_init_function'));

		//delete available date
		add_action('wp_ajax_delete_available_day', array($this, 'spark_tutors_delete_available_day'));

		//check finish lesson
		add_action('wp_ajax_check_finish_course', array($this, 'check_finish_course_function'));
		add_action('wp_ajax_nopriv_check_finish_course', array($this, 'check_finish_course_function'));

		//send test email
		add_action('wp_ajax_send_test_email', array($this, 'send_test_email_action'));
		add_action('wp_ajax_nopriv_send_test_email', array($this, 'send_test_email_action'));

		// send email to tutor
		add_action('send_tutor_registration_email', array($this, 'send_tutor_registration_email_action'), 10, 1);
		add_action('send_tutor_template_email', array($this, 'send_tutor_template_email_action'), 9, 3);

		//dowload tutor csv
		add_action('admin_post_download_csv', array($this, 'download_csv_action'));

		//Import sample data ajax
		add_action('wp_ajax_import_sample_data', array($this, 'gu_tut_import_sample_data'));

		//start session
		add_action('init', 'start_session', 1);
		function start_session() {
			if (!session_id()) {
				session_start();
			}
		}

		//get date format to front end
		add_action('wp_ajax_get_date_format', array($this, 'spark_tutors_get_date_format'));
		add_action('wp_ajax_nopriv_get_date_format', array($this, 'spark_tutors_get_date_format'));

		//get course_info
		add_action('wp_ajax_get_course_info', array($this, 'spark_tutors_get_course_info'));

		//save course_info
		add_action('admin_post_form_save_course', array($this, 'redirect_spark_tutors_form_save_course'));
		add_action('tutor_post_form_save_course', array($this, 'spark_tutors_form_save_course'));
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Spark_Tutor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Spark_Tutor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/spark-tutor-admin.css', array(), $this->version, 'all');
		wp_enqueue_style('bootstrap', SPARK_TUTOR_PLUGIN_URL . 'assets/css/bootstrap-ns.min.css', array(), $this->version, 'all');
		wp_enqueue_style('shards', SPARK_TUTOR_PLUGIN_URL . 'assets/css/shards.css', array(), $this->version, 'all');
		wp_enqueue_style('shards-extras', SPARK_TUTOR_PLUGIN_URL . 'assets/css/shards-extras.css', array(), $this->version, 'all');
		wp_enqueue_style('font-awesome', SPARK_TUTOR_PLUGIN_URL . 'assets/css/font-awesome/css/font-awesome.min.css', array(), $this->version, 'all');
		wp_enqueue_style('fullcalendar-core-css', SPARK_TUTOR_PLUGIN_URL . 'assets/css/fullcalendar-core/main.css', array(), $this->version, 'all');
		wp_enqueue_style('fullcalendar-daygrid-css', SPARK_TUTOR_PLUGIN_URL . 'assets/css/fullcalendar-daygrid/main.css', array(), $this->version, 'all');
		wp_enqueue_style('fullcalendar-timegrid-css', SPARK_TUTOR_PLUGIN_URL . 'assets/css/fullcalendar-timegrid/main.css', array(), $this->version, 'all');
		wp_enqueue_style('fullcalendar-list-css', SPARK_TUTOR_PLUGIN_URL . 'assets/css/fullcalendar-list/main.css', array(), $this->version, 'all');
		wp_enqueue_style('font-roboto', 'https://fonts.googleapis.com/css2?family=Roboto&display=swap', array(), $this->version, 'all');
		wp_enqueue_style('jquery-ui-css', plugin_dir_url(__FILE__) . 'css/jquery-ui.css', array(), $this->version, 'all');

		wp_enqueue_style('datatable-css', SPARK_TUTOR_PLUGIN_URL . 'assets/css/datatables.min.css', array(), $this->version, 'all');
		wp_enqueue_style('parsley-css', plugin_dir_url(__FILE__) . 'css/parsley.css', array(), $this->version, 'all');
		global $pagenow;
		if ($pagenow == 'admin.php') {
			if (isset($_GET['page']) && sanitize_text_field($_GET['page']) == 'spark-tutor-bookings') {
				wp_enqueue_style($this->plugin_name . 'custom-booking-page-css', plugin_dir_url(__FILE__) . 'css/spark-tutor-admin-custom-booking-page.css', array(), $this->version, 'all');
			}
		}
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Spark_Tutor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Spark_Tutor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script('datatable-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/datatables.min.js', array('jquery'), $this->version, false);
		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/spark-tutor-admin.js', array('jquery'), $this->version, false);
		wp_enqueue_script('bootstrap-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/bootstrap.bundle.min.js', array('jquery'), $this->version, true);
		wp_enqueue_script('jquery-ui-js', plugin_dir_url(__FILE__) . 'js/jquery-ui.js', array('jquery'), $this->version, true);
		wp_enqueue_script('parsley', plugin_dir_url(__FILE__) . 'js/parsley.js', array('jquery'), $this->version, false);
		wp_enqueue_script('shards-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/spark.js', array('jquery'), $this->version, false);
		global $pagenow;
		if ($pagenow == 'admin.php') {
			if (isset($_GET['page']) && sanitize_text_field($_GET['page']) == 'spark-tutor') {
				wp_enqueue_script('chartjs', plugin_dir_url(__FILE__) . 'js/chartjs.min.js', array('jquery'), $this->version, false);
				wp_enqueue_script('bootstrap-notify', plugin_dir_url(__FILE__) . 'js/bootstrap-notify.js', array('jquery'), $this->version, false);
				wp_enqueue_script('ui-dashboard', plugin_dir_url(__FILE__) . 'js/now-ui-dashboard.js', array('jquery'), $this->version, false);
				wp_enqueue_script('ui-dashboard-ui', plugin_dir_url(__FILE__) . 'js/dashboard.js', array('jquery'), $this->version, false);
				wp_enqueue_script('popper', plugin_dir_url(__FILE__) . 'js/popper.min.js', array('jquery'), $this->version, false);
				wp_enqueue_script('perfect-scrollbar', plugin_dir_url(__FILE__) . 'js/perfect-scrollbar.jquery.min.js', array('jquery'), $this->version, false);

			}
			if (isset($_GET['page']) && (sanitize_text_field($_GET['page']) == 'spark-tutor-calendar' || sanitize_text_field($_GET['page']) == 'spark-tutor-tutors') || sanitize_text_field($_GET['page']) == 'spark-tutor-bookings') {
				wp_enqueue_script('fullcalendar-core-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/fullcalendar-core/main.js', array('jquery'), $this->version, false);
				wp_enqueue_script('fullcalendar-daygrid-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/fullcalendar-daygrid/main.js', array('jquery'), $this->version, false);
				wp_enqueue_script('fullcalendar-timegrid-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/fullcalendar-timegrid/main.js', array('jquery'), $this->version, false);
				wp_enqueue_script('fullcalendar-list-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/fullcalendar-list/main.js', array('jquery'), $this->version, false);
				wp_enqueue_script('fullcalendar-interaction-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/fullcalendar-interaction/main.js', array('jquery'), $this->version, false);
			}
		}

	}

	public function gu_tut_admin_set_cookie() {
		$user = wp_get_current_user();

		if($user->has_cap('read_private_posts')) {
			setcookie('spark_can_read_private', '1');
		}
	}

	public function gu_tut_admin_delete_cookie() {
		setcookie('spark_can_read_private', '', 1);
		unset($_COOKIE['spark_can_read_private']);
	}

	public function gu_tut_admin_menu() {
		add_menu_page(esc_html__('SparkTutor', 'spark-tutor'), esc_html__('SparkTutor', 'spark-tutor'), 'manage_options', 'spark-tutor', array($this, 'gu_tut_admin_dashboard_handle'), 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAABvAAAAbwHxotxDAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAARFQTFRF////AP//G5ShGZKiGpGhGpGhGpGhGZGhGpGhGpGhGL60GbaxGbyzGcG2GcO3GpGhGpSiGpymGqSqGqWqGqaqGqutGrmzG66vG8O5G8i7HJGgHMm8HZKiHZakINDCIdDCI4+eI8K5I9HDJNHDJs/BJtHEKI6cMdTHM9THN8C1OYeRPNXKQ8/FRdjMRoiRSrS5S4eQTp6PUIOKVtvRW9zSZWxsZmZmZ2dnaIF/aMXIaWpqa4GEbYKDboCDcnx+eH98e7SpfOPbf5x9gH19gtjWiubfjefgk+jhk+jileLdl7m2mIt0nI5znY9zndHNqqqqrO3ouuflzvXy0IqL1fb01/f05/r46LO1+f79/f//////VhO7rgAAAAp0Uk5TAAEmcH+As7Xm9myQZpsAAAC9SURBVBhXZc9JTsNAEEDRX9WmM5FEiiIZFtlxAu5/EBAnCCF2sIWHHqpZZAc3eE8A1IliJRsgIOo9ACFYQRC/pF5v6YczUyiCq4+7vZYRpv7rnJ0uTI9zThCHW4e5asm0Mgsxh+ETlyoB9uuLUJ6161FV2NWx7Z8Oi9UJtAIO6Z3TY0nNBnD6wNS9xn7r249mJGqBFO1leuOafsCqHHweWj9Lc8tGMFeKp7gucJm/YbQ7XaNI2oQ7/V/uT/8XxO5qPh3LnasAAAAASUVORK5CYII=');
		add_submenu_page('spark-tutor', esc_html__('Dashboard', 'spark-tutor'), esc_html__('Dashboard', 'spark-tutor'), 'manage_options', 'spark-tutor', array($this, 'gu_tut_admin_dashboard_handle'));
		add_submenu_page('spark-tutor', esc_html__('Tutors', 'spark-tutor'), esc_html__('Tutors', 'spark-tutor'), 'manage_options', 'spark-tutor-tutors', array($this, 'gu_tut_admin_tutors_handle'));
		add_submenu_page('spark-tutor', esc_html__('Subjects', 'spark-tutor'), esc_html__('Subjects', 'spark-tutor'), 'manage_options', 'spark-tutor-subjects', array($this, 'gu_tut_admin_subjects_handle'));
		add_submenu_page('spark-tutor', esc_html__('Bookings', 'spark-tutor'), esc_html__('Bookings', 'spark-tutor'), 'manage_options', 'spark-tutor-bookings', array($this, 'gu_tut_admin_bookings_handle'));
		add_submenu_page('spark-tutor', esc_html__('Calendar', 'spark-tutor'), esc_html__('Calendar', 'spark-tutor'), 'manage_options', 'spark-tutor-calendar', array($this, 'gu_tut_admin_calendar_handle'));
		add_submenu_page('spark-tutor', esc_html__('Reports', 'spark-tutor'), esc_html__('Reports', 'spark-tutor'), 'manage_options', 'spark-tutor-reports', array($this, 'gu_tut_admin_reports_handle'));
		add_submenu_page('spark-tutor', esc_html__('Reviews', 'spark-tutor'), esc_html__('Reviews', 'spark-tutor'), 'manage_options', 'spark-tutor-reviews', array($this, 'gu_tut_admin_reviews_handle'));
		add_submenu_page('spark-tutor', esc_html__('Receipts', 'spark-tutor'), esc_html__('Receipts', 'spark-tutor'), 'manage_options', 'spark-tutor-receipts', array($this, 'gu_tut_admin_receipts_handle'));
		add_submenu_page('spark-tutor', esc_html__('Analytics', 'spark-tutor'), esc_html__('Analytics', 'spark-tutor'), 'manage_options', 'spark-tutor-analytics', array($this, 'gu_tut_admin_analytics_handle'));
		add_submenu_page('spark-tutor', esc_html__('Settings', 'spark-tutor'), esc_html__('Settings', 'spark-tutor'), 'manage_options', 'spark-tutor-settings', array($this, 'gu_tut_admin_settings_handle'));
	}

	public function gu_tut_admin_dashboard_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'dashboard' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-dashbroad.php';
		ob_flush();
	}
	public function gu_tut_admin_reports_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'reports' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-reports.php';
		ob_flush();
	}
	public function gu_tut_admin_tutors_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'tutors' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-tutors.php';
		ob_flush();
	}
	public function gu_tut_admin_subjects_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'subjects' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-subjects.php';
		ob_flush();
	}
	public function gu_tut_admin_bookings_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'bookings' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-bookings.php';
		ob_flush();
	}
	public function gu_tut_admin_calendar_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'calendar' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-calendar.php';
		ob_flush();
	}
	public function gu_tut_admin_reviews_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'reviews' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-reviews.php';
		ob_flush();
	}
	public function gu_tut_admin_receipts_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-receipts.php';
		ob_flush();
	}
	public function gu_tut_admin_analytics_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'analytics' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-analytics.php';
		ob_flush();
	}
	public function gu_tut_admin_settings_handle() {
		ob_start();
		require_once SPARK_TUTOR_PLUGIN_ADMIN_DIR . 'partials' . DIRECTORY_SEPARATOR . 'settings' . DIRECTORY_SEPARATOR . 'spark-tutor-admin-settings.php';
		ob_flush();
	}

	//Create custom post types for plugin
	public function gu_tut_create_post_type_handle() {
		$allow_public = false;
		//Create booking post type:
		register_post_type(SPARK_TUTOR_BOOKING_POST_TYPE,
			[
				'labels' => [
					'name' => 'Bookings',
					'singular_name' => 'Booking',
				],
				'public' => $allow_public,
				'has_archive' => true,
				'supports' => [
					'title',
					'editor',
					'custom-fields',
				],
			]
		);

		//Create tutor post type:
		register_post_type(SPARK_TUTOR_TUTOR_POST_TYPE,
			[
				'labels' => [
					'name' => 'Tutors',
					'singular_name' => 'Tutor',
				],
				'public' => $allow_public,
				'has_archive' => true,
				'supports' => [
					'title',
					'editor',
					'custom-fields',
				],
			]
		);
		//Create subject post type:
		register_post_type(SPARK_TUTOR_SUBJECT_POST_TYPE,
			[
				'labels' => [
					'name' => 'Subjects',
					'singular_name' => 'Subject',
				],
				'public' => $allow_public,
				'has_archive' => true,
				'supports' => [
					'title',
					'editor',
					'custom-fields',
				],
			]
		);

		//Create course post type:
		register_post_type(SPARK_TUTOR_COURSE_POST_TYPE,
			[
				'labels' => [
					'name' => 'Courses',
					'singular_name' => 'Course',
				],
				'public' => $allow_public,
				'has_archive' => true,
				'supports' => [
					'title',
					'editor',
					'custom-fields',
				],
			]
		);

		//Create availability post type:
		register_post_type(SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE,
			[
				'labels' => [
					'name' => 'Available Days',
					'singular_name' => 'Available Days',
				],
				'public' => $allow_public,
				'has_archive' => true,
				'supports' => [
					'title',
					'editor',
					'custom-fields',
				],
				'taxonomies' => array('weekday'),
			]
		);

		//Create review post type:
		register_post_type(SPARK_TUTOR_REVIEW_POST_TYPE,
			[
				'labels' => [
					'name' => 'Reviews',
					'singular_name' => 'Review',
				],
				'public' => $allow_public,
				'has_archive' => true,
				'supports' => [
					'title',
					'editor',
					'custom-fields',
				],
			]
		);

	}
	//for create custom taxonomies
	public function gu_tut_create_taxonomy_handle() {
		register_taxonomy('weekday', 'gu_tut_available_day', array(
			'label' => 'Weekday',
			'labels' => array(
				'name' => esc_html__('Weekday', 'spark-tutor'),
				'singular_name' => esc_html__('Weekday', 'spark-tutor'),
			),
			'public' => true,
			'hierarchical' => true,

		));
	}

	//For save form action
	public function gu_tut_save_form_info_handle() {
		$entity_type = isset($_POST['entity_type']) ? sanitize_text_field($_POST['entity_type']) : '';
		$enity = new CPEntity();
		switch ($entity_type) {
		case SPARK_TUTOR_SUBJECT_POST_TYPE:
			$enity = new CPSubject();
			break;
		case SPARK_TUTOR_TUTOR_POST_TYPE:
			$enity = new CPTutor();
			break;
		case SPARK_TUTOR_BOOKING_POST_TYPE:
			$enity = new CPBooking();
			break;
		case SPARK_TUTOR_COURSE_POST_TYPE:
			$enity = new CPCourse();
			break;
		case SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE:
			$enity = new CPAvailableDay();
			break;
		default:
			break;
		}
		$postData = $_POST;
		$id = $enity->save_post_data($postData);
		$view = spark_get_current_view($postData);

		if ( empty($id) ) {
			if ( empty($postData['id']) ) {
				wp_redirect(spark_get_view_url('list'));
				die();
			}
			$id = $postData['id'];
		}

		if ($view == 'edit') {
			if ($postData['status'] == SPARK_TUTOR_STATUS_FINISHED) {
				$this->send_mail_review_report($id);
			}
			if (isset($postData['modal']) && $postData['modal'] == '1') {
				wp_redirect(spark_get_view_url('edit', $id, spark_get_current_page($postData)) . '&modal=' . $postData['modal']);
			} else {
				wp_redirect(spark_get_view_url('view', $id, spark_get_current_page($postData)));
			}
		}
		die;
	}

	public function gu_tut_delete_object_handle() {
		$id = isset($_POST['id']) ? sanitize_text_field($_POST['id']) : '';
		$view_post_id = isset($_POST['view_post_id']) ? sanitize_text_field($_POST['view_post_id']) : '';
		if (!empty($id)) {
			wp_trash_post($_POST['id']);
		}

		if ($view_post_id === $id) {
			wp_redirect(spark_get_view_url('', null, spark_get_current_page($_POST)));
		} else {
			wp_redirect(spark_get_view_url('edit', $view_post_id, spark_get_current_page($_POST)));

		}

		die;
	}

	public function gu_tut_update_post_status_handle() {
		$id = isset($_POST['id']) ? sanitize_text_field($_POST['id']) : '';
		$status = isset($_POST['status']) ? sanitize_text_field($_POST['status']) : '';
		$doUpdate = true;

		if (!empty($id) && !empty($status)) {

			$post_type = get_post_type($id);

			if ( $post_type == SPARK_TUTOR_TUTOR_POST_TYPE ) {
				$tutor = CPTutor::getEntity($id);
				if (!empty($tutor)) {

					if ($tutor->getField('status') != $status) {

						update_post_meta($id, 'status', sanitize_text_field($status));
						if ( $status == SPARK_TUTOR_STATUS_REVIEWED ) {
							do_action(
								'send_tutor_template_email',
								$tutor->user()->ID,
								"tutor-session-invite-template.php");
						} else if (in_array($status, SPARK_TUTOR_REGISTRATION_STATUS_LIST)) {
							do_action(
								'send_tutor_template_email',
								$tutor->user()->ID,
								"tutor-welcome-template.php");
						}

					}

					echo json_encode(['status' => 1]);
				} else {
					echo json_encode(['status' => 0]);
				}
				die();
			}

			update_post_meta($id, 'status', sanitize_text_field($status));
			if ($status == SPARK_TUTOR_STATUS_FINISHED) {
				$this->send_mail_review_report($id);
			}
			echo json_encode(['status' => 1]);
		}
		die;
	}

	public function spark_tutor_save_settings_handle() {
		$spark_tutor_options = get_option(SPARK_TUTOR_OPTIOM);
		$data = $_POST;
		if ($data['setting_option'] == 'general') {
			$spark_tutor_options['product-description'] = sanitize_text_field($data['product-description']);
			$spark_tutor_options['tutor_onboarding_course'] = sanitize_text_field($data['tutor_onboarding_course']);
			$spark_tutor_options['tax'] = sanitize_text_field($data['tax']);
			$spark_tutor_options['date-format'] = sanitize_text_field($data['date-format']);
			$spark_tutor_options['term_condition_content'] = stripslashes(wp_filter_post_kses(addslashes($data['term_condition_content'])));
			$spark_tutor_options['booking-page'] = sanitize_text_field($data['booking-page']);
		}
		if ($data['setting_option'] == 'payment') {
			//paypal
			$spark_tutor_options['enabled-paypal'] = sanitize_text_field($data['enabled-paypal']);
			$spark_tutor_options['paypal-mode'] = sanitize_text_field($data['paypal-mode']);
			$spark_tutor_options['paypal-email'] = sanitize_text_field($data['paypal-email']);
			//stripe
			$spark_tutor_options['enabled_stripe'] = sanitize_text_field($data['enabled_stripe']);
			$spark_tutor_options['stripe_secret_key'] = sanitize_text_field($data['stripe_secret_key']);
			$spark_tutor_options['stripe_public_key'] = sanitize_text_field($data['stripe_public_key']);
		}
		if ($data['setting_option'] == 'email') {
			//admin email
			$spark_tutor_options['enabled_admin_email'] = sanitize_text_field($data['enabled_admin_email']);
			$spark_tutor_options['admin_email_subject'] = sanitize_text_field($data['admin_email_subject']);
			$spark_tutor_options['admin_email_heading'] = sanitize_text_field($data['admin_email_heading']);
			$spark_tutor_options['admin_email'] = sanitize_text_field($data['admin_email']);
			//tutor email
			$spark_tutor_options['enabled_tutor_email'] = sanitize_text_field($data['enabled_tutor_email']);
			$spark_tutor_options['tutor_email_subject'] = sanitize_text_field($data['tutor_email_subject']);
			$spark_tutor_options['tutor_email_heading'] = sanitize_text_field($data['tutor_email_heading']);
			//student email
			$spark_tutor_options['enabled_student_email'] = sanitize_text_field($data['enabled_student_email']);
			$spark_tutor_options['student_email_subject'] = sanitize_text_field($data['student_email_subject']);
			$spark_tutor_options['student_email_heading'] = sanitize_text_field($data['student_email_heading']);
			//host
			$spark_tutor_options['host'] = sanitize_text_field($data['host']);
			$spark_tutor_options['from_email'] = sanitize_text_field($data['from_email']);
			$spark_tutor_options['from_name'] = sanitize_text_field($data['from_name']);
			$spark_tutor_options['smtp_username'] = sanitize_text_field($data['smtp_username']);
			$spark_tutor_options['smtp_password'] = sanitize_text_field($data['smtp_password']);
			$spark_tutor_options['smtp_encryption'] = sanitize_text_field($data['smtp_encryption']);
			$spark_tutor_options['port'] = sanitize_text_field($data['port']);
			$spark_tutor_options['test_email'] = sanitize_text_field($data['test_email']);
		}
		update_option(SPARK_TUTOR_OPTIOM, $spark_tutor_options);
		$url = admin_url("admin.php?page=spark-tutor-settings&view=" . $data['setting_option']);
		wp_redirect($url);
	}

	public function spark_tutors_load_tutors() {
		$subject_id = sanitize_text_field($_POST['subject']);
		$args = array(
			'meta_query' => array(
				array(
					'key' => 'subject',
					'value' => serialize(strval($subject_id)),
					'compare' => 'LIKE',
				),
			),
		);
		$data = array();
		$list_subject = spark_get_list_entity(SPARK_TUTOR_TUTOR_POST_TYPE, $args);

		foreach ($list_subject as $key => $value) {
			$id = $value->getField('ID');
			$arg = array(
				'meta_key' => 'tutor',
				'meta_value' => $id,
			);
			$args = array(
				'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'tutor',
						'value' => $id,
						'compare' => '=',
					),
				),
			);
			$string_star = '';
			$list_review = spark_get_list_entity(SPARK_TUTOR_REVIEW_POST_TYPE, $args);
			if ($list_review) {
				$count = count($list_review);
				$sum = 0;
				foreach ($list_review as $k => $val) {
					$sum += $val->getField('rating');
				}
				$star = ceil($sum / $count);

				for ($i = 0; $i < 5; $i++) {
					if ($i < $star) {
						$string_star .= '<i class="fa fa-star"></i>';
					} else {
						$string_star .= '<i class="fa fa-star-o"></i>';
					}

				}
				$string_star .= ' ' . $star . '.0';
			} else {
				$string_star .= '<i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> 0.0';
			}

			$list_available_days = spark_get_list_entity(SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE, $arg);
			$list_course_by_tutor = CourseServices::get_courses_by_tutor_id($value->getField('ID'));
			$price_min = $list_course_by_tutor[0]->getField('price');
			$price_max = $list_course_by_tutor[0]->getField('price');
			$subject_tag = '';
			$subjects = $value->getField('subject');
			if (empty($subjects)) {
				$subjects = array();
			}
			foreach ($subjects as $subject_id) {
				$subject = spark_get_entity_by_id($subject_id);
				$subject_tag .= '<span class="badge mr-3 badge-subject">' . esc_html($subject->getField('name')) . '</span>';
			}
			foreach ($list_course_by_tutor as $v) {
				if ($v->getField('price') > $price_max) {
					$price_max = $v->getField('price');
				}
				if ($v->getField('price') < $price_min) {
					$price_min = $v->getField('price');
				}
			}
			$list_booking_by_tutor = BookingServices::get_bookings_by_tutor_id($value->getField('ID'));
			$tutored_hours = 0;
			foreach ($list_booking_by_tutor as $booking) {
				$tutored_hours += (int) $booking->getField('duration') / 60;
			}
			$tutored_hours_html = ($tutored_hours != 0) ? 'Tutored ' . $tutored_hours . ' ' . spark_render_text($tutored_hours, 'hour') : 'New Tutor';
			$html = '<div class="row mb-4 tutor row_tutor_' . esc_attr($id) . '">
						<div class="col-lg-7 col-md-12 p-0">
							<div class="card tutor-info">
								<div class="row card-body">
									<div class="col-sm-3 left-tutor-info">
										<a onclick="load_review(' . $id . ')">
											<img src="' . esc_attr($value->getField('picture')) . '" alt="tutor-avatar" class="img-thumbnail">
										</a>
										<a href="#" data-toggle="modal" data-target="#tutorDetailModal">
											<span class="star-color">' . $string_star . '</span><br>
										</a>
										<div class="tutored_hours">' . esc_html($tutored_hours_html) . '</div>
										<button type="button" class="btn btn-primary mt-2 tutor tutor_' . esc_attr($id) . '" onclick="load_course(' . esc_attr($id) . ')">' . esc_html__('BOOK', 'spark-tutor') . '</button>
									</div>
									<div class="col-sm-9 position-relative">
										<h4 class="card-title">' . esc_html($value->getField('name')) . '</h4>
										<span class="position-absolute range_price badge mr-3 badge-range-price">$' . esc_html($price_min) . ' - $' . esc_html($price_max) . '</span>
										<p class="mb-2 tutor-brief-intro"><span class="intro">' . esc_html(spark_substrwords($value->getField('intro'), 290)) . '</span>
										<a  onclick="load_review(' . esc_attr($id) . ')">' . esc_html__('Read More', 'spark-tutor') . ' </a></p>
										' . $subject_tag . '
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-5 col-md-12 p-0">
							<div class="card tutor-info">
								<div class="card-body">
									<h4 class="card-title">' . esc_html__('Calendar', 'spark-tutor') . '</h4>

									<table class="table table-bordered text-center">
										<tr class="reduce-height">
											<td></td>
											<td class="today"><span class="day_in_month">' . date('d', strtotime('now')) . '</span><br><span class="day_of_week">' . date('D', strtotime('now')) . '</span></td>
											<td><span class="day_in_month">' . date('d', strtotime('+1 day')) . '</span><br><span class="day_of_week">' . date('D', strtotime('+1 day')) . '</span></td>
											<td><span class="day_in_month">' . date('d', strtotime('+2 day')) . '</span><br><span class="day_of_week">' . date('D', strtotime('+2 day')) . '</span></td>
											<td><span class="day_in_month">' . date('d', strtotime('+3 day')) . '</span><br><span class="day_of_week">' . date('D', strtotime('+3 day')) . '</span></td>
											<td><span class="day_in_month">' . date('d', strtotime('+4 day')) . '</span><br><span class="day_of_week">' . date('D', strtotime('+4 day')) . '</span></td>
											<td><span class="day_in_month">' . date('d', strtotime('+5 day')) . '</span><br><span class="day_of_week">' . date('D', strtotime('+5 day')) . '</span></td>
											<td><span class="day_in_month">' . date('d', strtotime('+6 day')) . '</span><br><span class="day_of_week">' . date('D', strtotime('+6 day')) . '</span></td>
										</tr>';
			$start = 0;
			$end = 4;
			while ($start <= 20) {
				$sunday = $monday = $tuesday = $wednesday = $thursday = $friday = $saturday = 'class="unavailable-hour"';
				foreach ($list_available_days as $key => $day) {
					$weekday = wp_get_post_terms($day->getField('ID'), 'weekday', array('fields' => 'names'));
					$start_time = (int) $day->getField('start_time');
					$end_time = (int) $day->getField('end_time');

					if (($start_time >= $start * 60 && $start_time < $end * 60) || ($end_time >= $start * 60 && $end_time < $end * 60) || ($start_time < $start * 60 && $end_time > $end * 60)) {
						switch ($weekday[0]) {
						case 'sunday':
							$sunday = 'class="available-hour"';
							break;
						case 'monday':
							$monday = 'class="available-hour"';
							break;
						case 'tuesday':
							$tuesday = 'class="available-hour"';
							break;
						case 'wednesday':
							$wednesday = 'class="available-hour"';
							break;
						case 'thursday':
							$thursday = 'class="available-hour"';
							break;
						case 'friday':
							$friday = 'class="available-hour"';
							break;
						case 'saturday':
							$saturday = 'class="available-hour"';
							break;
						default:
							break;
						};
					}

				}
				$available_arg = array();
				switch (date('w', strtotime('now'))) {
				case 0:
					$available_arg = [$sunday, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday];
					break;
				case 1:
					$available_arg = [$monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday];
					break;
				case 2:
					$available_arg = [$tuesday, $wednesday, $thursday, $friday, $saturday, $sunday, $monday];
					break;
				case 3:
					$available_arg = [$wednesday, $thursday, $friday, $saturday, $sunday, $monday, $tuesday];
					break;
				case 4:
					$available_arg = [$thursday, $friday, $saturday, $sunday, $monday, $tuesday, $wednesday];
					break;
				case 5:
					$available_arg = [$friday, $saturday, $sunday, $monday, $tuesday, $wednesday, $thursday];
					break;
				case 6:
					$available_arg = [$saturday, $sunday, $monday, $tuesday, $wednesday, $thursday, $friday];
					break;
				default:
					break;
				};
				$am_pm = ($start <= 12) ? 'AM' : 'PM';
				$time = ($start <= 12 && $start > 0) ? $start : abs($start - 12);
				$html = $html . '<tr>
							<td class="width-10">' . $time . ' ' . $am_pm . '</td>
							<td ' . $available_arg[0] . '></td>
							<td ' . $available_arg[1] . '></td>
							<td ' . $available_arg[2] . '></td>
							<td ' . $available_arg[3] . '></td>
							<td ' . $available_arg[4] . '></td>
							<td ' . $available_arg[5] . '></td>
							<td ' . $available_arg[6] . '></td>
						</tr>';
				$start += 4;
				$end = $start + 4;
			}
			$html = $html . '</table>
						</div>
					</div>
				</div>
			</div>';
			array_push($data,
				array(
					'id' => $id,
					'avg_rating' => $value->getField('avg_rating'),
					'earining' => $value->getField('earining'),
					'email' => $value->getField('email'),
					'intro' => $value->getField('intro'),
					'name' => $value->getField('name'),
					'phone' => $value->getField('phone'),
					'picture' => $value->getField('picture'),
					'status' => $value->getField('status'),
					'intro' => $value->getField('intro'),
					'html' => $html,
					'trn_ssn' => $value->getField('trn_ssn'),

				)
			);
		}
		echo json_encode($data);

		die();
	}

	public function spark_tutors_load_course() {
		$spark_tutor_options = get_option(SPARK_TUTOR_OPTIOM);
		$tutor_id = sanitize_text_field($_POST['tutor']);
		$subject_id = sanitize_text_field($_POST['subject']);
		$args = array(
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'subject',
					'value' => $subject_id,
					'compare' => '=',
				),
				array(
					'key' => 'tutor',
					'value' => $tutor_id,
					'compare' => '=',
				),
			),
		);
		$tutor_name = spark_get_entity_by_id($tutor_id)->getField('name');
		$tutor_picture = spark_get_entity_by_id($tutor_id)->getField('picture');
		// $tutor_resume = spark_get_entity_by_id($tutor_id)->getField('resume');
		$data = array();
		$list_course = spark_get_list_entity(SPARK_TUTOR_COURSE_POST_TYPE, $args);
		error_log(json_encode($list_course));
		foreach ($list_course as $key => $value) {
			$tax = $spark_tutor_options['tax'] * $value->getField('price') / 100;
			$args = array(
				'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'course_name',
						'value' => $value->getField('name'),
						'compare' => 'like',
					),
				),
			);
			$string_star = '';
			$count = 0;
			$list_review = spark_get_list_entity(SPARK_TUTOR_REVIEW_POST_TYPE, $args);
			if ($list_review) {
				$count = count($list_review);
				$sum = 0;
				foreach ($list_review as $k => $val) {
					$sum += $val->getField('rating');
				}
				$star = ceil($sum / $count);

				for ($i = 0; $i < 5; $i++) {
					if ($i < $star) {
						$string_star .= '<i class="fa fa-star"></i>';
					} else {
						$string_star .= '<i class="fa fa-star-o"></i>';
					}

				}
				$string_star .= ' ' . $star . '.0';
			} else {
				$string_star .= '<i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> 0.0';
			}
			if ($value->getField('detail') != '') {
				$detail_html = '<i class="fa fa-check course-check-mark" aria-hidden="true"></i> ' . str_replace('<br />', '<br /><i class="fa fa-check course-check-mark" aria-hidden="true"></i> ', nl2br(esc_html($value->getField('detail'))));
			} else {
				$detail_html = '<i>' . esc_html__('No detail', 'spark-tutor') . '</i>';
			}
			array_push($data, array(
				'html' => '<div class="col-sm-12 mt-4" course_id="' . esc_attr($value->getField('ID')) . '" price="' . esc_attr($value->getField('price')) . '" tax="' . esc_attr($tax) . '" tutor_name="' . esc_attr($tutor_name) . '" course_name="' . esc_attr($value->getField('name')) . '" picture="' . esc_attr($tutor_picture) . '" duration="' . esc_attr($value->getField('duration')) . '">
									<div class="card course-info tooltipster" data-tooltip-content="#course-detail-' . esc_attr($value->getField('ID')) . '">
									<div class="tooltip_templates">
										<div id="course-detail-' . esc_attr($value->getField('ID')) . '">
											<h6><b>' . esc_html__('What you\'ll learn', 'spark-tutor') . '</b></h6>
												<span> ' . $detail_html . '
												</span>
										</div>
									</div>
										<div class="card-body row">
											<div class="course-thumbnail col-lg-3 col-sm-3">
												<img src="' . esc_attr($value->getField('thumbnail')) . '" alt="course-thumbnail" class="img-thumbnail">
											</div>
											<div class="col-lg-7 col-sm-6">
												<h5 class="card-title font-weight-bold">' . $value->getField('name') . '</h5>
												<span class="badge mr-3 badge-subject">' . esc_html(spark_get_entity_by_id($value->getField('subject'))->getField('name')) . '</span>
												<div class="d-inline-block"><i class="fa fa-clock-o total-minutes" aria-hidden="true"></i> <span class="card-text total-minutes">' . esc_html($value->getField('duration')) . esc_html__(' minutes', 'spark-tutor') . ' </span></div>
												<div class="course-description mt-2 mb-2">
													' . esc_html($value->getField('description')) . '
												</div>
											</div>
											<div class="col-lg-2 col-sm-3">
												<h5 class="card-title font-weight-bold"><span class=" badge badge-range-price pull-right">$' . esc_html($value->getField('price')) . '</span></h5><br>
												<div class="d-inline-block star-color pull-right">
													' . $string_star . '
												</div><br>
												<span class="total-rating pull-right">(' . esc_html($count) . ' ' . esc_html(spark_render_text($count, esc_html__('rating'))) . ')</span>
											</div>
										</div>
									</div>
								</div>',
				'id' => $value->getField('ID'),
				'name' => $value->getField('name'),
				'price' => $value->getField('price'),
				'tax' => $tax,
				'duration' => $value->getField('duration'),
			)
			);
		}
		echo json_encode($data);

		die();
	}

	public function spark_tutors_load_review() {
		$date_format = spark_date_format();
		$tutor_id = sanitize_text_field($_POST['tutor']);
		$args = array(
			'post_status' => array('publish'),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'tutor',
					'value' => $tutor_id,
					'compare' => '=',
				),
				array(
					'key' => 'type',
					'value' => SPARK_TUTOR_TYPE_REVIEW,
					'compare' => '=',
				),
			),
		);
		$tutor = spark_get_entity_by_id($tutor_id);
		$tutor_name = $tutor->getField('name');
		$tutor_picture = $tutor->getField('picture');
		$tutor_intro = $tutor->getField('intro');
		$data = array();
		$list_review = spark_get_list_entity(SPARK_TUTOR_REVIEW_POST_TYPE, $args);
		if (count($list_review) == 0) {
			array_push($data, array(
				'tutor_name' => $tutor_name,
				'tutor_intro' => $tutor_intro,
				'not' => 0));
			echo json_encode($data);

			die();
		}
		foreach ($list_review as $key => $value) {

			array_push($data, array(
				'rating' => $value->getField('rating'),
				'comment' => $value->getField('comment'),
				'tutor_name' => $tutor_name,
				'tutor_intro' => $tutor_intro,
				'course_name' => $value->getField('course_name'),
				'student_name' => $value->getField('student_name'),
				'booking_date' => date($date_format . ' H:i:s', strtotime($value->getField('booking_date'))),
				'not' => 1,
			)
			);
		}

		echo json_encode($data);

		die();
	}

	function spark_tutors_save_booking() {
		$enity = new CPBooking();
		$result = $enity->save_post_data($_POST);
		$unique_id = array(
			'data' => get_post_meta($result, 'unique_id', false),
			'post_id' => $result,
		);
		echo json_encode($unique_id);
		die();
	}
	function spark_tutors_get_available_day() {
		$tutor_id = sanitize_text_field($_POST['tutor_id']);
		$arg = array(
			'meta_key' => 'tutor',
			'meta_value' => $tutor_id,
		);
		$result = array();
		$list_available_day = spark_get_list_entity(SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE, $arg);

		foreach ($list_available_day as $key => $value) {
			$weekday = wp_get_post_terms($value->getField('ID'), 'weekday', array('fields' => 'names'));
			switch ($weekday[0]) {
			case 'sunday':
				$day = '0';
				break;
			case 'monday':
				$day = '1';
				break;
			case 'tuesday':
				$day = '2';
				break;
			case 'wednesday':
				$day = '3';
				break;
			case 'thursday':
				$day = '4';
				break;
			case 'friday':
				$day = '5';
				break;
			case 'saturday':
				$day = '6';
				break;
			default:
				break;
			};
			$result[] = array(
				'start_time' => $value->getField('start_time'),
				'end_time' => $value->getField('end_time'),
				'weekday' => $day,
			);
		}
		echo json_encode($result);
		die();
	}
	function spark_tutors_get_pick_available_day() {
		if (isset($_POST['tutor_id'])) {
			//show booking in step 3 in front end
			$tutor_id = sanitize_text_field($_POST['tutor_id']);
			$result = array();
			$list_picked_available_day = BookingServices::get_bookings_by_tutor_id($tutor_id);
			foreach ($list_picked_available_day as $key => $value) {
				$date = date_create($value->getField('booking_date'));
				$result[] = array(
					'start_time' => $value->getField('start_time'),
					'end_time' => $value->getField('end_time'),
					'pick_date' => date_format($date, "Y-m-d"),
					'weekday' => date_format($date, "w"),
				);
			}
		} else {
			//show booking in calendar page in back end
			$array_color = array(
				'Confirmed' => '#007BFF',
				'InProgress' => '#FFB400',
				'Finished' => '#17C671',
				'Cancelled' => '#C4183C',

			);
			//fillters
			$meta_query = array(
				'relation' => 'AND',
				array(
					'key' => 'status',
					'value' => 'Pending',
					'compare' => '!=',
				),
			);
			if (isset($_POST['fillter_tutor']) and sanitize_text_field($_POST['fillter_tutor']) != '') {
				$meta_query[] = array(
					'key' => 'tutor',
					'value' => sanitize_text_field($_POST['fillter_tutor']),
					'compare' => '=',
				);
			}
			if (isset($_POST['fillter_subject']) and sanitize_text_field($_POST['fillter_subject']) != '') {
				$meta_query[] = array(
					'key' => 'subject',
					'value' => sanitize_text_field($_POST['fillter_subject']),
					'compare' => '=',
				);
			}
			if (isset($_POST['fillter_status']) and sanitize_text_field($_POST['fillter_status']) != '') {
				$meta_query[] = array(
					'key' => 'status',
					'value' => sanitize_text_field($_POST['fillter_status']),
					'compare' => '=',
				);
			}
			$arg = array(
				'meta_query' => $meta_query,
			);
			$result = array();
			$list_picked_available_day = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $arg);
			$tutor_colors = array();
			$list_tutor = spark_get_list_entity(SPARK_TUTOR_TUTOR_POST_TYPE, []);

			foreach ($list_picked_available_day as $key => $value) {
				$date = date_create($value->getField('booking_date'));
				$tutor = spark_get_entity_by_id($value->getField('tutor'));
				foreach ($array_color as $key => $color) {
					if ($key == $value->getField('status')) {
						$color_booking = $color;
					}
				}
				$result[] = array(
					'start_time' => $value->getField('start_time'),
					'end_time' => $value->getField('end_time'),
					'pick_date' => date_format($date, "Y-m-d"),
					'color' => $color_booking,
					'id' => $value->getField('ID'),
					'title' => $tutor->getField('name'),
				);
			}

		}
		echo json_encode($result);
		die();
	}
	function spark_tutors_get_revenue_over_time() {
		$now_date = strtotime('now');
		$arg = array(
			'date_query' => array(
				array(
					'year' => date('Y', $now_date),
				),
			),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'status',
					'value' => 'Pending',
					'compare' => '!=',
				),
				array(
					'key' => 'status',
					'value' => 'Cancelled',
					'compare' => '!=',
				),
			),
		);
		$list_booking = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $arg);
		$jan = $feb = $mar = $apr = $may = $jun = $jul = $aug = $sep = $otc = $nov = $dec = 0;
		foreach ($list_booking as $key => $value) {
			$date = date_create($value->getField('date_create'));
			$month = date_format($date, 'm');
			switch ($month) {
			case '01':
				$jan += (int) $value->getField('total');
				break;
			case '02':
				$feb += (int) $value->getField('total');
				break;
			case '03':
				$mar += (int) $value->getField('total');
				break;
			case '04':
				$apr += (int) $value->getField('total');
				break;
			case '05':
				$may += (int) $value->getField('total');
				break;
			case '06':
				$jun += (int) $value->getField('total');
				break;
			case '07':
				$jul += (int) $value->getField('total');
				break;
			case '08':
				$aug += (int) $value->getField('total');
				break;
			case '09':
				$sep += (int) $value->getField('total');
				break;
			case '10':
				$otc += (int) $value->getField('total');
				break;
			case '11':
				$nov += (int) $value->getField('total');
				break;
			case '12':
				$dec += (int) $value->getField('total');
				break;

			default:
				break;
			}
		}
		$result = array($jan, $feb, $mar, $apr, $may, $jun, $jul, $aug, $sep, $otc, $nov, $dec);
		echo json_encode($result);
		die();
	}
	function spark_tutors_get_revenue_recent_year() {
		$now_date = strtotime('now');
		$arg = array(
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'status',
					'value' => 'Pending',
					'compare' => '!=',
				),
				array(
					'key' => 'status',
					'value' => 'Cancelled',
					'compare' => '!=',
				),
			),
		);
		$list_booking = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $arg);
		$count_month = 11;
		$result = array();
		while ($count_month >= 0) {
			$month_year = date('Y-m', strtotime('-' . $count_month . ' month'));
			$month = date('M', strtotime('-' . $count_month . ' month'));
			$revenue = 0;
			foreach ($list_booking as $key => $value) {
				$date = date_create($value->getField('date_create'));
				$month_year_booking = date_format($date, 'Y-m');
				$month_booking = date_format($date, 'm');
				if ($month_year_booking == $month_year) {
					$revenue += (int) $value->getField('total');
				}
			}
			$count_month -= 1;
			$result[] = array(
				$month => $revenue,
			);

		}
		echo json_encode($result);
		die();
	}

	public function phpmailer_init_function($phpmailer) {
		$cb_options = get_option(SPARK_TUTOR_OPTIOM);
		$phpmailer->isSMTP();
		if (isset($cb_options['host']) and $cb_options['host'] != '') {
			$phpmailer->Host = $cb_options['host'];
		}

		$phpmailer->SMTPAuth = true; // Force it to use Username and Password to authenticate
		if (isset($cb_options['port']) and $cb_options['port'] != '') {
			$phpmailer->Port = $cb_options['port'];
		}

		if (isset($cb_options['smtp_username']) and $cb_options['smtp_username'] != '') {
			$phpmailer->Username = $cb_options['smtp_username'];
		}

		if (isset($cb_options['smtp_password']) and $cb_options['smtp_password'] != '') {
			$phpmailer->Password = $cb_options['smtp_password'];
		}

		if (isset($cb_options['from_email']) and $cb_options['from_email'] != '') {
			$phpmailer->From = $cb_options['from_email'];
		}

		if (isset($cb_options['from_name']) and $cb_options['from_name'] != '') {
			$phpmailer->FromName = $cb_options['from_name'];
		}

	}
	public function spark_tutors_delete_available_day() {
		$arg = array(
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'start_time',
					'value' => sanitize_text_field($_POST['start_time']),
					'compare' => '=',
				),
				array(
					'key' => 'end_time',
					'value' => sanitize_text_field($_POST['end_time']),
					'compare' => '=',
				),
				array(
					'key' => 'tutor',
					'value' => sanitize_text_field($_POST['tutor']),
					'compare' => '=',
				),
			),
		);
		$list_available_day = spark_get_list_entity(SPARK_TUTOR_AVAILABLE_DAY_POST_TYPE, $arg);

		foreach ($list_available_day as $key => $value) {
			$weekday = wp_get_post_terms($value->getField('ID'), 'weekday', array('fields' => 'names'));
			switch ($weekday[0]) {
			case 'sunday':
				$day = '0';
				break;
			case 'monday':
				$day = '1';
				break;
			case 'tuesday':
				$day = '2';
				break;
			case 'wednesday':
				$day = '3';
				break;
			case 'thursday':
				$day = '4';
				break;
			case 'friday':
				$day = '5';
				break;
			case 'saturday':
				$day = '6';
				break;
			default:
				break;
			};
			if ($day == sanitize_text_field($_POST['weekday'])) {
				wp_delete_post($value->getField('ID'));
			}
		}
		die();
	}

	public function check_finish_course_function() {
		$args = array(
			'post_type' => 'gu_tut_booking',
			'meta_query' => array(
				array(
					'key' => 'status',
					'value' => SPARK_TUTOR_STATUS_INPROGRESS,
				),
			),
		);

		$current_time = date('Y-m-d H:i:s');
		$current_time = strtotime($current_time);

		$posts = get_posts($args);
		foreach ($posts as $post) {
			$post_meta = get_post_meta($post->ID);

			$booking_time = strtotime($post_meta['booking_date'][0]);

			$duration = $post_meta['duration'][0] * 60;

			if ($booking_time + $duration < $current_time + 900) {
				$this->send_mail_review_report($post->ID);
			}
		}
	}

	public function send_test_email_action() {

		$test_email = sanitize_text_field($_POST['test_email']);
		$_mail_subject = 'Test Email ' . $test_email;

		$template = new Template(SPARK_TUTOR_PLUGIN_PATH . 'includes/email_templates/test_email_template.php');
		$template->set('test_email', $test_email);

		$merge_message = $template->render();
		$body = $merge_message;

		$result = wp_mail($test_email, $_mail_subject, $body);

		echo esc_html($result);
		die();
	}

	protected function _tutor_email_template($user, $template_file) {

		$template = build_tutor_email_template($user, $template_file);
		$email = $user->get("user_email");
		$headers = array('Content-Type: text/html; charset=UTF-8');

		return [
			$email,
			$template,
			$headers
		];

	}


	/**
	 * @param mixed $user				WordPress User ID or object.
	 * @param string $template_file		Email template file.
	 * @param string $subject			Email subject
	 *
	 * @return string	Result from wp_mail.
	 */
	public function send_tutor_template_email_action (
		$user,
		$template_file,
		$subject='Tutor Account Status for Spark Education'
	) {

		if ( is_int($user) ) {
			$user = new \WP_User($user);
		}

		[
			$email,
			$template,
			$headers
		] = $this->_tutor_email_template(
			$user,
			"$template_file"
		);

		$merge_message = $template->render();
		$body = $merge_message;
		$headers[] = 'Bcc: '.spark_bcc_support();

		$result = wp_mail($email, $subject, $body, $headers);
		return esc_html($result) ;
	}

	/**
	 * @param mixed $user
	 *
	 * @return [type]
	 */
	public function send_tutor_registration_email_action($user) {
		return $this->send_tutor_template_email_action(
			$user,
			"tutor-registration-template.php"
		);
	}

	public function download_csv_action() {

		$type = sanitize_text_field($_POST['type']);
		$bookings = BookingServices::search_booking_by_status(null, null);

		$data_stats = array();
		foreach ($bookings as $booking) {
			$Id = $booking->getField($type);
			if (!array_key_exists($Id, $data_stats)) {
				$data_stats[$Id]['all_booking'] = 0;
				$data_stats[$Id][SPARK_TUTOR_STATUS_CONFIRMED] = 0;
				$data_stats[$Id][SPARK_TUTOR_STATUS_FINISHED] = 0;
				$data_stats[$Id][SPARK_TUTOR_STATUS_CANCELLED] = 0;
				$data_stats[$Id]['total'] = 0;
			}
			$data_stats[$Id]['all_booking'] += 1;
			$data_stats[$Id][$booking->getField('status')] += 1;
			$data_stats[$Id]['total'] += $booking->getField('price');

		}

		switch ($type) {
		case 'subject':
			$name = 'Subject';
			break;

		default:
			$name = 'Tutor';
			break;
		}
		$filename = $name . ".csv";
		$fp = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename=' . $filename);

		$header = array(esc_html__($name, 'spark-tutor'), esc_html__('All Bookings', 'spark-tutor'), esc_html__('Confirmed', 'spark-tutor'), esc_html__('Finished', 'spark-tutor'), esc_html__('Cancelled', 'spark-tutor'), esc_html__('Total Earnings', 'spark-tutor'));
		fputcsv($fp, $header);
		foreach ($data_stats as $key => $value) {
			$tutor = spark_get_entity_by_id($key);
			$body = array($tutor->getField('name'), $value['all_booking'], $value[SPARK_TUTOR_STATUS_CONFIRMED], $value[SPARK_TUTOR_STATUS_FINISHED], $value[SPARK_TUTOR_STATUS_CANCELLED], $value['total']);
			fputcsv($fp, $body);
		}
		fclose($fp);
	}

	public function gu_tut_import_sample_data() {
		DemoDataServices::insert_demo_data();
		echo "{'status':1}";
		die;
	}

	public function send_mail_review_report($post_id) {
		update_post_meta($post_id, 'status', SPARK_TUTOR_STATUS_FINISHED);
		update_post_meta($post_id, 'tutor_token', spark_generateUniqueId($post_id));
		update_post_meta($post_id, 'student_token', spark_generateUniqueId($post_id));

		$cb_options = get_option(SPARK_TUTOR_OPTIOM);
		$data = get_post_meta($post_id);
		$tutor_meta = get_post_meta($data['tutor'][0]);
		$course_meta = get_post_meta($data['course'][0]);
		$date = date("d-m-Y", strtotime($data['booking_date'][0]));
		$time = strtotime($data['booking_date'][0]) + $data['duration'][0] * 60;
		$time = date("H:i", $time);
		$template = new Template(SPARK_TUTOR_PLUGIN_PATH . 'includes/email_templates/student-review-template.php');
		$template->set('duration', $data['duration'][0]);
		$template->set('date', $date);
		$template->set('time', $time);
		$template->set('tutor_name', $tutor_meta['name'][0]);
		$template->set('course_name', $course_meta['name'][0]);
		$template->set('student_name', $data['student_name'][0]);
		$template->set('link', get_home_url() . '/tutor-booking/?step=review&id=' . $data['unique_id'][0] . '&token=' . $data['student_token'][0]);

		$student_heading = isset($cb_options['student_email_heading']) ? $cb_options['student_email_heading'] : '';
		$student_mail_subject = "Review your Tutor";
		$student_heading = empty($student_heading) ? "Student Heading" : $student_heading;

		$merge_message = $template->render();
		$body = $merge_message;
		wp_mail($data['student_email'][0], $student_mail_subject, $body, $student_heading);

		//Send tutor
		$template = new Template(SPARK_TUTOR_PLUGIN_PATH . 'includes/email_templates/tutor-report-template.php');
		$template->set('tutor_name', $tutor_meta['name'][0]);
		$template->set('link', get_home_url() . '/tutor-booking/?step=review&id=' . $data['unique_id'][0] . '&token=' . $data['tutor_token'][0]);
		$template->set('duration', $data['duration'][0]);
		$template->set('course_name', $course_meta['name'][0]);
		$template->set('date', $date);
		$template->set('student_name', $data['student_name'][0]);
		$template->set('time', $time);

		$tutor_heading = isset($cb_options['tutor_email_heading']) ? $cb_options['tutor_email_heading'] : '';
		$tutor_mail_subject = "Give report to your Student";
		$tutor_heading = empty($tutor_heading) ? "Tutor Heading" : $tutor_heading;

		$merge_message = $template->render();
		$body = $merge_message;
		wp_mail($tutor_meta['email'][0], $tutor_mail_subject, $body, $tutor_heading);
	}
	public function spark_tutors_get_date_format() {
		echo json_encode(spark_date_format());
		die();

	}
	public function spark_tutors_get_course_info() {
		$course_info = spark_get_entity_by_id($_POST['course_id']);
		$result = array(
			'name' => $course_info->getField('name'),
			'duration' => $course_info->getField('duration'),
			'price' => $course_info->getField('price'),
			'subject' => $course_info->getField('subject'),
			'id' => $course_info->getField('ID'),
			'description' => $course_info->getField('description'),
			'detail' => $course_info->getField('detail'),
			'thumbnail' => $course_info->getField('thumbnail'),
		);
		echo json_encode($result);
		die();
	}
	public function spark_tutors_form_save_course() {
		$enity = new CPCourse();
		$postData = $_POST;
		$enity->save_post_data($postData);
		wp_redirect(get_new_tutor_lms_links('my-courses', true));
	}

	public function redirect_spark_tutors_form_save_course() {
		$id = $this->spark_tutors_form_save_course();
		$redirect_view = post_url_param("redirect_view", "edit");
		$tutor_id = post_url_param('tutor');
		wp_redirect(admin_url() . "admin.php?page=spark-tutor-tutors&view=$redirect_view&id=$tutor_id");
	}
}
