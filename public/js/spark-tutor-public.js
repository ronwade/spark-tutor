var calendar=null;
var renderBookingPage;

(function($) {
    'use strict';

    renderBookingPage = function(){
    $(document).ready(function() {

        if($('#star-wrapper').length){
            $('#star-wrapper i').on('click', function(e){
                var selectedNumber=parseInt($(this).attr('data-num'));
                $('#rating').val(selectedNumber);
                var i=0;
                for (i=1;i<= selectedNumber; i++){
                    $('#star-'+i).attr('class','fa fa-star');
                }
                var i=selectedNumber+1;
                for (i=selectedNumber+1;i<= 5; i++){
                    $('#star-'+i).attr('class','fa fa-star-o');
                }
            });
        }

        $('#subject').change( function(e){
            $('.step-1 .js-btn-next').removeAttr('disabled');

            
        });
        $('.lazy').lazy();
        $('#carouselSubjects').slick({
          rows: 2,
          dots: true,
          arrows: true,
          infinite: false,
          speed: 300,
          slidesToShow: 4,
          slidesToScroll: 4,
          prevArrow: $('.carousel-control-prev'),
          nextArrow: $('.carousel-control-next'),
          responsive: [
            {
                breakpoint: 1199,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: true
                }
            },
            {
                breakpoint: 980,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  infinite: true,
                  dots: true
                }
              },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
        $(".top-subject").show();
        $( window ).resize(function() {
            resize_container_height();
            truncate_course_description();
        });
        


        $('.step-1 .js-btn-next,.step-2 .js-btn-next,.step-3 .js-btn-next,.step-4 .js-btn-next').click(function(){
            scrollToTop();
        });
        if ($('#form-review-rating').length) {


            $('#form-review-rating .btn-primary').on('click', function(e){

                e.preventDefault();
                if($('#form-review-rating').parsley().validate()){
                    $('#form-review-rating').submit();
                }


                    
            });
        };

        //This is for multi step form
        if($('.multisteps-form__progress-btn').length){
            //DOM elements
            const DOMstrings = {
                stepsBtnClass: 'multisteps-form__progress-btn',
                stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
                stepsBar: document.querySelector('.multisteps-form__progress'),
                stepsForm: document.querySelector('.multisteps-form__form'),
                stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
                stepFormPanelClass: 'multisteps-form__panel',
                stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
                stepPrevBtnClass: 'js-btn-prev',
                stepNextBtnClass: 'js-btn-next'
            };


            //remove class from a set of items
            const removeClasses = (elemSet, className) => {

                elemSet.forEach(elem => {

                    elem.classList.remove(className);

                });

            };

            //return exect parent node of the element
            const findParent = (elem, parentClass) => {

                let currentNode = elem;

                while (!currentNode.classList.contains(parentClass)) {
                    currentNode = currentNode.parentNode;
                }

                return currentNode;

            };

            //get active button step number
            const getActiveStep = elem => {
                return Array.from(DOMstrings.stepsBtns).indexOf(elem);
            };

            //set all steps before clicked (and clicked too) to active
            const setActiveStep = activeStepNum => {

                //remove active state from all the state
                removeClasses(DOMstrings.stepsBtns, 'js-active');

                //set picked items to active
                DOMstrings.stepsBtns.forEach((elem, index) => {

                    if (index <= activeStepNum) {
                        elem.classList.add('js-active');
                    }

                });
            };

            //get active panel
            const getActivePanel = () => {

                let activePanel;

                DOMstrings.stepFormPanels.forEach(elem => {

                    if (elem.classList.contains('js-active')) {

                        activePanel = elem;

                    }

                });

                return activePanel;

            };

            //open active panel (and close unactive panels)
            const setActivePanel = activePanelNum => {

                //remove active class from all the panels
                removeClasses(DOMstrings.stepFormPanels, 'js-active');

                //show active panel
                DOMstrings.stepFormPanels.forEach((elem, index) => {
                    if (index === activePanelNum) {

                        elem.classList.add('js-active');

                        setFormHeight(elem);

                    }
                });

            };

            //set form height equal to current panel height
            const formHeight = activePanel => {

                const activePanelHeight = activePanel.offsetHeight;

                DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;

            };

            const setFormHeight = () => {
                const activePanel = getActivePanel();

                formHeight(activePanel);
            };

            //STEPS BAR CLICK FUNCTION
            DOMstrings.stepsBar.addEventListener('click', e => {

                //check if click target is a step button
                const eventTarget = e.target;

                if (!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
                    return;
                }

                //get active button step number
                const activeStep = getActiveStep(eventTarget);
                
                //set all steps before clicked (and clicked too) to active
                setActiveStep(activeStep);

                //open active panel
                setActivePanel(activeStep);
                resize_container_height();
                truncate_course_description();

            });

            //PREV/NEXT BTNS CLICK
            DOMstrings.stepsForm.addEventListener('click', e => {

                const eventTarget = e.target;

                //check if we clicked on `PREV` or NEXT` buttons
                if (!(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`) || eventTarget.classList.contains(`${DOMstrings.stepNextBtnClass}`))) {
                    return;
                }
                
                //find active panel
                const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);

                let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);

                //set active step and active panel onclick
                if (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
                    activePanelNum--;

                } else {

                    activePanelNum++;

                }
                

                setActiveStep(activePanelNum);
                setActivePanel(activePanelNum);
                resize_container_height();
                truncate_course_description();

            });

            //SETTING PROPER FORM HEIGHT ONLOAD
            window.addEventListener('load', setFormHeight, false);

            //SETTING PROPER FORM HEIGHT ONRESIZE
            window.addEventListener('resize', setFormHeight, false);
        }
        
        call_tooltipster();

    });
    }


    
})(jQuery);

// ----- trim text 
var wordDesc = ""; 
var maxLength = 0;
var originLlength = 0;

// max length of description base on resolution
function ajustMaxLength() {
  var contentWidth = jQuery(window).width(); //get width of browser
  if (
    (contentWidth <= 1182 && contentWidth > 974) ||
    (contentWidth < 751 && contentWidth >= 600)
  ) {
    return 139;
  } else if (contentWidth < 600) {
    return 100;
  }

  return originLlength;
}

function load_tutors(){
    //remove old data when click another subject
    jQuery('.step-2 .js-btn-next').attr('disabled', true);
    jQuery(".data-tutors").html('');
    jQuery.ajax({
        type : "post", 
        dataType : "json", 
        url :window.location.origin+'/wp-admin/admin-ajax.php', 
        data : {
            action: "load_tutors",
            subject: jQuery('#subject').val(),
        },
        success: function(response) {
            jQuery.each( response, function( index, value ) {
                jQuery(".data-tutors").append(value['html']);
            });
            if(response.length > 1 ){
                jQuery('#tutor-found').html(response.length + ' Tutors found');
            }
            else{
                jQuery('#tutor-found').html(response.length + ' Tutor found');
            }

            
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log( 'The following error occured: ' + textStatus, errorThrown );
        }
    })
}
                                    
function load_course(tutors_id){
    jQuery('#tutor_id').val(tutors_id);
    //disable button "BOOK" when book a tutor

	jQuery('.multisteps-form__progress-btn.step-3').removeAttr('disabled');
    jQuery('.step-2 .js-btn-next').removeAttr('disabled');
    jQuery(this).addClass('border border-info rounded');
    jQuery('.tutor').removeClass('disabled');
    jQuery('.row.tutor').removeClass('border border-info rounded');
    var btn_class = '.tutor_'+tutors_id;
    var tutor_class = '.row_tutor_'+tutors_id;
    jQuery(btn_class).addClass('disabled');
    jQuery(tutor_class).addClass('border border-info rounded');
    //remove old data when book another tutor
    jQuery('.course-data').html('');
    jQuery.ajax({
        type : "post", 
        dataType : "json", 
        url :window.location.origin+'/wp-admin/admin-ajax.php', 
        data : {
            action: "load_course",
            tutor: tutors_id,
            subject: jQuery('#subject').val(),
        },
        success: function(response) {

            jQuery.each( response, function( index, value ) {
                //border border-info rounded
                jQuery(".course-data").append(value['html']);
                jQuery(".course-data .mt-4").off('click');
                jQuery(".course-data .mt-4").on('click',function(){
                    jQuery('.step-3 .js-btn-next').removeAttr('disabled');
                    jQuery(".course-data .mt-4").removeClass("border border-info rounded");
                    jQuery(this).addClass("border border-info rounded");
					jQuery('.step-4.multisteps-form__progress-btn').removeAttr('disabled');
                    jQuery("#course_id").val(jQuery(this).attr('course_id'));
                    jQuery("#price").val(jQuery(this).attr('price'));
                    jQuery("#tax").val(jQuery(this).attr('tax'));
                    jQuery("#total").val(Number(jQuery(this).attr('tax')) + Number(jQuery(this).attr('price')));
                    jQuery("#duration").val(jQuery(this).attr('duration'));
                    jQuery('.tutor_name').html(jQuery(this).attr('tutor_name'));
                    jQuery('.course_name').html(jQuery(this).attr('course_name'));
                    jQuery('.price-number').html('$'+jQuery(this).attr('price'));
                    jQuery('.tax').html('$'+jQuery(this).attr('tax'));
                    jQuery('.total').html('$'+ (Number(jQuery(this).attr('tax')) + Number(jQuery(this).attr('price'))));
                    jQuery('.tutor_avatar').attr('src', jQuery(this).attr('picture'));
                });
            });
            call_tooltipster();
            
            
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log( 'The following error occured: ' + textStatus, errorThrown );
        }
    })
    

    get_availability_day(tutors_id);

}

function save_booking(){
    var payment = jQuery('input[name="payment_method"]:checked').val();
    
    if(jQuery('.multisteps-form__form').parsley().validate()){
        if(payment=="PayPal"){
            jQuery(".fa-3x").show();
        } else{
            jQuery(".fa-3x").show();
            setInterval(function(){
                if(jQuery('.stripe_checkout_app').is(":visible")){
                   jQuery(".fa-3x").hide();
                }    
            },3000)
            
            
        }
        jQuery.ajax({
            type : "post",
            dataType : "json",
            url :window.location.origin+'/wp-admin/admin-ajax.php',
            data : {
                action: "save_booking",
                student_name: jQuery('#student_name').val(),
                student_phone: jQuery('#student_phone').val(),
                student_email: jQuery('#student_email').val(),
                student_grade: jQuery('#student_grade').val(),
                subject: jQuery('#subject').val(),
                course: jQuery("#course_id").val(),
                note: jQuery('#note').val(),
                payment_method: payment,
                price: jQuery("#price").val(),
                duration: jQuery("#duration").val(),
                tax: jQuery("#tax").val(),
                total: jQuery("#total").val(),
                start_time: jQuery("#start_time").val(),
                end_time: jQuery('#end_time').val(),
                booking_date: jQuery('#booking_date').val(),
                tutor: jQuery('#tutor_id').val()

            },
            success: function(response) {
                if (payment == 'PayPal'){
                    jQuery('#paypal_amount').val(jQuery("#total").val());
                    jQuery('#paypal_return').val(window.location.href + '?step=confirm&booking-id='+response['data']);
                    jQuery('#frm_paypal_checkout').submit();

                }else if (payment == 'Stripe') {
                    let amount = parseFloat(jQuery("#total").val() * 100);
                    StripeCheckout.open({
                        key:         jQuery('#key_stripe').val(),
                        billingAddress:     false,
                        amount:      amount,
                        currency:    'USD',
                        name:        'TutorBooking',
                        description: 'TutorBooking',
                        email:       jQuery('#student_email').val(),
                        panelLabel:  'Checkout',
                        token:       function(res){
                            let input1 = jQuery('<input type="hidden" name="total" />').val(amount);
                            let input2 = jQuery('<input type="hidden" name="booking_id" />').val(response['data']);
                            let input3 = jQuery('<input type="hidden" name="post_id" />').val(response['post_id']);
                            let input4 = jQuery('<input type="hidden" name="stripeToken" /></form>').val(res.id);
                            jQuery('<form action="" method="post" novalidate="novalidate">').append(input1).append(input2).append(input3).append(input4).appendTo('body').submit();
                        },
                        locale: 'auto'
                    });
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log( 'The following error occured: ' + textStatus, errorThrown );
            }
        });
    }

    
}


function get_availability_day(tutor_id){
            //get picked by another student
           jQuery.ajax({
            type : "post", 
            dataType : "json", 
            url :window.location.origin+'/wp-admin/admin-ajax.php', 
            data : {
                action: "get_pick_available_day",
                tutor_id: tutor_id
            },
            success: function(response) {
                
                var pick_date = [];
                 for(var i = 0; i< response.length ; i++){
                        var x = reference_day_of_week[response[i]['weekday']];
                        var y = reference_hour[response[i]['start_time']];
                        if(reference_data[y][x]['booking_date'] == response[i]['pick_date']){
                            for(y ; y < reference_hour[response[i]['end_time']]; y++){
                            pick_date.push({row: y, col: x});
                        }
                        }
                        
                    }
                    //get available time
                    jQuery.ajax({
                        type : "post", 
                        dataType : "json", 
                        url :window.location.origin+'/wp-admin/admin-ajax.php', 
                        data : {
                            action: "get_available_day",
                            tutor_id: tutor_id
                        },
                        success: function(response) {

                            for(var i = 0; i< response.length ; i++){
                                var x = reference_day_of_week[response[i]['weekday']];
                                
                                
                                for(var y = reference_hour[response[i]['start_time']]; y < reference_hour[response[i]['end_time']]; y++){
                                    sheetData[y][x] = 2;
                                }
                            }
                            for(var i = 0; i< pick_date.length; i++){
                                sheetData[pick_date[i]['row']][pick_date[i]['col']] = 3;
                            }
                            sheetNrow=sheetData.length;
                            sheetNcol=sheetData[0].length;
                            var tempSheetData=[];
                            var tempHourList=[];
                            var tempReferenceData=[];
                            for(i=0;i<sheetNrow;i++){
                                var rowSum=sheetData[i].reduce(function(acc, val) { return acc + val; }, 0);
                                if(rowSum>0){
                                    if(start_time==0)
                                        start_time=30*i;
                                    tempSheetData.push(sheetData[i]);
                                    tempHourList.push(hourList[i]);
                                    tempReferenceData.push(reference_data[i]);
                                }
                            }
                            reference_data=tempReferenceData;
                            sheetData=tempSheetData;
                            hourList=tempHourList;
                            dimensions[0]=sheetData.length;
                            dimensions[1]=sheetData[0].length;
                            var sheet = jQuery("#availability").TimeSheet({
                                data: {
                                    dimensions : dimensions,
                                    colHead : dayList,
                                    rowHead : hourList,
                                    sheetHead : {name:""},
                                    sheetData : sheetData
                                },
                                remarks : false,
                                start: function(ev, selectedArea){
                                    sheet.clean();
                                },
                                end : function(ev,selectedArea){
                                    if(arraysIdentical(selectedArea.topLeft, selectedArea.bottomRight) == true){
                                        updateRemark(sheet);
                                        
                                        var row = ev.target.dataset['row'];
                                        var col = ev.target.dataset['col'];
                                        var start_time = Number(reference_data[row][col]['start_time']);
                                        var end_time = start_time + Number(jQuery('#duration').val());
                                        var booking_date = reference_data[row][col]['booking_date'];
                                        jQuery('#start_time').val(start_time);
                                        jQuery('#end_time').val(end_time);
                                        jQuery('#booking_date').val(booking_date + " " + convert_minutes_to_time(start_time));
                                        jQuery('.multisteps-form__progress-btn.step-5').removeAttr('disabled');
                                        jQuery('.step-4 .js-btn-next').removeAttr('disabled');
                                        var booking_date_data= new Date(booking_date);
                                        jQuery.ajax({
                                            type : "post", 
                                            dataType : "json", 
                                            url :window.location.origin+'/wp-admin/admin-ajax.php', 
                                            data : {
                                                action: "get_date_format",
                                            },
                                            success: function(response) {
                                                if(response == 'd-m-Y'){
                                                    jQuery('.course_time').html(date_format('DD-MM-YYYY', booking_date_data) + " " + convert_minutes_to_time(start_time) + "-" + convert_minutes_to_time(end_time))
                                                }
                                                if(response == 'm-d-Y'){
                                                    jQuery('.course_time').html(date_format('MM-DD-YYYY', booking_date_data) + " " + convert_minutes_to_time(start_time) + "-" + convert_minutes_to_time(end_time))
                                                }
                                                if(response == 'Y-m-d'){
                                                    jQuery('.course_time').html(date_format('YYYY-MM-DD', booking_date_data) + " " + convert_minutes_to_time(start_time) + "-" + convert_minutes_to_time(end_time))
                                                }
                                            },
                                            error: function( jqXHR, textStatus, errorThrown ){
                                                console.log( 'The following error occured: ' + textStatus, errorThrown );
                                            }
                                        })
                                    }
                                    else{
                                        sheet.clean();
                                    }
                                    
                                }


                            });
                            updateRemark(sheet); 
                        },
                        error: function( jqXHR, textStatus, errorThrown ){
                            console.log( 'The following error occured: ' + textStatus, errorThrown );
                        }
                    })

            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log( 'The following error occured: ' + textStatus, errorThrown );
            }
        })
        
       
    
    var dimensions = [48,7];
    var start_week = new Date();
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    var dayList = [
        {name:"<span class='today day_in_month'>"+start_week.getDate()+"</span><br><span class='today day_of_week'>"+weekday[start_week.getDay()]+"</span>", title:"",},
        {name:"<span class='day_in_month'>"+plus_days(start_week, 1).getDate()+"</span><br><span class='day_of_week'>"+weekday[plus_days(start_week, 1).getDay()]+"</span>", title:"", },
        {name:"<span class='day_in_month'>"+plus_days(start_week, 2).getDate()+"</span><br><span class='day_of_week'>"+weekday[plus_days(start_week, 2).getDay()]+"</span>", title:""},
        {name:"<span class='day_in_month'>"+plus_days(start_week, 3).getDate()+"</span><br><span class='day_of_week'>"+weekday[plus_days(start_week, 3).getDay()]+"</span>", title:""},
        {name:"<span class='day_in_month'>"+plus_days(start_week, 4).getDate()+"</span><br><span class='day_of_week'>"+weekday[plus_days(start_week, 4).getDay()]+"</span>", title:""},
        {name:"<span class='day_in_month'>"+plus_days(start_week, 5).getDate()+"</span><br><span class='day_of_week'>"+weekday[plus_days(start_week, 5).getDay()]+"</span>", title:""},
        {name:"<span class='day_in_month'>"+plus_days(start_week, 6).getDate()+"</span><br><span class='day_of_week'>"+weekday[plus_days(start_week, 6).getDay()]+"</span>", title:""}
    ];
    
    var hourList=[];
    var reference_hour = [];
    var start_minute = 0;
    for(var i = 0; i< 48; i++ ){
        reference_hour[start_minute] = i;
        
        var timeTitle = pad(Math.floor(start_minute/60),2)+":"+pad(start_minute%60,2);
        var timeTitleStart=timeTitle;
        var timeTitleEnd = pad(Math.floor((start_minute+30)/60,2))+":"+pad((start_minute+30)%60,2);
        if(i%2==1){
            timeTitle="&nbsp;";
        }
        hourList.push({name:"<span class='width-10'>"+timeTitle+"</span>",title:timeTitleStart+"-"+timeTitleEnd});
        start_minute += 30;
    }
    var reference_day_of_week = [];
    if(start_week.getDay() == 0){
        reference_day_of_week[0] = 0;
        reference_day_of_week[1] = 1;
        reference_day_of_week[2] = 2;
        reference_day_of_week[3] = 3;
        reference_day_of_week[4] = 4;
        reference_day_of_week[5] = 5;
        reference_day_of_week[6] = 6;
    }
    if(start_week.getDay() == 1){
        reference_day_of_week[0] = 6;
        reference_day_of_week[1] = 0;
        reference_day_of_week[2] = 1;
        reference_day_of_week[3] = 2;
        reference_day_of_week[4] = 3;
        reference_day_of_week[5] = 4;
        reference_day_of_week[6] = 5;
    }
    if(start_week.getDay() == 2){
        reference_day_of_week[0] = 5;
        reference_day_of_week[1] = 6;
        reference_day_of_week[2] = 0;
        reference_day_of_week[3] = 1;
        reference_day_of_week[4] = 2;
        reference_day_of_week[5] = 3;
        reference_day_of_week[6] = 4;
    }  
    if(start_week.getDay() == 3){
        reference_day_of_week[0] = 4;
        reference_day_of_week[1] = 5;
        reference_day_of_week[2] = 6;
        reference_day_of_week[3] = 0;
        reference_day_of_week[4] = 1;
        reference_day_of_week[5] = 2;
        reference_day_of_week[6] = 3;
    } 
    if(start_week.getDay() == 4){
        reference_day_of_week[0] = 3;
        reference_day_of_week[1] = 4;
        reference_day_of_week[2] = 5;
        reference_day_of_week[3] = 6;
        reference_day_of_week[4] = 0;
        reference_day_of_week[5] = 1;
        reference_day_of_week[6] = 2;
    }  
    if(start_week.getDay() == 5){
        reference_day_of_week[0] = 2;
        reference_day_of_week[1] = 3;
        reference_day_of_week[2] = 4;
        reference_day_of_week[3] = 5;
        reference_day_of_week[4] = 6;
        reference_day_of_week[5] = 0;
        reference_day_of_week[6] = 1;
    } 
    if(start_week.getDay() == 6){
        reference_day_of_week[0] = 1;
        reference_day_of_week[1] = 2;
        reference_day_of_week[2] = 3;
        reference_day_of_week[3] = 4;
        reference_day_of_week[4] = 5;
        reference_day_of_week[5] = 6;
        reference_day_of_week[6] = 0;
    } 
        
    

    var sheetData = [];
    var reference_data = [];
    var start_minute = 0;
    for(var i = 0; i<48 ; i++){
        sheetData.push([0,0,0,0,0,0,0]);
        reference_data.push (
            [
            {booking_date: plus_days(start_week, 0).getFullYear() +"-" + leftPad(plus_days(start_week, 0).getMonth()+1, 2) + "-"+leftPad(plus_days(start_week, 0).getDate(), 2), start_time: start_minute},
            {booking_date: plus_days(start_week, 1).getFullYear() +"-" + leftPad(plus_days(start_week, 1).getMonth()+1, 2) + "-"+leftPad(plus_days(start_week, 1).getDate(), 2), start_time: start_minute},
            {booking_date: plus_days(start_week, 2).getFullYear() +"-" + leftPad(plus_days(start_week, 2).getMonth()+1, 2) + "-"+leftPad(plus_days(start_week, 2).getDate(), 2), start_time: start_minute},
            {booking_date: plus_days(start_week, 3).getFullYear() +"-" + leftPad(plus_days(start_week, 3).getMonth()+1, 2) + "-"+leftPad(plus_days(start_week, 3).getDate(), 2), start_time: start_minute},
            {booking_date: plus_days(start_week, 4).getFullYear() +"-" + leftPad(plus_days(start_week, 4).getMonth()+1, 2) + "-"+leftPad(plus_days(start_week, 4).getDate(), 2), start_time: start_minute},
            {booking_date: plus_days(start_week, 5).getFullYear() +"-" + leftPad(plus_days(start_week, 5).getMonth()+1, 2) + "-"+leftPad(plus_days(start_week, 5).getDate(), 2), start_time: start_minute},
            {booking_date: plus_days(start_week, 6).getFullYear() +"-" + leftPad(plus_days(start_week, 6).getMonth()+1, 2) + "-"+leftPad(plus_days(start_week, 6).getDate(), 2), start_time: start_minute},

            ]
            );
        start_minute += 30;
    }
    var start_time=0;
    var updateRemark = function(sheet){

        var sheetStates = sheet.getSheetStates();
        var rowsCount = dimensions[0];
        var colsCount = dimensions[1];
        var rowRemark = [];
        var rowRemarkLen = 0;
        var remarkHTML = '';

        for(var row= 0, rowStates=[]; row<rowsCount; ++row){
            rowRemark = [];
            rowStates = sheetStates[row];
            for(var col=0; col<colsCount; ++col){
                if(rowStates[col]===0 && rowStates[col-1]===1){
                    rowRemark[rowRemarkLen-1] += (col<=10?'0':'')+col+':00';
                }else if(rowStates[col]===1 && (rowStates[col-1]===0 || rowStates[col-1]===undefined)){
                    rowRemarkLen = rowRemark.push((col<=10?'0':'')+col+':00-');
                }
                if(rowStates[col]===1 && col===colsCount-1){
                    rowRemark[rowRemarkLen-1] += '00:00';
                }
            }
            remarkHTML = rowRemark.join("，");
            sheet.setRemark(row,remarkHTML==='' ? sheet.getDefaultRemark() : remarkHTML);
        }
    };


     
}

function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}
function convert_minutes_to_time(minutes){
    var hour = Math.floor(Number(minutes)/60);
    var minute = Number(minutes)%60;
    return leftPad(hour, 2)+":"+leftPad(minute, 2);
}
function convert_time_to_minutes(hour, minute){
    return Number(hour)*60 + Number(minute)
}

function load_review(tutor_id){
    jQuery.ajax({
        type : "post", 
        dataType : "json", 
        url :window.location.origin+'/wp-admin/admin-ajax.php', 
        data : {
            action: "load_review",
            tutor: tutor_id,
        },
        success: function(response) {
            jQuery('#reviews_info > .review_info').remove();
            jQuery.each( response, function( index, value ) {

                jQuery('#tutorDetailLabel').text(value['tutor_name']);
                jQuery('.modal-body #comment').text(value['tutor_intro']);
                if (value['not'] == 0) {
                    jQuery('#tutorDetailModal').modal('toggle');
                    return;
                };
                var string = '';
                for (var i = 0; i < 5; i++) {
                    if (i<value['rating']) 
                        string += '<i class="fa fa-star"></i>';
                    else
                        string += '<i class="fa fa-star-o"></i>';
                };
                string += value['rating']+'.0';
                var html = '<div class="review_info col-sm-12 mb-4"><div class="card"><div class="card-body">'+
                            '<span class="star-color">'+string+'</span>'+
                            '<p class="card-text">'+value['comment']+'</p>'+
                            '<div class="row align-items-end">'+
                            '<div class="col-sm-9"><span class="student-name-bold">'+value['student_name']+'</span><br><span class="course-name-grey">'+value['course_name']+'</span></div>'+
                            '<div class="col-sm-3 course-name-grey">'+value['booking_date']+'</div>'+
                            '</div></div></div></div>';
                jQuery('#reviews_info').append(html);
                jQuery('#tutorDetailModal').modal('toggle');


            });
            
            
        },
        error: function( jqXHR, textStatus, errorThrown ){
            console.log( 'The following error occured: ' + textStatus, errorThrown );
        }
    })
}
function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
    return true;

}
function scrollToTop() { 
    window.scrollTo(0, 0); 
} 
function date_format(format, date){
    var result = '';
    if(format == 'DD-MM-YYYY'){
        result = leftPad(date.getDate(), 2) +"-" + leftPad(date.getMonth()+1, 2) + "-"+ date.getFullYear()  ;
    }
    if(format == 'MM-DD-YYYY'){
        result = leftPad(date.getMonth()+1, 2) + "-"+ leftPad(date.getDate(), 2) +"-" +  date.getFullYear()  ;
    }
    if(format == 'YYYY-MM-DD'){
        result = date.getFullYear() +"-"+ leftPad(date.getMonth()+1, 2) +"-"+ leftPad(date.getDate(), 2)     ;
    }
    return result;
}
function resize_container_height(){
    setTimeout(function(){
        jQuery( ".multisteps-form__form" ).height(jQuery( ".multisteps-form__form .multisteps-form__panel.js-active" ).outerHeight());    
    }, 1000)
}
function set_subject_value(subject_id){
    jQuery('#subject').val(subject_id);
    load_tutors();
    jQuery(".step-1").removeClass('js-active');
    jQuery(".step-2").addClass('js-active');
    jQuery('.step-1 .js-btn-next').removeAttr('disabled');
	jQuery('.step-2.multisteps-form__progress-btn').removeAttr('disabled');
    resize_container_height();
}
function call_tooltipster(){
    jQuery('.tooltipster').tooltipster({
        theme: 'tooltipster-shadow',
        trigger: 'hover',
        side: 'top',
    });
} 
function plus_days(date_input, increment){
    var date = date_input;
    var date_increace = date.setDate(new Date(date).getDate() + increment);
    date_input = date.setDate(new Date(date).getDate() - increment);
    result = new Date(date_increace);
    return result;
}
function arraysIdentical(a, b) {
    var i = a.length;
    if (i != b.length) return false;
    while (i--) {
        if (a[i] !== b[i]) return false;
    }
    return true;
};



function truncate(str, n){
    if(n==0)
        return "";
    return (str.length > n) ? str.substr(0, n-1) + '&hellip;' : str;
};
function truncate_course_description(){

    var contentWidth = jQuery(window).width(); //get width of browser
    var numChar=400;
      if (contentWidth <= 1200 && contentWidth > 992) {
        numChar=285;
      } else if (contentWidth <= 992 && contentWidth > 768){
        numChar=83;
      } else if (contentWidth <= 768 && contentWidth > 576) {
        numChar=0;
      } else if (contentWidth <= 576){
        numChar=400;
      }
     jQuery( ".course-description" ).each(function( index ) {
        var attr = jQuery(this).attr('description');

        if (!(typeof attr !== typeof undefined && attr !== false)) {
            jQuery(this).attr('description',jQuery(this).html());
        }
        jQuery(this).html(truncate(jQuery(this).attr('description'),numChar));
        
    });
}
function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size);
}
