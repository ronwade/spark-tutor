<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://osoobe.com
 * @since      1.0.0
 *
 * @package    Spark_Tutor
 * @subpackage Spark_Tutor/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Spark_Tutor
 * @subpackage Spark_Tutor/public
 * @author     Oshane Bailey <b4.oshany@gmail.com>
 */
class Spark_Tutor_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		//Frontend shortcode render
		function tutor_booking_shortcode_handle() {
			ob_start();
			require_once plugin_dir_path(__FILE__) . 'partials/tutor-booking/spark-tutor-public-tutor-booking.php';
			return ob_get_clean();
		}

		//Frontend shortcode render
		function tutor_registration_shortcode_handle() {
			ob_start();
			require_once plugin_dir_path(__FILE__) . 'partials/tutor-registration/spark-tutor-public-tutor-registration.php';
			return ob_get_clean();
		}

		function spark_tutor_counter() {
			ob_start();
			require_once SPARK_TUTOR_PLUGIN_SHORT_CODES . 'tutor-counter.php';
			return ob_get_clean();

		}

		add_shortcode('spark_tutor_booking', 'tutor_booking_shortcode_handle');
		add_shortcode('spark_tutor_register', 'tutor_registration_shortcode_handle');
		add_shortcode('spark_tutor_counter', 'spark_tutor_counter');

		//Add save rating
		add_action('wp_ajax_save_rating', array($this, 'gu_tut_save_rating_handle'));
		
	}

	public function load_public_templates($template) {
		$post_type = get_query_var( 'gp_type' );
		$action = get_query_var( 'gp_action' );

		if ($post_type == SPARK_TUTOR_TUTOR_POST_TYPE) {
			switch ($action) {
				case 'register';
				$template = SPARK_TUTOR_PLUGIN_PUBLIC_PAGES . 'tutor-registration/spark-tutor-public-tutor-registration.php';
			break;
				case 'status_check';
				$template = SPARK_TUTOR_PLUGIN_PUBLIC_PAGES . 'tutor-registration/spark-tutor-public-tutor-registration-status.php';
			break;
			}
		} else if ($post_type == SPARK_TUTOR_BOOKING_POST_TYPE) {
			if($action == 'register') {
				return SPARK_TUTOR_PLUGIN_PUBLIC_PAGES . 'booking/tutor-booking.php';
			}
		} else if ($post_type == SPARK_TUTOR_COURSE_POST_TYPE) {
			if ($action == 'create-course') {
				return gu_path_join(SPARK_TUTOR_PLUGIN_AUTH_DIR, 'pages', 'tutor-add-live-course.php');
			} else if ($action == 'edit-course') {
				return gu_path_join(SPARK_TUTOR_PLUGIN_AUTH_DIR, 'pages', 'tutor-edit-live-course.php');
			} else if ($action == 'delete-course') {
				return gu_path_join(SPARK_TUTOR_PLUGIN_AUTH_DIR, 'pages', 'tutor-delete-live-course.php');
			}
		}
		return $template;
	}

	public function redirect_public_endpoints(){


	}

	public function save_tutor(){
		$enity = new CPTutor();
		$postData = $_POST;
		$postData['name'] = "Oshane 3";
		$postData['status'] = SPARK_TUTOR_STATUS_PENDING;
		if(!$enity->uniqueValidator($postData)) {
		}
		$id = $enity->save_post_data($postData);
		return $id;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Spark_Tutor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Spark_Tutor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/spark-tutor-public.css', array(), $this->version, 'all');
		wp_enqueue_style('bootstrap', SPARK_TUTOR_PLUGIN_URL . 'assets/css/bootstrap-ns.min.css', array(), $this->version, 'all');
		wp_enqueue_style('shards', SPARK_TUTOR_PLUGIN_URL . 'assets/css/shards.css', array(), $this->version, 'all');
		wp_enqueue_style('shards-extras', SPARK_TUTOR_PLUGIN_URL . 'assets/css/shards-extras.css', array(), $this->version, 'all');
		wp_enqueue_style('font-awesome', SPARK_TUTOR_PLUGIN_URL . 'assets/css/font-awesome/css/font-awesome.min.css', array(), $this->version, 'all');
		wp_enqueue_style('font-roboto', 'https://fonts.googleapis.com/css2?family=Roboto&display=swap', array(), $this->version, 'all');
		wp_enqueue_style('parsley-css', plugin_dir_url(__FILE__) . 'css/parsley.css', array(), $this->version, 'all');
		wp_enqueue_style('slick', plugin_dir_url(__FILE__) . 'css/slick/slick.css', array(), $this->version, 'all');
		wp_enqueue_style('slick-theme', plugin_dir_url(__FILE__) . 'css/slick/slick-theme.css', array(), $this->version, 'all');
		wp_enqueue_style('tooltipster', plugin_dir_url(__FILE__) . 'css/tooltipster/tooltipster.bundle.min.css', array(), $this->version, 'all');
		wp_enqueue_style('tooltipster-theme', plugin_dir_url(__FILE__) . 'css/tooltipster/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-shadow.min.css', array(), $this->version, 'all');
		wp_enqueue_style('time-sheet', plugin_dir_url(__FILE__) . 'css/TimeSheet.css', array(), $this->version, 'all');

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Spark_Tutor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Spark_Tutor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/spark-tutor-public.js', array('jquery'), $this->version, false);
		wp_enqueue_script('jquery-lazy', plugin_dir_url(__FILE__) . 'js/jquery.lazy.min.js', array('jquery'), $this->version, false);
		wp_enqueue_script('bootstrap-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/bootstrap.bundle.min.js', array('jquery'), $this->version, true);
		wp_enqueue_script('shards-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/spark.js', array('jquery'), $this->version, false);
		wp_enqueue_script('stripe_checkout', plugin_dir_url(__FILE__) . 'js/checkout.js', array('jquery'));

		wp_enqueue_script('parsley', plugin_dir_url(__FILE__) . 'js/parsley.js', array('jquery'), $this->version, false);
		wp_enqueue_script('slick', plugin_dir_url(__FILE__) . 'js/slick.min.js', array('jquery'), $this->version, false);
		wp_enqueue_script('tooltipster', plugin_dir_url(__FILE__) . 'js/tooltipster.bundle.min.js', array('jquery'), $this->version, false);
		wp_enqueue_script('time-sheet', plugin_dir_url(__FILE__) . 'js/TimeSheet.js', array('jquery'), $this->version, false);
	}

	public function gu_tut_save_rating_handle() {
		$unique_id = sanitize_text_field($_POST['id']);

		$booking = BookingServices::get_booking_by_unique_id($unique_id);
		$id = $booking->getField('ID');
		$tutor = spark_get_entity_by_id($booking->getField('tutor'));
		$course = spark_get_entity_by_id($booking->getField('course'));
		$token = sanitize_text_field($_POST['token']);
		$rating = sanitize_text_field($_POST['rating']);
		$comment = sanitize_textarea_field($_POST['comment']);

		if (empty($token)) {
			spark_redirect_to_404_page();
		}
		if ($token != $booking->getField('tutor_token') && $token != $booking->getField('student_token')) {
			spark_redirect_to_404_page();
		}
		$reviewType = '';
		//For tutor report student
		if ($token == $booking->getField('tutor_token')) {
			update_post_meta($id, 'tutor_token', 0);
			$reviewType = SPARK_TUTOR_TYPE_REPORT;
		}
		//For tutor report student
		if ($token == $booking->getField('student_token')) {
			update_post_meta($id, 'student_token', 0);
			$reviewType = SPARK_TUTOR_TYPE_REVIEW;

		}
		$review = new CPReview();
		$review->save_post_data(
			[
				'rating' => $rating,
				'comment' => $comment,
				'type' => $reviewType,
				'tutor' => $tutor->getField('ID'),
				'booking' => $id,
				'tutor_name' => $tutor->getField('name'),
				'student_name' => $booking->getField('student_name'),
				'course_name' => $course->getField('name'),
				'booking_date' => $booking->getField('booking_date'),
				'booking_unique_id' => $booking->getField('unique_id'),
			]);
		$spark_tutor_options = get_option(SPARK_TUTOR_OPTIOM);
		$bookingPageId = $spark_tutor_options['booking-page'];

		$page = get_post($bookingPageId);
		wp_redirect(site_url($page->post_name . '/?step=review_success'));
		die;
	}

}
