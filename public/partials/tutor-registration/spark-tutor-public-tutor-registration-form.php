<?php

wp_enqueue_script('select2-js', SPARK_TUTOR_PLUGIN_URL . 'assets/js/select2.min.js', array('jquery'), '', false);
wp_enqueue_style('select2-css', SPARK_TUTOR_PLUGIN_URL . 'assets/css/select2.min.css', array(), '', false);

$subjects = spark_get_list_entity(SPARK_TUTOR_SUBJECT_POST_TYPE, null);
$isEdit = false;

$tutor = null;
$tutor_id = 0;
$formErrors = [];
$courses = array();

$subject_options = "";

foreach ($subjects as $value) {
    $subjectArray[$value->getPost()->ID] = $value->getField('name');
    $subject_title = $value->getField('name');
    $subject_options .= "<option value='$subject_title'>$subject_title</option>";
}
$lessionDurationArray = ['90' => '90 mins', '60' => '60 mins', '30' => '30 mins'];
$statusArray = [
	SPARK_TUTOR_STATUS_ACTIVE => SPARK_TUTOR_STATUS_ACTIVE,
	SPARK_TUTOR_STATUS_INACTIVE => SPARK_TUTOR_STATUS_INACTIVE,
];


$post_type = get_query_var( 'gp_type' );
$action = get_query_var( 'gp_action' );

if ($post_type == SPARK_TUTOR_TUTOR_POST_TYPE && $action == 'register') {
    if (
        get_query_param('entity_type') == SPARK_TUTOR_TUTOR_POST_TYPE &&
        get_query_param('save_and_view', false) == '1'
    ) {
		$tutor = new CPTutor();
		$postData = $_POST;
        $postData['status'] = SPARK_TUTOR_STATUS_PENDING;
		if(!$tutor->uniqueValidator($postData)) {
            echo '<div class="alert alert-danger" role="alert">Tutor already exists with'
                . $postData['email'] 
                . " or ". $postData['trn_ssn'] . '</div>';
		} else {
            $tutor_id = $tutor->save_post_data($postData);
        }
    }
}

// $user = new \WP_User(14);
// var_dump($user);
// // echo $user->get('_spark_tutor_id');
// echo "<br>";
// echo CPTutor::get_profile_code_by_user($user);
// echo "<br>";
// // echo $user->get('first_name');
// do_action('send_tutor_registration_email', 14);


?>


<?php 
    if (!$tutor_id) {
?>

<form class="edit-form wizard-form"  id="form-tutor" 
          action="<?php echo get_site_url(); ?>/gtutor-registration/" method="post" enctype="multipart/form-data" data-parsley-validate>
        <input type="hidden" name="action" value="save_form_info">
        <input type="hidden" name="entity_type"  value="<?php echo SPARK_TUTOR_TUTOR_POST_TYPE; ?>">
        <input type="hidden" name="page" value="<?php echo spark_get_current_page() ?>">
        <input type="hidden" name="view" value="<?php echo spark_get_current_view() ?>">
        <input type="hidden" name="modal" value="<?php echo ($isEdit == false) ? esc_html__('1', 'spark-tutor') : '' ?>">
        <input type="hidden" name="cusr" value='t' />

    <h1>Become a tutor:</h1>
    <br>
    
    <!-- One "tab" for each step in the form: -->
    <div class="tab" style="display: block;">
        <h4><?php echo esc_html__('Basic Information', 'spark-tutor') ?></h4>
        <p class="field">
            <input type="text" required="" id="form-name" name="name" title="Name" placeholder="<?php echo esc_html__('Firstname Lastname', 'spark-tutor') ?>"
            value="<?php echo get_query_param('name') ?>"  oninput="this.className = ''">
            <label for="form-name"><?php echo esc_html__('Name', 'spark-tutor') ?></label>
        </p>
        <p class="field">
            <input type="email" required="" id="form-email" name="email" title="email"
                 placeholder="<?php echo esc_html__('Email', 'spark-tutor') ?>"
                value="<?php echo get_query_param('email') ?>"  oninput="this.className = ''">
            <label for="form-email"><?php echo esc_html__('Email', 'spark-tutor') ?></label>
        </p>
        <p class="field">
            <input type="text" required="" id="form-phone" name="phone" title="phone" placeholder="<?php echo esc_html__('Contact Number', 'spark-tutor') ?>"
            value="<?php echo get_query_param('phone') ?>"  oninput="this.className = ''">
            <label for="form-phone"><?php echo esc_html__('Phone', 'spark-tutor') ?></label>
        </p>
    </div>

    <div class="tab">
        <h4><?php echo esc_html__('Introduction / Biography', 'spark-tutor') ?></h4>
        <p class="field">
            <textarea id="intro" rows="3" name="intro" required
                oninput="this.className = ''"><?php echo get_query_param('intro') ?></textarea>
            <label for="intro" ><?php echo esc_html__('Biography', 'spark-tutor') ?></label>
            <div>
                Share a short summary  about yourself with your prospective clients.
                <ul style="margin-top: 0px; padding-top: 0px;">
                    <li>Who you are?</li>
                    <li>What are you passionate about?</li>
                    <li>What is your favorite thing to teach?</li>
                </ul>
                You can provide any additional notable accolades.
            </div>
        </p>
        <p class="field">
            <?php generate_tutor_intro_video_source_selector_html( ); ?>
        </p>
    </div>
    <div class="tab">
        <h4><?php echo esc_html__('Work experience', 'spark-tutor') ?></h4>
        <p class="field">
            <select style="width: 100%;" require="" class="select2 form-control" id="form-grades" multiple name="grades[]" ></select>
            <label for="form-grade">Select the grades you have experience with.</label>
        </p>
        <p class="field">
            <select style="width: 100%;" require="" class="select2 form-control" id="form-subjects" multiple name="subjects[]" >
                <?php echo $subject_options; ?>
            </select>
            <label for="form-grade">Select the subjects you have experience teaching.</label>
        </p>
        <p class="field">
            <input type="file" required  class="upload-custom-file-input" id="customResumeFile" name="resume" 
                oninput="this.className = ''" accept="application/pdf, application/msword">
            <label for="customResumeFile" >
                <?php echo esc_html__('Resume', 'spark-tutor') ?>
            </label>
            <p>
                <?php echo esc_html__(
                    'Upload your resume of CV, preferable a PDF or Word Document.',
                    'spark-tutor'
                );
            ?>
            </p>
        </p>
    </div>
    
    <div class="tab">
        <h4><?php echo esc_html__('Identification', 'spark-tutor') ?></h4>
        <p class="field">
            <input type="text"  oninput="this.className = ''" id="form-trn"
                    placeholder="<?php echo esc_html__('TRN', 'spark-tutor') ?>"
                    value="<?php echo get_query_param('trn_ssn') ?>" name="trn_ssn" required>
            <label for="form-trn" ><?php echo esc_html__('TRN', 'spark-tutor') ?></label>
        </p>
        <p class="field">
            <input type="file" required class="upload-custom-file-input" id="customFile1"
            oninput="this.className = ''" name="picture" accept="image/*">
            <label for="custmFile1" >
                <?php echo esc_html__('Profile Photo', 'spark-tutor') ?>
            </label>
            <p>
                <?php echo esc_html__(
                    'Upload a photo of yourself, preferable a passport size photo, which clearly shows your face.',
                    'spark-tutor'
                );
            ?>
            </p>
        </p>
    </div>

    <div class="tab">
        <h4><?php echo esc_html__('Account Info', 'spark-tutor') ?></h4>
        <p class="field">
            <input type="password" id="password"   oninput="this.className = ''"
                placeholder="<?php echo esc_html__('Password must be 8 or more characters', 'spark-tutor') ?>" 
                pattern=".{8,}" title="<?php echo esc_html__('Password must be 8 or more characters', 'spark-tutor') ?>"
                name="password" value="<?php echo get_query_param('password') ?>" required>
            <label for="password" ><?php echo esc_html__('Password', 'spark-tutor') ?></label>
        </p>
    </div>

    <div style="overflow:auto;">
        <br>
      <div style="float:right;">
        <button type="button" id="prevBtn" onclick="nextPrev(-1)" style="display: none;">Previous</button>
        <button type="button"name="save_and_view" value="1"  id="nextBtn" onclick="nextPrev(1)">Next</button>
      </div>
    </div>
    
    <!-- Circles which indicates the steps of the form: -->
    <div style="text-align:center;margin-top:40px;">
      <span class="step active"></span>
      <span class="step"></span>
      <span class="step"></span>
      <span class="step"></span>
    </div>
    
    </form>
<?php 
    } else {
?>
    <h2>Thank you for registering as a Tutor</h2>
    <p>
        You should receive a confirmation email shortly with your username and password reminder.
    </p>
    <p>
        Before you can be given access to the website we need to verify your registration,
        which can take up to 3 working days to process. A notification email will be sent to you once this has been completed.
    </p>
    <p style="margin-bottom: 50px;">
        If you need any help using the website please contact us with the contact information provided below:
        <br/>
        <span  style="display: inline-block; width: 50px" >Email:</span> <?php echo spark_get_contact_email(true); ?>
        <br/>
        <span  style="display: inline-block; width: 50px">Tel:</span> <?php echo spark_get_contact_number(true); ?>
    </p>
<?php 
    }
?>

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab
window.addEventListener('load', function () {
    jQuery("#form-grades").select2({
        tags: true,
        data: [1,2,3,4,5,6,7,8,9,10,11],
        tokenSeparators: [',', ' ']
    });
    jQuery("#form-subjects").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    });
})
</script>

<?php echo HtmlHelper::render_review_modal() ?>
    <?php echo HtmlHelper::render_report_modal() ?>
