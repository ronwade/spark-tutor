<button class="btn btn-outline-primary btn-form-submit pull-right btn-spark-green" 
    name="save_and_view" value="1"
    style="background: linear-gradient(310deg, #23C4D9 0%, #29F25D 100%); color: #ffffff; border: none; text-shadow: 0px 1px black; font-size: large;" >
    <?php echo esc_html__('Register As Tutor', 'spark-tutor') ?>
</button>
