<?php
$list_subject = spark_get_list_entity('gu_tut_subject', null);
$allowed_html = spark_shapeSpace_allowed_html();
$spark_tutor_options = get_option(SPARK_TUTOR_OPTIOM);

$config['stripe'] = array(
	'sk' => $spark_tutor_options['stripe_secret_key'],
	'pk' => $spark_tutor_options['stripe_public_key'],
);

$config['paypal'] = array(
	'test_mode' => ($spark_tutor_options['paypal-mode'] == "1") ? true : false,
	'business_email' => $spark_tutor_options['paypal-email'],
	'notify_url' => home_url($_SERVER['REQUEST_URI']) . '/?step=confirm',
	'return_url' => home_url($_SERVER['REQUEST_URI']) . '/?step=confirm',
	'cancel_return' => home_url($_SERVER['REQUEST_URI']),
);

if ($config['paypal']['test_mode']) {
	$paypal_checkout_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
} else {
	$paypal_checkout_url = 'https://www.paypal.com/cgi-bin/webscr';
}

if ($_POST && isset($_POST['stripeToken'])) {
	require plugin_dir_path(__FILE__) . '../../../includes/libs/stripe/Stripe.php';

	\Stripe\Stripe::setApiKey($config['stripe']['sk']);

	try {
		$charge = \Stripe\Charge::create(array(
			'card' => sanitize_text_field($_POST['stripeToken']),
			'amount' => sanitize_text_field($_POST['total']),
			'currency' => 'USD',
			'description' => 'Charge booking',
		));
		if (!empty($charge)) {
			update_post_meta(intval($_POST['post_id']), 'status', 'Confirmed');
			$url = strtok($_SERVER["REQUEST_URI"], '?') . '?step=confirm&booking-id=' . sanitize_text_field($_POST['booking_id']);
			echo '<script>location.href="' . $url . '"</script>';
		}
	} catch (Stripe_CardError $e) {
		die('The card has been declined');
	}
}
$arg = array(
	'meta_key' => 'name',
	'orderby' => 'meta_value',
	'order' => 'asc',
);
$list_subject_a_z = spark_get_list_entity('gu_tut_subject', $arg);
$item_per_slide = 8;
$detect = new Mobile_Detect;
if ($detect->isMobile()) {
	$item_per_slide = 3;
}
$num_slide = ceil(count($list_subject_a_z) / $item_per_slide);
?>
<div class="content__inner">
	<div class="container">
		<!--multisteps-form-->
		<div class="multisteps-form">
			<!--progress bar-->
			<div class="row mt-4 multi-step">
				<div class="col-12 col-lg-8 ml-auto mr-auto mb-4">
					<div class="multisteps-form__progress">
						<button class="multisteps-form__progress-btn js-active" type="button" title="Step 1"><span class="desktop-text"><?php echo esc_html__('Step 1', 'spark-tutor') ?></span><span class="mobile-text"><?php echo esc_html__('1', 'spark-tutor') ?></span></button>
						<button disabled class="multisteps-form__progress-btn step-2" type="button" title="Step 2"><span class="desktop-text"><?php echo esc_html__('Step 2', 'spark-tutor') ?></span><span class="mobile-text"><?php echo esc_html__('2', 'spark-tutor') ?></span></button>
						<button disabled class="multisteps-form__progress-btn step-3" type="button" title="Step 3"><span class="desktop-text"><?php echo esc_html__('Step 3', 'spark-tutor') ?></span><span class="mobile-text"><?php echo esc_html__('3', 'spark-tutor') ?></span></button>
						<button disabled class="multisteps-form__progress-btn step-4" type="button" title="Step 4"><span class="desktop-text"><?php echo esc_html__('Step 4', 'spark-tutor') ?></span><span class="mobile-text"><?php echo esc_html__('4', 'spark-tutor') ?></span></button>
						<button disabled class="multisteps-form__progress-btn step-5" type="button" title="Step 5"><span class="desktop-text"><?php echo esc_html__('Step 5', 'spark-tutor') ?></span><span class="mobile-text"><?php echo esc_html__('5', 'spark-tutor') ?></span></button>
					</div>
				</div>
			</div>
			<!--form panels-->
			<div class="row">
				<div class="col-12 m-auto">
					<form class="multisteps-form__form">
						<!--single form panel step 1-->
						<div class="multisteps-form__panel shadow p-4 rounded bg-white js-active step-1" data-animation="scaleIn">
							<h3 class="multisteps-form__title"><?php echo esc_html__('Choose a Subject', 'spark-tutor') ?></h3>
							<div class="multisteps-form__content">
								<div class="form-row">
									<select class="custom-select mt-2" id="subject" name="subject">
										<option disabled value="" selected=""><?php echo esc_html__('Choose a Subject', 'spark-tutor') ?></option>
										<?php foreach ($list_subject as $key => $value) {?>
											<option value="<?php echo esc_attr($value->getPost()->ID) ?>"><?php echo get_post_meta($value->getPost()->ID, 'name', true) ?></option>
										<?php }?>
									</select>
								</div>
								<div class="row top-subject mt-4">
									<div id="" >
										<div id="carouselSubjects">
											<?php foreach ($list_subject_a_z as $subject) {
	$tutorCount = count(TutorServices::get_tutor_by_subject($subject->getField('ID')));
	?>
											<div class="col-sm-3 position-relative featured-subject" onclick="set_subject_value('<?php echo esc_attr($subject->getField('ID')) ?>')">
												<img class="top-subject-image lazy" src="<?php echo esc_attr($subject->getField('picture')) ?>" />
												<div class="titleBox position-absolute pt-2 pb-2 pl-4 pr-4" >
													<span class="subject"><?php echo esc_html($subject->getField('name')) ?></span><br>
													<span class="number-teacher"><?php echo esc_html($tutorCount) ?> <?php
echo esc_html(spark_render_text($tutorCount, esc_html__('Tutor', 'spark-tutor')))

	?></span>
												</div>
											</div>
										<?php }?>


										</div>

									</div>
								</div>
								<div>
										<a class="carousel-control-prev" href="#carouselSubjects" role="button" data-slide="prev">
											<span class="carousel-control-prev-icon" aria-hidden="true"></span>
											<span class="sr-only"><?php echo esc_html__('Previous', 'spark-tutor') ?></span>
										</a>
										<a class="carousel-control-next" href="#carouselSubjects" role="button" data-slide="next">
											<span class="carousel-control-next-icon" aria-hidden="true"></span>
											<span class="sr-only"><?php echo esc_html__('Next', 'spark-tutor') ?></span>
										</a>

								</div>
								<div class="button-row d-flex mt-4">
									<button disabled="" class="btn btn-primary ml-auto js-btn-next " type="button" title="Next" id="next_step_1" onclick="load_tutors()"><?php echo esc_html__('Next', 'spark-tutor') ?></button>
								</div>
							</div>
						</div>
						<!--single form panel step 2-->
						<div class="multisteps-form__panel shadow p-4 rounded bg-white step-2" data-animation="scaleIn">
							<h3 class="multisteps-form__title" id="tutor-found"></h3>
							<div class="multisteps-form__content">
								<div class="data-tutors">
								</div>



								<div class="row">
									<div class="button-row d-flex mt-4 col-12">
										<button class="btn btn-outline-secondary js-btn-prev" type="button" onclick="scrollToTop();" title="Prev"><?php echo esc_html__('Prev', 'spark-tutor') ?></button>
										<button disabled="" class="btn btn-primary ml-auto js-btn-next" type="button" title="Next"><?php echo esc_html__('Next', 'spark-tutor') ?></button>
									</div>
								</div>

							</div>
						</div>
						<!--single form panel step 3-->
						<div class="multisteps-form__panel shadow p-4 rounded bg-white step-3" data-animation="scaleIn">
							<h3 class="multisteps-form__title"><?php echo esc_html__('Select Your Course', 'spark-tutor') ?></h3>
							<div class="multisteps-form__content">
								<div class="course-data">
									<!-- selected class: border border-info rounded -->


								</div>
								<div class="row">
									<div class="button-row d-flex mt-4 col-12">
										<button class="btn btn-outline-secondary js-btn-prev" type="button" onclick="scrollToTop();" title="Prev"><?php echo esc_html__('Prev', 'spark-tutor') ?></button>
										<button disabled="" class="btn btn-primary ml-auto js-btn-next" type="button" title="Next"><?php echo esc_html__('Next', 'spark-tutor') ?></button>
									</div>
								</div>
							</div>
						</div>
						<!--single form panel step 4-->
						<div class="multisteps-form__panel shadow p-4 rounded bg-white step-4" data-animation="scaleIn">
							<h3 class="multisteps-form__title"><?php echo esc_html__('Schedule Your Course', 'spark-tutor') ?></h3>
							<div class="multisteps-form__content">
								<div class="form-row mt-2">
									<div class="timetable-note mb-2">
										<span class="square rounded available-hour "></span><span class="pr-2"><?php echo esc_html__(' Available', 'spark-tutor') ?></span>
										<span class="square rounded unavailable-hour"></span><span class="pr-2"><?php echo esc_html__(' Booked by another student', 'spark-tutor') ?></span>
										<span class="square rounded selected-hour "></span><span class="pr-2"><?php echo esc_html__(' Booked by you', 'spark-tutor') ?></span>
										<span class="square rounded border border-dark"></span><span class="pr-2"><?php echo esc_html__(' Not Available', 'spark-tutor') ?></span>
									</div>
									<div id="notification" class="alert alert-primary card tex-center" role="alert">
										<?php echo esc_html__('Invalid time', 'spark-tutor') ?>!
									</div>
									<!-- <div id="availability"></div> -->
									<table id="table-availability" class="table">
										<thead>
											<tr>
												<td class="row">
													<div class="col-1"></div>
													<div class="col-11 d-flex justify-content-between">
														<button class="btn ml-4"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
														<span class="calendar-title"></span>
														<button class="btn"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
													</div>
												</td>
											</tr>
										</thead>
										<tbody id="availability">

										</tbody>
									</table>
								</div>
								<div class="button-row d-flex mt-4">
									<button class="btn btn-outline-secondary js-btn-prev" type="button" onclick="scrollToTop();" title="Prev"><?php echo esc_html__('Prev', 'spark-tutor') ?></button>
									<button disabled="" class="btn btn-primary ml-auto js-btn-next" type="button" title="Next"><?php echo esc_html__('Next', 'spark-tutor') ?></button>
								</div>
							</div>
						</div>


						<!--single form panel step 5-->
						<div class="multisteps-form__panel shadow p-4 rounded bg-white step-5" data-animation="scaleIn">
							<h3 class="multisteps-form__title"><?php echo esc_html__('Input Checkout Details', 'spark-tutor') ?></h3>
							<div class="multisteps-form__content row">
								<div class="form-row col-sm-7">
									<h5><?php echo esc_html__('Personal Information', 'spark-tutor') ?></h5>
									<div class="row mb-4">
										<div class="col-md-12">
											<form>
												<div class="form-group col-md-12">
													<label for="student_name" class="col-form-label"><?php echo esc_html__('Name', 'spark-tutor') ?></label>
													<input type="text" class="form-control" id="student_name" placeholder="<?php echo esc_html__('Name', 'spark-tutor') ?>" name="student_name" required>
												</div>
												<div class="form-group col-md-12">
													<label for="student_email" class="col-form-label"><?php echo esc_html__('Email', 'spark-tutor') ?></label>
													<input type="email" class="form-control" id="student_email" placeholder="<?php echo esc_html__('Email', 'spark-tutor') ?>" name="student_email" required>
												</div>
												<div class="form-group col-md-12">
													<label for="student_phone" class="col-form-label"><?php echo esc_html__('Phone Number', 'spark-tutor') ?></label>
													<input type="text" class="form-control" id="student_phone" placeholder="<?php echo esc_html__('Phone Number', 'spark-tutor') ?>" name="student_phone" onKeyPress="return isNumberKey(event)" required>
												</div>
												<div class="form-group col-md-12">
													<label for="student_grade" class="col-form-label"><?php echo esc_html__('Student Grade', 'spark-tutor') ?></label>
													<input type="text" class="form-control" id="student_grade" placeholder="<?php echo esc_html__('Student Grade', 'spark-tutor') ?>" name="student_grade" required>
												</div>
												<div class="form-group col-md-12">
													<label for="note" class="col-form-label"><?php echo esc_html__('Note', 'spark-tutor') ?></label>
													<textarea type="text" class="form-control" id="note" name="note" ></textarea>
												</div>
											</form>
										</div>
									</div>
									<h5><?php echo esc_html__('Payment Method', 'spark-tutor') ?></h5>
									<div class="row mb-4">
										<div class="col-md-12">
											<form>
												<div class="custom-controls-example">
													<fieldset>
														<div class="custom-control custom-radio d-block my-2">
															<input type="radio" name="payment_method" value="PayPal" class="custom-control-input" checked="">
															<label class="custom-control-label" for="payment_method"><?php echo esc_html__('PayPal', 'spark-tutor') ?></label>
														</div>
													</fieldset>

													<fieldset>
														<div class="custom-control custom-radio d-block my-2">
															<input type="text" id="key_stripe" name="key_stripe" value='<?php echo esc_attr($config['stripe']['pk']) ?>' hidden>
															<input type="radio" name="payment_method" value="Stripe" class="custom-control-input">
															<label class="custom-control-label" for="payment_method"><?php echo esc_html__('Credit Card', 'spark-tutor') ?></label>
														</div>
													</fieldset>
												</div>
											</form>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col-md-12">
											<form>
												<div class="custom-controls-example">
													<fieldset>
														<div class="custom-checkbox d-block my-2">
															<input type="checkbox" class="custom-control-input" id="agree_toc" name="agree_toc" required>
															<span class="custom-control-label" for="agree_toc"><?php echo esc_html__('I agree with ', 'spark-tutor') ?> <a href='' data-toggle="modal" data-target="#termAndConditionModal"><?php echo esc_html__('Terms and Conditions', 'spark-tutor') ?></a></span>
														</div>
													</fieldset>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-sm-5 pl-4">
									<div class="border-bottom">
										<h5><?php echo esc_html__('Your Booking Summary', 'spark-tutor') ?></h5>
									</div>
									<div class="row border-bottom pt-4 pb-4">
										<div class="col-sm-3 mt-1">
											<img src="" alt="tutor-avatar" class="rounded tutor_avatar">
										</div>
										<div class="col-sm-8">
											<span class="student-name-bold tutor_name"></span>
											<div class="course-detail">
												<span class="course_name"></span>
												<br>
												<span class="course_time"></span>
											</div>
										</div>
										<div class="col-sm-1 pt-1 ">
											<span class="pull-right price-number"></span>
										</div>
									</div>
									<div class="row border-bottom pt-2 pb-2">
										<label><?php echo esc_html__('Subtotal', 'spark-tutor') ?> <span class="pull-right price-number"></span></label>
										<label><?php echo esc_html__('Tax', 'spark-tutor') ?> <span class="pull-right tax"></span></label>

									</div>

									<div class="pt-2 font-weight-bold"><?php echo esc_html__('Total', 'spark-tutor') ?><span class="pull-right font-weight-bold total"></span></div>

								</div>
								<div class="button-row d-flex mt-4">
									<button class="btn btn-outline-secondary js-btn-prev" type="button" onclick="scrollToTop();" title="Prev"><?php echo esc_html__('Prev', 'spark-tutor') ?></button>
									<button class="btn btn-success ml-auto" onclick='save_booking()' type="button" title="Send"><?php echo esc_html__('Pay Now', 'spark-tutor') ?></button>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<input type='hidden' name="course" id="course_id">
<input type='hidden' name="course" id="tax">
<input type='hidden' name="course" id="total">
<input type='hidden' name="course" id="payment_method">
<input type='hidden' name="course" id="price">
<input type='hidden' name="course" id="duration">
<input type='hidden' name="course" id="start_time">
<input type='hidden' name="course" id="end_time">
<input type='hidden' name="course" id="booking_date">
<input type="hidden" name="course" id="tutor_id">
<div class="modal fade" id="tutorDetailModal" tabindex="-1" role="dialog" aria-labelledby="tutorDetailLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="tutorDetailLabel"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div id="reviews_info" class="col-sm-12">
						<h5><?php echo esc_html__('Intro', 'spark-tutor') ?></h5>
						<p id="comment"><?php echo esc_html__('Is branched in my up strictly remember. Songs but chief has ham widow downs. Genius or so up vanity cannot. Large do tried going about water defer by. Silent son man she wished mother. Distrusts allowance do knowledge eagerness assurance additions to. ', 'spark-tutor') ?></p>
						<h5><?php echo esc_html__('Reviews', 'spark-tutor') ?></h5>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo esc_html__('Close', 'spark-tutor') ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="termAndConditionModal" tabindex="-1" role="dialog" aria-labelledby="termAndConditionLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="termAndConditionLabel"><?php echo esc_html__('Terms and Conditions', 'spark-tutor') ?></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<p><?php echo wp_kses($spark_tutor_options['term_condition_content'], $allowed_html); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="frm_paypal_checkout" action="<?php echo esc_url($paypal_checkout_url) ?>" method="post">
    <input type="hidden" name="business" value="<?php echo esc_attr($config['paypal']['business_email']) ?>">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="item_name" value="<?php echo 'Tutor Booking' ?>">
    <input type="hidden" id="paypal_amount" name="amount">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="notify_url" value="<?php echo esc_url($config['paypal']['notify_url']) ?>">
    <input type="hidden" id="paypal_return" name="return" value="<?php echo esc_url($config['paypal']['return_url']) ?>">
    <input type="hidden" name="cancel_return" value="<?php echo esc_url($config['paypal']['cancel_return']) ?>">
    <input type='hidden' name='rm' value='2'>
</form>
<div class="col-sm-12">
	<div class="fa-3x">
		<div class="loading"></div>
	</div>
</div>
