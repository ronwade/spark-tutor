<div class="col-sm-6 mb-4 offset-sm-3 booking-confirm">
    <div class="card">
        <div class="card-body">
            <div class="text-center mb-4"><i class="fa fa-hand-peace-o" aria-hidden="true"></i></div>
            <h4 class="card-title text-center"><?php echo esc_html__('Successfully Reviewed', 'spark-tutor') ?></h4>
        </div>
    </div>
</div>
