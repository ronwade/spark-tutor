<?php
$booking = BookingServices::get_booking_by_unique_id($_GET['id']);
$tutor = spark_get_entity_by_id($booking->getField('tutor'));
$token = sanitize_text_field($_GET['token']);
if (empty($token) || $token === '0') {
	spark_redirect_to_404_page();
}
if ($token != $booking->getField('tutor_token') && $token != $booking->getField('student_token')) {
	spark_redirect_to_404_page();
}
$isReview = true;
if ($token == $booking->getField('tutor_token')) {
	$isReview = false;
}
?>
<div class="col-sm-6 mb-4 offset-sm-3">
    <div class="card">
        <div class="card-body">
            <form id="form-review-rating" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" >
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo esc_html__('Tutor', 'spark-tutor') ?>: <?php echo esc_html($tutor->getField('name')) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo esc_html__('Student', 'spark-tutor') ?>: <?php echo esc_html($booking->getField('student_name')) ?>
                    </div>
                </div>
                <input type="hidden" name="id" value="<?php echo esc_attr($_GET['id']) ?>" />
                <input type="hidden" name="action" value="save_rating" />
                <input type="hidden" name="token" value="<?php echo esc_attr($token) ?>" />
                <div class="row mt-4 review-rating">
                    <div class="col-sm-12 ">
                        <?php if ($isReview) {?>
                        <h5><?php echo esc_html__('Rating', 'spark-tutor') ?></h5>
                    <?php } else {?>
                        <h5><?php echo esc_html__('Report', 'spark-tutor') ?></h5>
                    <?php }?>
                    </div>
                    <div class="col-sm-12 star-rating-modal">
                        <span class="star-color" id="star-wrapper">
                            <i class="fa fa-star-o" id="star-1" data-num="1"></i>
                            <i class="fa fa-star-o" id="star-2" data-num="2"></i>
                            <i class="fa fa-star-o" id="star-3" data-num="3"></i>
                            <i class="fa fa-star-o" id="star-4" data-num="4"></i>
                            <i class="fa fa-star-o" id="star-5" data-num="5"></i>
                        </span>
                        <input type="hidden" id="rating" name="rating" value="0" />
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="row information">
                        <div class="col-sm-12">
                        <label for="note" class="col-form-label"><?php echo esc_html__('Comment', 'spark-tutor') ?></label>
                            <div class="form-group">
                                <textarea class="form-control" rows="3" name="comment" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" data-dismiss="modal" value="Save" />
            </form>
        </div>
    </div>
</div>
