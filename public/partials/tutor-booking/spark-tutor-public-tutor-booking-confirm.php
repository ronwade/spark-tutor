<?php
$arg = array(
	'meta_key' => 'unique_id',
	'meta_value' => sanitize_text_field($_GET['booking-id']),
);
$booking = spark_get_list_entity(SPARK_TUTOR_BOOKING_POST_TYPE, $arg);
$booking_id = $booking[0]->getField('ID');
if (isset($_GET['booking-id']) && !empty($_POST['verify_sign'])) {
	update_post_meta(intval($booking_id), 'status', 'Confirmed');
}
$date_format = spark_date_format();
$post_meta = get_post_meta($booking_id);
if ($post_meta['status'][0] == 'Confirmed') {
	$cb_options = get_option(SPARK_TUTOR_OPTIOM);
	$course_meta = get_post_meta($post_meta['course'][0]);
	$tutor_meta = get_post_meta($post_meta['tutor'][0]);

	$template = new Template(SPARK_TUTOR_PLUGIN_PATH . 'includes/email_templates/admin-template.php');
	$template->set('tutor_name', $tutor_meta['name'][0]);
	$template->set('course_name', $course_meta['name'][0]);
	$template->set('student_name', $post_meta['student_name'][0]);
	$template->set('time', date('H:i', strtotime($post_meta['booking_date'][0])));
	$template->set('date', date($date_format, strtotime($post_meta['booking_date'][0])));

	$template->set('duration', $post_meta['duration'][0]);
	$template->set('student_skype', $post_meta['student_grade'][0]);

	if ($cb_options['enabled_admin_email'] == 'on') {
		$admin_subject = isset($cb_options['admin_email_subject']) ? $cb_options['admin_email_subject'] : '';
		$admin_heading = isset($cb_options['admin_email_heading']) ? $cb_options['admin_email_heading'] : '';
		$admin_mail_subject = empty($admin_subject) ? "Booking Information" : $admin_subject;
		$admin_heading = empty($admin_heading) ? "Admin Heading" : $admin_heading;

		$merge_message = $template->render();
		$body = $merge_message;
		wp_mail($cb_options['admin_email'], $admin_mail_subject, $body, $admin_heading);
	}

	if ($cb_options['enabled_tutor_email'] == 'on') {
		$tutor_subject = isset($cb_options['tutor_email_subject']) ? $cb_options['tutor_email_subject'] : '';
		$tutor_heading = isset($cb_options['tutor_email_heading']) ? $cb_options['tutor_email_heading'] : '';
		$tutor_mail_subject = empty($tutor_subject) ? "Booking Information" : $tutor_subject;
		$tutor_heading = empty($tutor_heading) ? "Tutor Heading" : $tutor_heading;

		$merge_message = $template->render();
		$body = $merge_message;
		wp_mail($tutor_meta['email'][0], $tutor_mail_subject, $body, $tutor_heading);
	}

	if ($cb_options['enabled_student_email'] == 'on') {
		$template = new Template(SPARK_TUTOR_PLUGIN_PATH . 'includes/email_templates/student-template.php');
		$template->set('student_name', $post_meta['student_name'][0]);
		$template->set('course_name', $course_meta['name'][0]);
		$template->set('tutor_name', $tutor_meta['name'][0]);
		$template->set('time', date('H:i', strtotime($post_meta['booking_date'][0])));
		$template->set('date', date($date_format, strtotime($post_meta['booking_date'][0])));
		$template->set('duration', $post_meta['duration'][0]);
		$template->set('tutor_trn_ssn', $tutor_meta['trn_ssn'][0]);

		$student_subject = isset($cb_options['student_email_subject']) ? $cb_options['student_email_subject'] : '';
		$student_heading = isset($cb_options['student_email_heading']) ? $cb_options['student_email_heading'] : '';
		$student_mail_subject = empty($student_subject) ? "Booking Information" : $student_subject;
		$student_heading = empty($student_heading) ? "Student Heading" : $student_heading;

		$merge_message = $template->render();
		$body = $merge_message;
		wp_mail($post_meta['student_email'][0], $student_mail_subject, $body, $student_heading);
	}
}

?>

<div class="col-sm-6 mb-4 offset-sm-3 booking-confirm">
    <div class="card">
        <div class="card-body">
            <div class="text-center mb-4"><i class="fa fa-hand-peace-o" aria-hidden="true"></i></div>
            <h4 class="card-title text-center"><?php echo esc_html__('Course Booked', 'spark-tutor') ?></h4>
            <p class="card-text"><?php echo esc_html__('You and your tutor ', 'spark-tutor') ?><?php echo get_post_meta($booking[0]->getField('tutor'), 'name', true) ?> <?php echo esc_html__('will receive the confirmation email with booking details.', 'spark-tutor') ?></p>
            <span><?php echo esc_html__('This is how you are going to contact ', 'spark-tutor') ?><?php echo get_post_meta($booking[0]->getField('tutor'), 'name', true) ?>:</span>
            <div class="text-center"><span class="badge badge-warning"><?php echo get_post_meta($booking[0]->getField('tutor'), 'trn_ssn', true) ?></span></div>

        </div>
    </div>
</div>
