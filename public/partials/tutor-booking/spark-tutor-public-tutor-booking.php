<div style="margin-bottom: 80px; margin-top: 80px;" class="gutut twbs gu-tut-main-content">
	<div class="container">
<h1 align="center">Book a Tutor</h1>
		<?php
$step = isset($_GET['step']) ? sanitize_text_field($_GET['step']) : '';
if ($step === 'confirm') {
	require_once 'spark-tutor-public-tutor-booking-confirm.php';
} else if ($step === 'review') {
	require_once 'spark-tutor-public-tutor-booking-review.php';
} else if ($step === 'review_success') {
	require_once 'spark-tutor-public-tutor-booking-review-success.php';
} else {
	require_once 'spark-tutor-public-tutor-booking-form.php';
}
?>
	</div>
</div>
<script>		
	renderBookingPage ();
</script>
