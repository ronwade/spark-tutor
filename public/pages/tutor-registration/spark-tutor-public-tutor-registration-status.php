<?php

/**
 * Display single login
 *
 * @since v.1.0.0
 * @author themeum
 * @url https://themeum.com
 *
 * @package TutorLMS/Templates
 * @version 1.4.3
 */

if ( ! defined( 'ABSPATH' ) )
	exit;

get_header();

?>
<div class="gutut twbs gu-tut-main-content">
    <div class="subtitle-cover sub-title" style="text-align: center; background-color:#163BD7; background-image:url('https://spark.osoobe.com/wp-content/uploads/2020/06/DSC0077.jpg');background-size: cover;background-position: center;">
            <div class="subtitle-cover-holder" style="padding-top: 150px; padding-bottom: 150px; background-color: rgba(151,59,176, 0.5);">
                <h1 class="page-leading" style="color: #FFFFFF; font-family: 'Rubik', Sans-serif; font-size: 50px; font-weight: 700;">
                    Empowering children on your time!
                </h1>
            </div>
    </div>
    <div class="container gu-container-v-margin" >
        <h2>Tutor Registration Status</h2>
        <hr>
        <?php
        
            $pcode = get_url_param('pcode', null);
            $tid = get_url_param('tid', null);

            if (! is_user_logged_in() ) {
                $tutor = spark_get_entity_by_id($tid);
                if ($tutor->getCpPostType() == SPARK_TUTOR_TUTOR_POST_TYPE) {
                    $gp_user = $tutor->user();
                } else {
                    $gp_user = null;
                }
                
            ?>
                <div class="alert alert-warning" role="alert">
                    Please note, you are currently not logged in.
                </div>
            <?php 
            } else {
                $gp_user = wp_get_current_user();

                if ( ! empty($tid) ) {
                    $tutor = spark_get_entity_by_id($tid);
                    if ($tutor->getCpPostType() == SPARK_TUTOR_TUTOR_POST_TYPE) {
                        $gp_user = $tutor->user();
                    } else {
                        $gp_user = null;
                    }
                } else {
                    $tid = $gp_user->get('_spark_tutor_id');
                    $tutor = spark_get_entity_by_id($tid);
                }
            }

            if ( ! empty($gp_user) && !empty($tutor) ) {
        ?>
            <div class="row">
                <div class="col-md-4">
                    <strong>Name:</strong>
                </div>
                <div class="col-md-8">
                    <?php echo $gp_user->get('first_name')." ".$gp_user->get('last_name'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <strong>Registration status as of: </strong>
                </div>
                <div class="col-md-8">
                    <?php
                    echo sprintf(
                        __("Registered at : %s %s", 'tutor'),
                            date_i18n(get_option('date_format'), true),
                            date_i18n(get_option('time_format'),
                            true
                        ) 
                    );
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <strong>Email:</strong>
                </div>
                <div class="col-md-8">
                    <?php echo $gp_user->get('user_email'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <strong>Status:</strong>
                </div>
                <div class="col-md-8">
                    <?php echo $tutor->getField('status'); ?>
                </div>
            </div>
        <?php
        
                if ( current_user_can('administrator') ) {
                    ?>

                    <div >
                        <br/>
                        <a class="btn btn-primary" href="<?php echo get_home_url()."/wp-admin/admin.php?page=spark-tutor-tutors&view=view&id=".$gp_user->get('_spark_tutor_id'); ?>">
                            View Tutor's Profile
                        </a>
                    </div>
                <?php
               }

            }
        ?>
        <br/>
        <p>
            For information about your registration details, please contact us at
            <a href="mailto:brittany@spark-education.com">
                brittany@spark-education.com
            </a>
            or click on the button below.
            <br>
        </p>
        <a class="btn btn-primary" href="<?php echo get_home_url(); ?>/connect-with-us/" title="Contact Us">
            Contact Us
        </a>

    </div>
</div>
<?php
get_footer();
?>
