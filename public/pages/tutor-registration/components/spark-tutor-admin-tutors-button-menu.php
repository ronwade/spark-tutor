<?php
$view = spark_get_current_view();
$id = isset($_GET['id']) ? sanitize_text_field($_GET['id']) : null;
?>
<div class="tutor-view">
    <div class="card">
        <div class="action mb-2 d-inline btn-group mt-2" role="group">
            <a href="<?php echo spark_get_view_url('list') ?>" class="btn btn btn-outline-primary"><?php echo esc_html__('Back', 'spark-tutor') ?></a>
            <?php if ($view != 'edit') {?>
                <a href="<?php echo spark_get_view_url('edit', $id) ?>" class="btn btn btn-outline-primary"><i class="fa fa-edit"></i> <?php echo esc_html__('Edit', 'spark-tutor') ?></a>
            <?php } else {?>
                <button class="btn btn-outline-primary btn-form-submit" name="save_and_view"><i class="fa fa-eye"></i> <?php echo esc_html__('Save & View', 'spark-tutor') ?></button>
            <?php }?>
            <button class="btn dropdown-toggle btn-outline-primary" type="button" id="set-status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo esc_html__('Availability', 'spark-tutor') ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="set-status">
                <a class="dropdown-item" href="<?php echo spark_get_view_url('working-time', $id) ?>"><?php echo esc_html__('Working Time', 'spark-tutor') ?></a>
            </div>
            <?php 
            if ($view != 'edit') {
                require_once gu_path_join(SPARK_TUTOR_PLUGIN_ADMIN_DIR, 'partials', 'tutors', 'components', 'spark-tutor-admin-tutors-status-dropdown.php');
            }
            ?>
            <button class="btn btn btn-outline-primary" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i> <?php echo esc_html__('Delete', 'spark-tutor') ?></button>
        </div>
    </div>
</div>

<?php echo HtmlHelper::render_delete_modal($id) ?>
