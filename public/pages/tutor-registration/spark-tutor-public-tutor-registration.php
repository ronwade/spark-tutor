<?php

/**
 * Display single login
 *
 * @since v.1.0.0
 * @author themeum
 * @url https://themeum.com
 *
 * @package TutorLMS/Templates
 * @version 1.4.3
 */

if ( ! defined( 'ABSPATH' ) )
	exit;

get_header();

?>

<div class="gutut twbs gu-tut-main-content">
        <div class="subtitle-cover sub-title" style="text-align: center; background-color:#163BD7; background-image:url('https://spark.osoobe.com/wp-content/uploads/2020/06/DSC0077.jpg');background-size: cover;background-position: center;">
                <div class="subtitle-cover-holder" style="padding-top: 150px; padding-bottom: 150px; background-color: rgba(151,59,176, 0.5);">
					<h1 class="page-leading" style="color: #FFFFFF; font-family: 'Rubik', Sans-serif; font-size: 50px; font-weight: 700;">
						Empowering children on your time!
					</h1>
				</div>
        </div>
	<div class="container">
		<br/>
		<h4>Tutor Registration</h4>
		<?php
			require_once SPARK_TUTOR_PLUGIN_PUBLIC_DIR . 'partials/tutor-registration/spark-tutor-public-tutor-registration.php';
		?>
	</div>
</div>
<?php
get_footer();
?>
