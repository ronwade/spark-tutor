<?php

/**
 * Display single login
 *
 * @since v.1.0.0
 * @author themeum
 * @url https://themeum.com
 *
 * @package TutorLMS/Templates
 * @version 1.4.3
 */

if ( ! defined( 'ABSPATH' ) )
	exit;

get_header();
require_once SPARK_TUTOR_PLUGIN_PUBLIC_DIR . 'partials/tutor-booking/spark-tutor-public-tutor-booking.php';
get_footer();
?>
